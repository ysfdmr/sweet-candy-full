<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSetupProjectTable extends Migration
{

    /**
     * Run the migrations.
     *
     * if (Schema::hasTable('users')) {
     * //
     * }
     *
     * if (Schema::hasColumn('users', 'email')) {
     * //
     * }
     * ADD COLUMN
     * Schema::table('users', function ($table) {
     *  $table->string('email');
     * });
     * @return void
     */
    public function up()
    {
        /**
         * ADMIN MODULE
         **/


        if (!Schema::hasTable('locales')) {
            Schema::create('locales', function (Blueprint $table) {
                $table->increments('id');
                $table->timestamps();

                $table->string('language', 2)->unique();
                $table->string('name')->nullable();
                $table->boolean('status')->default(true);
            });
        }

        if (!Schema::hasTable('settings')) {
            Schema::create('settings', function (Blueprint $table) {
                $table->increments('id');
                $table->timestamps();

                $table->string('key');
                $table->text('value')->nullable()->default(null);

                $table->integer('locale_id')->unsigned()->index()->nullable();
                $table->foreign('locale_id')->references('id')->on('locales')->onDelete('cascade');
            });
        }
        /**
         * AUTH MODULE
         **/
        if (!Schema::hasTable('users')) {
            Schema::create('users', function (Blueprint $table) {
                $table->increments('id');
                $table->string('first_name');
                $table->string('last_name');
                $table->string('username')->unique();
                $table->string('email')->unique();
                $table->string('password', 60);
                $table->boolean('active')->default(true);
                $table->rememberToken();
                $table->timestamps();
            });
        }
        if (!Schema::hasTable('password_resets')) {
            Schema::create('password_resets', function (Blueprint $table) {
                $table->string('email')->index();
                $table->string('token')->index();
                $table->timestamp('created_at');
            });
        }
        // Create table for storing roles
        if (!Schema::hasTable('roles')) {
            Schema::create('roles', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name')->unique();
                $table->string('display_name')->nullable();
                $table->string('description')->nullable();
                $table->timestamps();
            });
        }
        // Create table for associating roles to users (Many-to-Many)
        if (!Schema::hasTable('role_user')) {
            Schema::create('role_user', function (Blueprint $table) {
                $table->integer('user_id')->unsigned();
                $table->integer('role_id')->unsigned();

                $table->foreign('user_id')->references('id')->on('users')
                    ->onUpdate('cascade')->onDelete('cascade');
                $table->foreign('role_id')->references('id')->on('roles')
                    ->onUpdate('cascade')->onDelete('cascade');

                $table->primary(['user_id', 'role_id']);
            });
        }
        // Create table for storing permissions
        if (!Schema::hasTable('permissions')) {
            Schema::create('permissions', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name')->unique();
                $table->string('display_name')->nullable();
                $table->string('description')->nullable();
                $table->timestamps();
            });
        }
        // Create table for associating permissions to roles (Many-to-Many)
        if (!Schema::hasTable('permission_role')) {
            Schema::create('permission_role', function (Blueprint $table) {
                $table->integer('permission_id')->unsigned();
                $table->integer('role_id')->unsigned();

                $table->foreign('permission_id')->references('id')->on('permissions')
                    ->onUpdate('cascade')->onDelete('cascade');
                $table->foreign('role_id')->references('id')->on('roles')
                    ->onUpdate('cascade')->onDelete('cascade');

                $table->primary(['permission_id', 'role_id']);
            });
        }

        /**
         * MEDIAZER MODULE
         **/

        if (!Schema::hasTable('images')) {
            Schema::create('images', function (Blueprint $table) {
                $table->increments('id');
                $table->string('path');
                $table->string('file_name');
                $table->string('alt')->nullable();
                $table->string('title')->nullable();
                $table->text('description')->nullable();
                $table->timestamps();
            });

        }
        if (!Schema::hasTable('galleries')) {
            Schema::create('galleries', function (Blueprint $table) {
                $table->increments('id');
                $table->string('name');
                $table->text('description')->nullable();
                $table->integer('width')->nullable();
                $table->integer('height')->nullable();
                $table->timestamps();
            });
        }
        if (!Schema::hasTable('gallery_images')) {
            Schema::create('gallery_images', function (Blueprint $table) {
                $table->increments('id');

                $table->unsignedInteger('order');

                $table->unsignedInteger('image_id')->index();
                $table->unsignedInteger('gallery_id')->index();

                $table->foreign('image_id')->references('id')->on('images')->onDelete('cascade');
                $table->foreign('gallery_id')->references('id')->on('galleries')->onDelete('cascade');

                $table->timestamps();
            });
        }
        /**
         * BLOG MODULE
         **/
        if (!Schema::hasTable('categories')) {
            Schema::create('categories', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');

                //nested
                $table->integer('parent_id')->nullable()->index();
                $table->integer('lft')->nullable()->index();
                $table->integer('rgt')->nullable()->index();
                $table->integer('depth')->nullable();
                //nested

                $table->string('title');
                $table->string('slug');
                $table->text('description')->nullable()->default(null);
                $table->string('meta_title')->nullable();
                $table->string('meta_description')->nullable();
                $table->string('meta_keywords')->nullable();

                $table->boolean('status')->default(true);
                //relation
                $table->integer('user_id')->unsigned()->nullable()->index();
                $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');

                $table->integer('locale_id')->unsigned()->index();
                $table->foreign('locale_id')->references('id')->on('locales')->onDelete('cascade');

                $table->timestamps();
            });
        }
        if (!Schema::hasTable('blog_posts')) {
            Schema::create('blog_posts', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');

                $table->string('title');
                $table->string('slug');
                $table->text('description')->nullable()->default(null);
                $table->longText('content');
                $table->string('meta_title')->nullable();
                $table->string('meta_description')->nullable();
                $table->string('meta_keywords')->nullable();
                $table->enum('status', array('PUBLISHED', 'DRAFT', 'PASSIVE'))->default('DRAFT');


                $table->unsignedInteger('category_id')->nullable()->index();
                $table->foreign('category_id')->references('id')->on('categories')->onDelete('set null');

                $table->unsignedInteger('cover')->nullable()->index();
                $table->foreign('cover')->references('id')->on('images')->onDelete('set null');

                $table->unsignedInteger('user_id')->nullable()->index();
                $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');

                $table->integer('locale_id')->unsigned()->index();
                $table->foreign('locale_id')->references('id')->on('locales')->onDelete('cascade');
                $table->timestamps();
            });
        }
        if (!Schema::hasTable('blog_custom_fields')) {
            Schema::create('blog_custom_fields', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');

                $table->string('key');
                $table->text('value')->nullable()->default(null);

                $table->unsignedInteger('post_id')->nullable()->index();
                $table->foreign('post_id')->references('id')->on('blog_posts')->onDelete('cascade');

                $table->timestamps();
            });

        }

        /**
         * CUSTOM MODULES
         **/
        if (!Schema::hasTable('module_posts')) {
            Schema::create('module_posts', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->string('module')->index();

                $table->string('title');
                $table->string('slug');
                $table->text('description')->nullable()->default(null);
                $table->longText('content');
                $table->string('meta_title')->nullable();
                $table->string('meta_description')->nullable();
                $table->string('meta_keywords')->nullable();
                $table->enum('status', array('PUBLISHED', 'DRAFT', 'PASSIVE'))->default('PUBLISHED');

                $table->unsignedInteger('cover')->nullable()->index();
                $table->foreign('cover')->references('id')->on('images')->onDelete('set null');

                $table->unsignedInteger('slider')->nullable()->index();
                $table->foreign('slider')->references('id')->on('galleries')->onDelete('set null');

                $table->unsignedInteger('user_id')->nullable()->index();
                $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');

                $table->integer('locale_id')->unsigned()->index();
                $table->foreign('locale_id')->references('id')->on('locales')->onDelete('cascade');
                $table->timestamps();
            });
        }
        if (!Schema::hasTable('module_custom_fields')) {
            Schema::create('module_custom_fields', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');

                $table->string('key');
                $table->text('value')->nullable()->default(null);

                $table->unsignedInteger('module_post_id')->nullable()->index();
                $table->foreign('module_post_id')->references('id')->on('module_posts')->onDelete('cascade');

                $table->timestamps();
            });
        }
        if (!Schema::hasTable('module_gallery')) {
            Schema::create('module_gallery', function (Blueprint $table) {
                $table->increments('id');

                $table->unsignedInteger('order');

                $table->unsignedInteger('image_id')->index();
                $table->unsignedInteger('module_post_id')->index();

                $table->foreign('image_id')->references('id')->on('images')->onDelete('cascade');
                $table->foreign('module_post_id')->references('id')->on('module_posts')->onDelete('cascade');

                $table->timestamps();
            });
        }
        /**
         * PAGE MODULE
         **/
        if (!Schema::hasTable('pages')) {
            Schema::create('pages', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');

                $table->string('title');
                $table->string('slug');
                $table->text('description')->nullable()->default(null);
                $table->longText('content');

                $table->string('meta_title')->nullable();
                $table->string('meta_description')->nullable();
                $table->string('meta_keywords')->nullable();
                $table->boolean('status')->default(true);

                $table->unsignedInteger('cover')->nullable()->index();
                $table->foreign('cover')->references('id')->on('images')->onDelete('set null');

                $table->unsignedInteger('slider')->nullable()->index();
                $table->foreign('slider')->references('id')->on('galleries')->onDelete('set null');

                $table->unsignedInteger('user_id')->nullable()->index();
                $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');

                $table->integer('locale_id')->unsigned()->index();
                $table->foreign('locale_id')->references('id')->on('locales')->onDelete('cascade');
                $table->timestamps();

            });
        }
        /**
         * LOCATIONS MODULE
         **/
        if (!Schema::hasTable('locations')) {
            Schema::create('locations', function (Blueprint $table) {
                $table->increments('id');
                $table->string('title');
                $table->string('slug');
                $table->text('description')->nullable()->default(null);
                $table->longText('content')->nullable()->default(null);

                $table->text('address')->nullable()->default(null);

                $table->text('map_address')->nullable()->default(null);
                $table->string('long')->nullable()->default(null);
                $table->string('lat')->nullable()->default(null);

                $table->unsignedInteger('cover')->nullable()->index();
                $table->foreign('cover')->references('id')->on('images')->onDelete('set null');

                $table->integer('locale_id')->unsigned()->index();
                $table->foreign('locale_id')->references('id')->on('locales')->onDelete('cascade');

                $table->boolean('status')->default(true);
                $table->timestamps();
            });
        }


        /**
         * MENU MODULE
         **/
        if (!Schema::hasTable('menu')) {
            Schema::create('menu', function (Blueprint $table) {
                $table->increments('id');

                $table->string('name');
                $table->text('description')->nullable()->default(null);
                $table->string('suffix')->nullable()->default(null);

                $table->unsignedInteger('cover')->nullable()->index();
                $table->foreign('cover')->references('id')->on('images')->onDelete('set null');

                $table->unsignedInteger('locale_id')->index();
                $table->foreign('locale_id')->references('id')->on('locales')->onDelete('cascade');

                $table->boolean('status')->default(true);
                $table->timestamps();
            });
        }

        if (!Schema::hasTable('links')) {
            Schema::create('links', function (Blueprint $table) {
                $table->increments('id');

                $table->string('type');// page,external,plugin,blog vs
                $table->string('icon')->nullable()->default(null);
                $table->string('suffix')->nullable()->default(null);

                $table->string('name');
                $table->text('description')->nullable()->default(null);

                $table->string('path')->nullable()->default(null);
                $table->string('url')->nullable()->default(null);
                $table->integer('order')->nullable()->default(null);
                $table->integer('parent')->nullable()->default(null);
                $table->boolean('target')->default(true);

                $table->unsignedInteger('page')->nullable()->index();
                $table->foreign('page')->references('id')->on('pages')->onDelete('cascade');

                $table->unsignedInteger('cover')->nullable()->index();
                $table->foreign('cover')->references('id')->on('images')->onDelete('set null');

                $table->unsignedInteger('menu')->index();
                $table->foreign('menu')->references('id')->on('menu')->onDelete('cascade');

                $table->boolean('status')->default(true);
                $table->timestamps();
            });
        }

        /**
            Product Module Migrations
         */

        if (!Schema::hasTable('product_categories')) {
            Schema::create('product_categories', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');

                //nested
                $table->integer('parent_id')->nullable()->index();
                $table->integer('lft')->nullable()->index();
                $table->integer('rgt')->nullable()->index();
                $table->integer('depth')->nullable();
                //nested

                $table->string('title');
                $table->string('slug');
                $table->text('description')->nullable()->default(null);
                $table->string('meta_title')->nullable();
                $table->string('meta_description')->nullable();
                $table->string('meta_keywords')->nullable();

                $table->unsignedInteger('cover')->nullable()->index();
                $table->foreign('cover')->references('id')->on('images')->onDelete('set null');

                $table->unsignedInteger('slider')->nullable()->index();
                $table->foreign('slider')->references('id')->on('galleries')->onDelete('set null');

                $table->boolean('status')->default(true);
                /*
                //relation
                $table->integer('user_id')->unsigned()->nullable()->index();
                $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
                */
                $table->integer('locale_id')->unsigned()->index();
                $table->foreign('locale_id')->references('id')->on('locales')->onDelete('cascade');

                $table->timestamps();
            });
        }


        if (!Schema::hasTable('product_brand')) {
            Schema::create('product_brand', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');

                //nested
                $table->integer('parent_id')->nullable()->index();
                $table->integer('lft')->nullable()->index();
                $table->integer('rgt')->nullable()->index();
                $table->integer('depth')->nullable();
                //nested

                $table->string('title');
                $table->string('slug');
                $table->text('description')->nullable()->default(null);
                $table->string('meta_title')->nullable();
                $table->string('meta_description')->nullable();
                $table->string('meta_keywords')->nullable();

                $table->unsignedInteger('cover')->nullable()->index();
                $table->foreign('cover')->references('id')->on('images')->onDelete('set null');

                $table->unsignedInteger('slider')->nullable()->index();
                $table->foreign('slider')->references('id')->on('galleries')->onDelete('set null');

                $table->boolean('status')->default(true);
                /*
                //relation
                $table->integer('user_id')->unsigned()->nullable()->index();
                $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');
                */
                $table->integer('locale_id')->unsigned()->index();
                $table->foreign('locale_id')->references('id')->on('locales')->onDelete('cascade');

                $table->timestamps();
            });
        }


        if (!Schema::hasTable('products')) {
            Schema::create('products', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');

                $table->string('title');
                $table->string('slug');
                $table->text('description')->nullable()->default(null);
                $table->longText('content');
                $table->string('meta_title')->nullable();
                $table->string('meta_description')->nullable();
                $table->string('meta_keywords')->nullable();
                $table->string('suffix')->nullable();
                $table->string('code')->nullable();
                $table->decimal('old_price',12,2)->nullable();
                $table->decimal('price',12,2)->nullable();
                $table->integer('stock')->nullable();
                $table->enum('status', array('PUBLISHED', 'DRAFT', 'PASSIVE'))->default('PUBLISHED');

                $table->unsignedInteger('cover')->nullable()->index();
                $table->foreign('cover')->references('id')->on('images')->onDelete('set null');

                $table->unsignedInteger('slider')->nullable()->index();
                $table->foreign('slider')->references('id')->on('galleries')->onDelete('set null');

                $table->integer('locale_id')->unsigned()->index();
                $table->foreign('locale_id')->references('id')->on('locales')->onDelete('cascade');

                $table->unsignedInteger('category_id')->nullable()->index();
                $table->foreign('category_id')->references('id')->on('product_categories')->onDelete('set null');

                $table->unsignedInteger('brand_id')->nullable()->index();
                $table->foreign('brand_id')->references('id')->on('product_brand')->onDelete('set null');

                $table->timestamps();
            });
        }

        if (!Schema::hasTable('product_custom_fields')) {
            Schema::create('product_custom_fields', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');

                $table->string('key');
                $table->text('value')->nullable()->default(null);

                $table->unsignedInteger('product_id')->nullable()->index();
                $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');

                $table->timestamps();
            });
        }
        if (!Schema::hasTable('product_gallery')) {
            Schema::create('product_gallery', function (Blueprint $table) {
                $table->increments('id');

                $table->unsignedInteger('order');

                $table->unsignedInteger('image_id')->index();
                $table->unsignedInteger('product_id')->index();

                $table->foreign('image_id')->references('id')->on('images')->onDelete('cascade');
                $table->foreign('product_id')->references('id')->on('products')->onDelete('cascade');

                $table->timestamps();
            });
        }

        /**
        Estate Module Migrations
         

        if (!Schema::hasTable('estate_categories')) {
            Schema::create('estate_categories', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');

                //nested
                $table->integer('parent_id')->nullable()->index();
                $table->integer('lft')->nullable()->index();
                $table->integer('rgt')->nullable()->index();
                $table->integer('depth')->nullable();
                //nested

                $table->string('title');
                $table->string('slug');
                $table->text('description')->nullable()->default(null);
                $table->string('meta_title')->nullable();
                $table->string('meta_description')->nullable();
                $table->string('meta_keywords')->nullable();

                $table->unsignedInteger('cover')->nullable()->index();
                $table->foreign('cover')->references('id')->on('images')->onDelete('set null');

                $table->unsignedInteger('slider')->nullable()->index();
                $table->foreign('slider')->references('id')->on('galleries')->onDelete('set null');

                $table->boolean('status')->default(true);
                
                $table->integer('locale_id')->unsigned()->index();
                $table->foreign('locale_id')->references('id')->on('locales')->onDelete('cascade');

                $table->timestamps();
            });
        }


        if (!Schema::hasTable('estate_products')) {
            Schema::create('estate_products', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');

                $table->string('title');
                $table->string('slug');
                $table->text('description')->nullable()->default(null);
                $table->longText('content');
                $table->string('meta_title')->nullable();
                $table->string('meta_description')->nullable();
                $table->string('meta_keywords')->nullable();
                $table->string('suffix')->nullable();
                $table->string('code')->nullable();
                $table->decimal('old_price',12,2)->nullable();
                $table->decimal('price',12,2)->nullable();
                $table->integer('stock')->nullable();
                $table->enum('status', array('PUBLISHED', 'DRAFT', 'PASSIVE'))->default('PUBLISHED');

                $table->unsignedInteger('cover')->nullable()->index();
                $table->foreign('cover')->references('id')->on('images')->onDelete('set null');

                $table->unsignedInteger('slider')->nullable()->index();
                $table->foreign('slider')->references('id')->on('galleries')->onDelete('set null');

                $table->integer('locale_id')->unsigned()->index();
                $table->foreign('locale_id')->references('id')->on('locales')->onDelete('cascade');

                $table->unsignedInteger('estate_category_id')->nullable()->index();
                $table->foreign('estate_category_id')->references('id')->on('estate_product_categories')->onDelete('set null');

                //$table->unsignedInteger('brand_id')->nullable()->index();
                //$table->foreign('brand_id')->references('id')->on('product_brand')->onDelete('set null');

                $table->timestamps();
            });
        }

        if (!Schema::hasTable('estate_product_custom_fields')) {
            Schema::create('estate_product_custom_fields', function (Blueprint $table) {
                $table->engine = 'InnoDB';
                $table->increments('id');

                $table->string('key');
                $table->enum('type',['text','textarea','checkbox','radio','select','selectrange']);
                $table->string('name')->nullable();
                $table->text('value')->nullable()->default(null);
                $table->boolean('selected')->default(false);// multiple checkbox & select list
                $table->string('group')->nullable();//radio
                $table->integer('range_start')->default(null);
                $table->integer('range_end')->default(null);
                $table->string('source')->nullable()->default(null);

                $table->unsignedInteger('estate_product_id')->nullable()->index();
                $table->foreign('estate_product_id')->references('id')->on('estate_products')->onDelete('cascade');

                $table->timestamps();
            });
        }

        if (!Schema::hasTable('estate_product_gallery')) {
            Schema::create('estate_product_gallery', function (Blueprint $table) {
                $table->increments('id');

                $table->unsignedInteger('order');

                $table->unsignedInteger('image_id')->index();
                $table->unsignedInteger('estate_product_id')->index();

                $table->foreign('image_id')->references('id')->on('images')->onDelete('cascade');
                $table->foreign('estate_product_id')->references('id')->on('estate_products')->onDelete('cascade');

                $table->timestamps();
            });
        }
*/

    }






    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('categories');
        Schema::drop('blog_posts');
        Schema::drop('blog_custom_fields');

        Schema::drop('module_custom_fields');
        Schema::drop('module_gallery');
        Schema::drop('module_posts');

        Schema::drop('pages');

        Schema::drop('images');
        Schema::drop('galleries');
        Schema::drop('gallery_images');

        Schema::drop('users');
        Schema::drop('password_resets');
        Schema::drop('roles');
        Schema::drop('role_user');
        Schema::drop('permissions');
        Schema::drop('permission_role');

        Schema::drop('locations');
        Schema::drop('locales');
        Schema::drop('settings');
    }

}
