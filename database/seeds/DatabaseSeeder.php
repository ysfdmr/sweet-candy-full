<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        // $this->call(UserTableSeeder::class);

        \Modules\Admin\Entities\Locale::create(array(
            'language'  => 'en',
            'name'      => 'English',
            'status'    => 1
        ));
        \Modules\Admin\Entities\Locale::create(array(
            'language'  => 'tr',
            'name'      => 'Türkçe',
            'status'    => 1
        ));


        $this->command->info('Languages seeded!');

        $adminGroup = \Modules\Auth\Entities\Role::firstOrNew([
            'name'  => 'admin',
            'display_name'  =>'Administrator',
            'description'   => ''
        ]);
        $adminUser = \App\User::firstOrCreate([
            'first_name' => 'admin',
            'last_name' => 'user',
            'username' => 'admin',
            'email' => 'admin@admin.com',
            'password' => bcrypt('admin')
        ]);

        $this->command->info('Users seeded!');


        \Modules\Page\Entities\Page::create(array(
            'title'         => 'BMCMS First Page',
            'description'   => 'Description here',
            'content'       => 'Content here',
            'status'        => 1,
            'user_id'       => 1,
            'locale_id'     => 1
        ));
        
        \Modules\Page\Entities\Page::create(array(
            'title'         => 'BMCMS İlk Sayfam',
            'description'   => 'Açıklama buraya',
            'content'       => 'İçerik buraya',
            'status'        => 1,
            'user_id'       => 1,
            'locale_id'     => 2
        ));

        $this->command->info('Default Pages seeded!');

        \Modules\Admin\Entities\Setting::insert([
            [
            'key'       => 'language',
            'value'     => 'en',
            'locale_id' => null
            ],
            [
            'key'       => 'theme',
            'value'     => 'default',
            'locale_id' => null
            ],
            [
            'key'       => 'company_name',
            'value'     => 'BMCMS',
            'locale_id' => null
            ],
            [
            'key'       => 'company_info',
            'value'     => 'Your Company Info Here',
            'locale_id' => null
            ],
            [
            'key'       => 'company_address',
            'value'     => 'Your Company Address Here',
            'locale_id' => null
            ],
            [
            'key'       => 'company_phone',
            'value'     => '+90 (212) 111 22 33',
            'locale_id' => null
            ],
            [
            'key'       => 'company_fax',
            'value'     => '+90 (212) 111 22 33',
            'locale_id' => null
            ],
            [
            'key'       => 'company_email',
            'value'     => 'info@yourdomain.com',
            'locale_id' => null
            ],
            [
            'key'       => 'company_map_address',
            'value'     => 'İstanbul, Türkiye',
            'locale_id' => null
            ],
            [
            'key'       => 'company_long',
            'value'     => '28.97835889999999',
            'locale_id' => null
            ],
            [
            'key'       => 'company_lat',
            'value'     => '41.0082376',
            'locale_id' => null
            ],
            [
            'key'       => 'social_facebook',
            'value'     => '#',
            'locale_id' => null
            ],
            [
            'key'       => 'social_twitter',
            'value'     => '#',
            'locale_id' => null
            ],
            [
            'key'       => 'social_instagram',
            'value'     => '#',
            'locale_id' => null
            ],
            [
            'key'       => 'social_youtube',
            'value'     => '#',
            'locale_id' => null
            ],
            [
            'key'       => 'social_google',
            'value'     => '#',
            'locale_id' => null
            ],
            [
            'key'       => 'social_youtube_channel',
            'value'     => '#',
            'locale_id' => null
            ],
            [
            'key'       => 'custom_css',
            'value'     => '<style>/* Your Custom CSS Here */</style>',
            'locale_id' => null
            ],
            [
            'key'       => 'custom_js',
            'value'     => '<script>/* Your Custom JS Here */</script>',
            'locale_id' => null
            ],
            [
            'key'       => 'site_title',
            'value'     => 'BMCMS EN Title',
            'locale_id' => 1
            ],
            [
            'key'       => 'site_title',
            'value'     => 'BMCMS TR Title',
            'locale_id' => 2
            ],
            [
            'key'       => 'home',
            'value'     => '1',
            'locale_id' => 1
            ],
            [
            'key'       => 'home',
            'value'     => '2',
            'locale_id' => 2
            ],
            [
            'key'       => 'media_logo',
            'value'     => '/default/bmcms-logo.png',
            'locale_id' => null
            ]
            /*[
            'key'       => '',
            'value'     => '',
            'locale_id' => null
            ]*/
            ]
        );

        $this->command->info('Default Settings seeded!');

        $adminUser->attachRole($adminGroup);
        $this->command->info('Auth table seeded!');

        Model::reguard();
    }
}
