<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSetupModuleTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('module_posts', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('module')->index();

            $table->string('title');
            $table->string('slug');
            $table->text('description')->nullable()->default(null);
            $table->longText('content');
            $table->string('meta_title')->nullable();
            $table->string('meta_description')->nullable();
            $table->string('meta_keywords')->nullable();
            $table->enum('status', array('PUBLISHED','DRAFT', 'PASSIVE'))->default('PUBLISHED');

            $table->unsignedInteger('cover')->nullable()->index();
            $table->foreign('cover')->references('id')->on('images')->onDelete('set null');

            $table->unsignedInteger('slider')->nullable()->index();
            $table->foreign('slider')->references('id')->on('galleries')->onDelete('set null');

            $table->unsignedInteger('user_id')->nullable()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');

            $table->integer('locale_id')->unsigned()->index();
            $table->foreign('locale_id')->references('id')->on('locales')->onDelete('cascade');
            $table->timestamps();
        });

        Schema::create('module_custom_fields', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->string('key');
            $table->text('value')->nullable()->default(null);

            $table->unsignedInteger('module_post_id')->nullable()->index();
            $table->foreign('module_post_id')->references('id')->on('module_posts')->onDelete('cascade');

            $table->timestamps();
        });

        Schema::create('module_gallery', function(Blueprint $table)
        {
            $table->increments('id');

            $table->unsignedInteger('order');

            $table->unsignedInteger('image_id')->index();
            $table->unsignedInteger('module_post_id')->index();

            $table->foreign('image_id')->references('id')->on('images')->onDelete('cascade');
            $table->foreign('module_post_id')->references('id')->on('module_posts')->onDelete('cascade');

            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('categories');
        //Schema::drop('category_translations');
        Schema::drop('blog_posts');
        Schema::drop('blog_custom_fields');
    }

}
