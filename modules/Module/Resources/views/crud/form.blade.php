{{--CONTENT SECTION--}}
<input name="module" type="hidden" value="{{ $moduleName }}"/>
@if($modulePost)
    <input name="post" type="hidden" value="{{ $modulePost->id }}"/>
@endif
<div class="form-group {{ $errors->first('title','has-error') }}">
    <label for="title" class="control-label">{{ Lang::get('global.word.title') }}</label>
    {!! Form::text('title', null, ['class' => 'form-control','placeholder'=>Lang::get('global.word.title') ]) !!}
    {{-- $errors->first('title','<p class="help-block">:message</p>') --}}
</div>

<div class="form-group">
    <label for="content" class="control-label">{{ Lang::get('global.word.description') }}</label>
    {!! Form::textarea('description', null, ['class' => 'form-control ckeditor', 'size'=>'3x5']) !!}
</div>

<div class="form-group">
    <label for="content" class="control-label">{{ Lang::get('global.word.article') }}</label>
    {!! Form::textarea('content', null, ['class' => 'form-control ckeditor']) !!}
</div>

<div class="row">
    <div class="col-lg-6">
        <div class="row">

            <div class="col-md-6">
                <div class="form-group">
                    <div class="mediazer" data-input="cover" data-select="single" data-source="id"></div>
                </div>
            </div>

            
                <div class="col-md-6">
                    @if($modulePost->cover!=null)
                        <label>
                            {!! Form::checkbox('rm_image', '1', null,  ['id' => 'rm_image']) !!}
                            {{ Lang::get('global.text.remove_cover') }}
                        </label> <br/>
                        <img class="img-responsive thumbnail MZ-view-cover" src="{{Helpers::image($modulePost->Cover->FullPath) }}"  />
                    @else
                        <img class="img-responsive thumbnail MZ-view-cover" src="holder.js/400x300?random=yes&text=No-Photo" alt=""/>
                    @endif
                </div>

        </div>

    </div>

    <div class="col-lg-6">
        {!! Form::select('slider', [null=>'Select Slider']+$sliders->toArray() , null , ['class' => 'form-control']) !!}
    </div>
</div>
{{--CONTENT SECTION END--}}






