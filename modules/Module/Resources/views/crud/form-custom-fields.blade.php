{{--META SECTION START--}}
<div class="page-header">
    <h1 style="text-transform: capitalize">{{ $moduleName }}  <small>Module Custom Fields</small></h1>
</div>
<div class="row">
    <div class="col-lg-12" id="custom-fields">
        {{--
        @if($modulePost)
            @foreach($modulePost->CustomField as $field)
                <div class="form-group cf cf-{{ $field->key }}" data-key="{{ $field->key }}">
                    <label class="control-label">
                        Key: {{ $field->key }}
                    </label>
                    <input type="text" name="custom_field[{{ $field->key }}]" class="form-control" placeholder="{{ $field->key }}" value="{{ $field->value }}">
                </div>
            @endforeach
        @else
            @foreach(ThemeComponents::getModuleFields($moduleName) as $field)
                <div class="form-group cf cf-{{ $field }}" data-key="{{ $field }}">
                    <label class="control-label">
                        Key: {{ $field }}
                    </label>
                    <input type="text" name="custom_field[{{ $field }}]" class="form-control" placeholder="{{ $field }}" value="">
                </div>
            @endforeach
        @endif
        --}}
            @foreach(ThemeComponents::getModuleFields($moduleName) as $field)

                <div class="form-group cf cf-{{ $field }}" data-key="{{ $field }}">
                    <label class="control-label">
                        Key: {{ $field }}
                    </label>
                    <input type="text" name="custom_field[{{ $field }}]" class="form-control" placeholder="{{ $field }}"
                        @if($modulePost)
                            @if(in_array($field,$modulePost->CustomField->lists('key')->toArray()))
                            value="{{ $modulePost->CustomField->where('key',$field)->first()->value }}"
                            @endif
                        @endif >
                </div>

            @endforeach
    </div>
</div>


{{--META SECTION END--}}

