<?php

Route::group(['prefix' => '/admin','middleware'=>['auth'], 'namespace' => 'Modules\Module\Http\Controllers'], function()
{
	Route::controller('/modules/{modulename}', 'ModuleController');
});
