<?php namespace Modules\Module\Http\Controllers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Input;
use Modules\Module\Entities\ModuleGallery;
use Modules\Mediazer\Entities\Gallery;
use Modules\Module\Entities\CustomField;
use Modules\Module\Entities\CustomModule;
use Modules\Module\Http\Requests\ModuleRequest;
use Pingpong\Modules\Routing\Controller;

class ModuleController extends Controller {
    protected $user;

    function __construct()
    {
        $this->user=Auth::id();
    }

	public function getIndex($module)
	{
        if(!in_array($module,\ThemeComponents::getModuleNames())){
            return "Module Not Found! Please Check Theme Settings Field.";
        }else{
            $moduleName=$module;
            $modules=CustomModule::where('module',$module)->get();
            return view('module::crud.index',compact('moduleName','modules'));
        }
	}

    public function getCreate($module)
    {
        if(!in_array($module,\ThemeComponents::getModuleNames())){
            return "Module Not Found! Please Check Theme Settings Field.";
        }else{
            $moduleName=$module;
            $sliders=Gallery::lists('name','id');
            return view('module::crud.create',compact('moduleName','sliders'));
        }
    }

    public function postIndex(ModuleRequest $request, $module)
    {
        if(!in_array($module,\ThemeComponents::getModuleNames())){
            return "Module Not Found! Please Check Theme Settings Field.";
        }else{
            $moduleName=$module;
            $modulePost=new CustomModule();
            $modulePost->module = $moduleName;
            $modulePost->title            = Input::get('title');
            $modulePost->description      = Input::get('description');
            $modulePost->content          = Input::get('content');
            if(Input::get('cover') != null){
                $modulePost->cover = Input::get('cover');
            }
            $modulePost->meta_title       = Input::get('meta_title');
            $modulePost->meta_description = Input::get('meta_description');
            $modulePost->meta_keywords    = Input::get('meta_keywords');
            if(Input::get('slider') != null){
                $modulePost->slider = Input::get('slider');
            }
            $modulePost->user_id          = $this->user;
            $modulePost->locale_id        = app('app.Languages')->where('language',App::getLocale())->first()->id;
            if($modulePost->save()){
                if(Input::has('custom_field')){
                    foreach(Input::get('custom_field') as $key => $value){
                        $customField=new CustomField();
                        $customField->key=$key;
                        $customField->value=$value;
                        $customField->module_post_id=$modulePost->id;
                        $customField->save();
                    }
                }

                if(Input::get('gallery') !=""){
                    foreach(explode(',',Input::get('gallery')) as $index => $media){
                        $item = new ModuleGallery();
                        $item->module_post_id = $modulePost->id;
                        $item->image_id = $media;
                        $item->order=$index;
                        $item->save();
                    }
                }
            }
            //Cache::forget('helpers.pages'.App::getLocale());
            Cache::flush();
            return  redirect('/admin/modules/'.$moduleName.'/edit/'.$modulePost->slug);
        }
    }

    public function getEdit($module,$one)
    {
        if(!in_array($module,\ThemeComponents::getModuleNames())){
            return "Module Not Found! Please Check Theme Settings Field.";
        }else{
            $moduleName=$module;
            $sliders=Gallery::lists('name','id');
            $modulePost=CustomModule::with('Gallery.Image')->where('slug',$one)->first();
            return view('module::crud.update',compact('moduleName','sliders','modulePost'));
        }
    }

    public function putEdit(ModuleRequest $request,$module,$one)
    {
        if(!in_array($module,\ThemeComponents::getModuleNames())){
            return "Module Not Found! Please Check Theme Settings Field.";
        }else{
            $moduleName=$module;
            $modulePost=CustomModule::findBySlugOrIdOrFail($one);
            $modulePost->module = $moduleName;
            $modulePost->title            = Input::get('title');
            $modulePost->description      = Input::get('description');
            $modulePost->content          = Input::get('content');
            Input::get('rm_image') != true ?:$modulePost->cover = null;
            if(Input::get('cover') != null){
                $modulePost->cover = Input::get('cover');
            }
            $modulePost->meta_title       = Input::get('meta_title');
            $modulePost->meta_description = Input::get('meta_description');
            $modulePost->meta_keywords    = Input::get('meta_keywords');
            if(Input::get('slider') != null){
                $modulePost->slider = Input::get('slider');
            }else{
                $modulePost->slider = null;
            }
            $modulePost->locale_id        = app('app.Languages')->where('language',App::getLocale())->first()->id;
            if($modulePost->save()){
                if(Input::has('custom_field')){
                    foreach(Input::get('custom_field') as $key => $value){
                        if(!$customField = CustomField::where('key',$key)->where('module_post_id',$modulePost->id)->first()){
                            $customField = new CustomField();
                            $customField->module_post_id=$modulePost->id;
                            $customField->key=$key;
                        }
                        $customField->value=$value;
                        $customField->save();
                    }
                }
                if(Input::get('gallery') !=""){
                    $galleryItems = explode(',',Input::get('gallery'));
                    foreach($galleryItems as $index => $media){
                        if(!$item = ModuleGallery::where('image_id',$media)->where('module_post_id',$modulePost->id)->first()){
                            $item = new ModuleGallery();
                            $item->module_post_id = $modulePost->id;
                            $item->image_id = $media;
                        }
                        $item->order=$index;
                        $item->save();
                    }
                }
            }
            //Cache::forget('helpers.'.$moduleName.App::getLocale());
            Cache::flush();
            return  redirect('/admin/modules/'.$moduleName.'/edit/'.$modulePost->slug);
        }
    }
    public function deleteDelete($module,$one)
    {
        $post=CustomModule::findBySlugOrIdOrFail($one);
        $post->delete();
        Cache::flush();
        return redirect()->back();
    }

    public function postGalleryDelete($module,$id)
    {
        $item=ModuleGallery::where('module_post_id',$id)->where('image_id',Input::get('mediaID'))->first();
        if(!$item){
            return response()->Json([
                'status'    => 'success',
                'message'   => 'Media Not Found!'
            ]);
        }else{
            $item->delete();
            Cache::flush();
            return response()->Json([
                'status'    => 'success',
                'message'   => 'Media Deleted!'
            ]);
        }

    }
}