<?php namespace Modules\Module\Entities;

use Illuminate\Database\Eloquent\Model;

class ModuleGallery extends Model {

    protected $table = 'module_gallery';
    public $timestamps = true;
    protected $fillable = [];


    public function Image()
    {
        return $this->hasOne('Modules\Mediazer\Entities\Mediazer','id','image_id');
    }
}