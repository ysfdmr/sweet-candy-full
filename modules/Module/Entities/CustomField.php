<?php namespace Modules\Module\Entities;

use Illuminate\Database\Eloquent\Model;

class CustomField extends Model {

    protected $table = 'module_custom_fields';
    public $timestamps = true;
    protected $fillable = [];

}