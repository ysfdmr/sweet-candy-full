<?php namespace Modules\Module\Entities;
   
use bmcms\scopes\languageTrait;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class CustomModule extends Model implements SluggableInterface{

    protected $table = 'module_posts';
    public $timestamps = true;
    protected $fillable = [];

    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'title',
        'save_to'    => 'slug',
        'on_update'  => true
    ];

    use languageTrait;

    public function Locale()
    {
        return $this->belongsTo('Modules\Admin\Entities\Locale','locale_id','id');
    }

    public function CustomField()
    {
        return $this->hasMany('Modules\Module\Entities\CustomField','module_post_id','id');
    }

    public function Cover()
    {
        return $this->belongsTo('Modules\Mediazer\Entities\Mediazer','cover','id');
    }

    public function Gallery()
    {
        return $this->hasMany('Modules\Module\Entities\ModuleGallery','module_post_id','id')->orderBy('order');
    }
}