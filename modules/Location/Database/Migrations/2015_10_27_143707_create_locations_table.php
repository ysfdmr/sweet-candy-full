<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locations', function(Blueprint $table)
        {
            $table->increments('id');

            $table->string('title');
            $table->string('slug');
            $table->text('description')->nullable()->default(null);
            $table->text('address')->nullable()->default(null);

            $table->text('map_address')->nullable()->default(null);
            $table->string('long')->nullable()->default(null);
            $table->string('lat')->nullable()->default(null);

            $table->integer('locale_id')->unsigned()->index();
            $table->foreign('locale_id')->references('id')->on('locales')->onDelete('cascade');

            $table->boolean('status')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('locations');
    }

}
