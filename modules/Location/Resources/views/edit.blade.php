@extends('admin::layouts.master')

@section('panel.header')
    Location <small> Create</small>
@stop

@section('panel.style')
    <link rel="stylesheet" href="/assets/admin/plugins/mediazer/mediazer.css"/>
@stop

@section('panel.script')
    <script src="/assets/admin/plugins/mediazer/mediazer-components.js"></script>
    <script src="/assets/admin/plugins/mediazer/mediazer.js"></script>
    <script src="/assets/global/plugins/ckeditor/ckeditor.js"></script>
@stop

@section('panel.content')
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <i class="fa fa-coffee"></i>
                    <h3 class="box-title">{{ Lang::get('global.word.quick-menu') }}</h3>
                </div><!-- /.box-header -->
                <div class="box-body clearfix">
                    <div class="row">
                        <div class="col-lg-12 text-right">
                            <a class="btn btn-app" href="/admin/location">
                                <i class="fa fa-list"></i> All Locations
                            </a>
                        </div>
                    </div>
                </div><!-- /.box-body -->
            </div>
        </div>

        <div class="col-lg-12">
            {!! Form::model($location ,['url' => ['/admin/location',$location->slug], 'method' => 'PUT', 'class'=>'','files'=>true]) !!}
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#location-form" data-toggle="tab">Location - {{ $location->id }}</a></li>
                    <li><a href="#meta-form" data-toggle="tab">Meta</a></li>
                    <li class="pull-right"><button type="submit" class="btn btn-primary pull-right">{{ Lang::get('global.word.submit') }}</button></li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="location-form">
                        @include('location::form')
                    </div><!-- /.tab-pane -->
                </div><!-- /.tab-content -->
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-primary pull-right">{{ Lang::get('global.word.submit') }}</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>

@stop