@section('panel.script')
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBGWMg9-5Vss0jfxdSrEWEvVBGSu6Ax2nE"></script>
    <script src="/assets/global/plugins/gmap/gmaps.js"></script>
    <script>
        var map = new GMaps({
            div: '#companyMap',
            lat: 41.0082376,
            lng: 28.97835889999999
        });
        $('#address').keypress(function (e) {
            if (e.keyCode == 13) {
                $('#btn-address-search').trigger("click");
                return false;
            }
        });

        $('body').on('click', '#btn-address-search', function () {
            GMaps.geocode({
                address: $('#address').val(),
                callback: function (results, status) {
                    if (status == 'OK') {
                        var latlng = results[0].geometry.location;
                        map.setCenter(latlng.lat(), latlng.lng());
                        updateMapData(results)
                        map.addMarker({
                            lat: latlng.lat(),
                            lng: latlng.lng(),
                            draggable: true,
                            click: function (e) {
                                var latlng = e.position;
                                GMaps.geocode({
                                    location: latlng,
                                    callback: function (results, status) {
                                        updateMapData(results)
                                    }
                                });
                            },
                            dragend: function (e) {
                                GMaps.geocode({
                                    location: e.latLng,
                                    callback: function (results, status) {
                                        updateMapData(results)
                                    }
                                });
                            }
                        });
                    }
                }
            });

        });

        function updateMapData(results) {
            var latlng = results[0].geometry.location;
            $('input[name="map_address"]').val(results[0].formatted_address);
            $('input[name="long"]').val(latlng.lng());
            $('input[name="lat"]').val(latlng.lat());
        }
    </script>
@append
{{--CONTENT SECTION--}}
<div class="row">
    <div class="col-lg-6">
        <div class="form-group {{ $errors->first('title','has-error') }}">
            <label for="title" class="control-label">{{ Lang::get('global.word.title') }}</label>
            {!! Form::text('title', null, ['class' => 'form-control','placeholder'=>Lang::get('global.word.title') ])
            !!}
            {{-- $errors->first('title','<p class="help-block">:message</p>') --}}
        </div>

        <div class="form-group">
            <label for="description" class="control-label">{{ Lang::get('global.word.description') }}</label>
            {!! Form::textarea('description', null, ['class' => 'form-control ckeditor']) !!}
        </div>

        <div class="form-group">
            <label for="content" class="control-label">{{ Lang::get('global.word.content') }}</label>
            {!! Form::textarea('content', null, ['class' => 'form-control ckeditor']) !!}
        </div>

    </div>

    <div class="col-lg-6">
        <label for="content" class="control-label">Search Address</label>

        <div class="input-group input-group-sm">
            <input type="text" id="address" class="form-control"/>
            <span class="input-group-btn"><button class="btn btn-info btn-flat" type="button" id="btn-address-search">
                    Search!
                </button></span>
        </div>
        <div id="companyMap" style="width: 100%;height: 300px;margin-bottom: 30px;"></div>

        <div class="form-group">
            <label for="content" class="control-label">Custom Adress</label>
            {!! Form::text('address', null, ['class' => 'form-control','placeholder'=>Lang::get('global.word.address')]) !!}
        </div>

        <div class="form-group">
            <label for="content" class="control-label">Map Address</label>
            {!! Form::text('map_address', null, ['class' => 'form-control','placeholder'=>'Select On Map','readonly']) !!}
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="content" class="control-label">long</label>
                    {!! Form::text('long', null, ['class' =>
                    'form-control','placeholder'=>'Long' ,'readonly']) !!}
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="content" class="control-label">lat</label>
                    {!! Form::text('lat', null, ['class' => 'form-control','placeholder'=>'lat','readonly']) !!}
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="mediazer" data-input="cover" data-select="single" data-source="id"></div>
                </div>
            </div>
            <div class="col-md-6">
                @if($location->cover!=null)
                    <label>
                        {!! Form::checkbox('rm_image', '1', null, ['id' => 'rm_image']) !!}
                        {{ Lang::get('global.text.remove_cover') }}
                    </label> <br/>
                    <img class="img-responsive thumbnail MZ-view-cover"
                         src="{{Helpers::image($location->Cover->FullPath) }}"/>
                @else
                    <img class="img-responsive thumbnail MZ-view-cover"
                         src="holder.js/400x300?random=yes&text=No-Photo" alt=""/>
                @endif
            </div>
        </div>
    </div>
</div>

{{--CONTENT SECTION END--}}






