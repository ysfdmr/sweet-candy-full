@extends('admin::layouts.master')

@section('panel.header')
    Location<small> Locations list</small>
@stop

@section('panel.style')
    <link rel="stylesheet" href="/assets/modules/blog/css/blog.css"/>
@stop

@section('panel.script')
    <script src="/assets/modules/blog/js/blog.js"></script>
@stop

@section('panel.content')
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <i class="fa fa-coffee"></i>
                    <h3 class="box-title">{{ Lang::get('global.word.quick-menu') }}</h3>
                </div><!-- /.box-header -->
                <div class="box-body clearfix">
                    <div class="row">
                        <div class="col-lg-12 text-right">
                            <a class="btn btn-app" href="/admin/location/create">
                                <i class="fa fa-edit"></i> Create Location
                            </a>
                        </div>
                    </div>

                </div><!-- /.box-body -->
            </div>
        </div>

        <div class="col-lg-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Location List</h3>
                </div><!-- /.box-header -->
                <div class="box-body no-padding">
                    <table class="table table-striped">
                        <tbody>
                        <tr>
                            <th style="width: 20px">#</th>
                            <th style="width: 20px">{{ Lang::get('global.word.language') }}</th>
                            <th>{{ Lang::get('global.word.title') }}</th>
                            <th>{{ Lang::get('global.word.slug') }}</th>
                            <th>{{ Lang::get('global.word.status') }}</th>
                            <th style="width: 150px">{{ Lang::get('global.word.user') }}</th>
                            <th style="width: 100px">{{ Lang::get('global.word.action') }}</th>
                        </tr>
                        @foreach($locations as $location)
                            <tr>
                                <td>{{ $location->id }}</td>
                                <td>{{ $location->title }}</td>
                                <td>{{ $location->status==true?'ACTIVE':'PASSIVE' }}</td>
                                <td>{{ $location->category_id }}</td>
                                <td>{{ $location->user_id }}</td>
                                <td>
                                    <a class="action badge bg-green" href="/admin/location/{{ $location->id }}/edit" data-toggle="tooltip" data-original-title="{{ Lang::get('global.word.edit') }}">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                    {!! Form::open(array('url' => array('/admin/location',$location->id),'method'=> 'DELETE')) !!}
                                    <button type="submit" class="action badge bg-red"
                                            data-toggle="tooltip" data-original-title="{{ Lang::get('global.word.delete') }}"
                                            onclick="if(confirm('{{ Lang::get('global.text.deleteQuestion') }}'))return true;else return false;">
                                        <i class="fa fa-close"></i>
                                    </button>
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div><!-- /.box-body -->
            </div>
        </div>
    </div>


@stop