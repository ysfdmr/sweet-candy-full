<?php

Route::group(['prefix' => '/admin','middleware'=>['auth'], 'namespace' => 'Modules\Location\Http\Controllers'], function()
{
	Route::resource('/location', 'LocationController');
});