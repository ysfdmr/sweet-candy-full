<?php namespace Modules\Location\Http\Controllers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Input;
use Modules\Location\Entities\Location;
use Modules\Location\Http\Requests\LocationRequest;
use Pingpong\Modules\Routing\Controller;

class LocationController extends Controller {
	
	public function index()
	{
        $locations=Location::where('status',true)->get();
		return view('location::index',compact('locations'));
	}

    public function create()
    {
        return view('location::create');
    }

    public function store(LocationRequest $request)
    {
        $location = new Location();
        $location->title = Input::get('title');
        $location->description = Input::get('description');
        $location->content = Input::get('content');
        $location->address = Input::get('address');
        $location->map_address = Input::get('map_address');
        $location->long = Input::get('long');
        $location->lat = Input::get('lat');
        $location->locale_id = app('app.Languages')->where('language',App::getLocale())->first()->id;

        if(Input::get('cover') != null){
            $location->cover = Input::get('cover');
        }else{
            $location->cover = null;
        }

        $location->save();
        Cache::flush();
        return redirect()->to('/admin/location/'.$location->id.'/edit')->with('message','Success Location Create');
    }

    public function edit($slug)
    {
        $location= Location::findBySlugOrIdOrFail($slug);
        return view('location::edit',compact('location'));
    }

    public function update(LocationRequest $request,$slug)
    {
        $location = Location::findBySlugOrId($slug);
        $location->title = Input::get('title');
        $location->description = Input::get('description');
        $location->content = Input::get('content');
        $location->address = Input::get('address');
        $location->map_address = Input::get('map_address');
        $location->long = Input::get('long');
        $location->lat = Input::get('lat');
        Input::get('rm_image') != true ?:$location->cover = null;
        if(Input::get('cover') != null){
            $location->cover = Input::get('cover');
        }
        $location->save();
        Cache::flush();
        return redirect()->to('/admin/location/'.$location->slug.'/edit')
            ->with('message', 'Update Success');
    }

    public function destroy($slug)
    {
        $location=Location::findBySlugOrId($slug);
        $location->delete();
        Cache::flush();
        return redirect()->back();
    }

}