{{--META SECTION START--}}
<div class="page-header">
    <h1 style="text-transform: capitalize">Product <small> Custom Fields</small></h1>
</div>
<div class="row">
    <div class="col-lg-12" id="custom-fields">
        {{--

        @if($product)
            @foreach($product->CustomField as $field)
                <div class="form-group cf cf-{{ $field->key }}" data-key="{{ $field->key }}">
                    <label class="control-label">
                        Key: {{ $field->key }}
                    </label>
                    <input type="text" name="custom_field[{{ $field->key }}]" class="form-control" placeholder="{{ $field->key }}" value="{{ $field->value }}">
                </div>
            @endforeach
        @else
        @foreach(ThemeComponents::getProductCustomFields() as $field)
            <div class="form-group cf cf-{{ $field }}" data-key="{{ $field }}">
                <label class="control-label">
                    Key: {{ $field }}
                </label>
                <input type="text" name="custom_field[{{ $field }}]" class="form-control" placeholder="{{ $field }}" value="">
            </div>
        @endforeach
        @endif
        --}}
            @foreach(ThemeComponents::getProductCustomFields() as $field)

                <div class="form-group cf cf-{{ $field['key'] }}" data-key="{{ $field['key'] }}">
                    <label class="control-label">
                        Key: {{ $field['key'] }} - Description: {{ $field['description'] }}
                    </label>
                    <input type="text" name="custom_field[{{ $field['key'] }}]" class="form-control" placeholder="{{ $field['key'] }}"
                        @if($product)
                            @if(in_array($field['key'],$product->CustomField->lists('key')->toArray()))
                            value="{{ $product->CustomField->where('key',$field['key'])->first()->value }}"
                            @endif
                        @endif >
                </div>

            @endforeach

    </div>
</div>


{{--META SECTION END--}}

