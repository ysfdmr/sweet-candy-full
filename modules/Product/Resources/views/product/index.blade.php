@extends('admin::layouts.master')

@section('panel.header')
    Product Manage<small>List Products</small>
@stop

@section('panel.style')
@stop

@section('panel.script')
@stop

@section('panel.content')
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <i class="fa fa-coffee"></i>
                    <h3 class="box-title">{{ Lang::get('global.word.quick-menu') }}</h3>
                </div><!-- /.box-header -->
                <div class="box-body clearfix">
                    <div class="row">
                        <div class="col-lg-12 text-right">
                            <a class="btn btn-app" href="/admin/product/create">
                                <i class="fa fa-edit"></i> Create Product
                            </a>
                            <a class="btn btn-app" href="/admin/product/">
                                <i class="fa fa-list"></i> List Product
                            </a>
                        </div>
                    </div>

                </div><!-- /.box-body -->
            </div>
        </div>

        <div class="col-lg-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Product List</h3>
                </div><!-- /.box-header -->
                <div class="box-body no-padding">
                    <table class="table table-striped">
                        <tbody>
                        <tr>
                            <th style="width: 20px">#</th>
                            <th style="width: 20px">{{ Lang::get('global.word.language') }}</th>
                            <th>{{ Lang::get('global.word.title') }}</th>
                            <th>{{ Lang::get('global.word.status') }}</th>
                            <th>{{ Lang::get('global.word.category') }}</th>
                            <th style="width: 150px">{{ Lang::get('global.word.user') }}</th>
                            <th style="width: 100px">{{ Lang::get('global.word.action') }}</th>
                        </tr>
                        @foreach($products as $product)
                            <tr>
                                <td>{{ $product->id }}</td>
                                <td><img src="/assets/global/flags/{{ $product->Locale->language }}.png" alt="flag"/></td>
                                <td>{{ $product->title }}</td>
                                <td>{{ $product->status }}</td>
                                <td>{{ $product->category_id }}</td>
                                <td>{{ $product->user_id }}</td>
                                <td>
                                    <a class="action badge bg-green" href="/admin/product/{{ $product->slug }}/edit" data-toggle="tooltip" data-original-title="Edit">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                    {!! Form::open(array('url' => array('/admin/product',$product->id),'method'=> 'DELETE')) !!}
                                    <button type="submit" class="action badge bg-red"
                                            data-toggle="tooltip" data-original-title="delete"
                                            onclick="if(confirm('{{ Lang::get('global.text.deleteQuestion') }}'))return true;else return false;">
                                        <i class="fa fa-close"></i>
                                    </button>
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div><!-- /.box-body -->
            </div>
        </div>
    </div>


@stop