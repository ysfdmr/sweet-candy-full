@section('page.style')
    <link rel="stylesheet" href="/assets/global/plugins/select2/select2.min.css"/>
@append
@section('page.script')
    <script src="/assets/global/plugins/select2/select2.min.js"></script>
@append

<div class="row">
    <div class="col-lg-6">
        <div class="form-group {{ $errors->first('title','has-error') }}">
            <label for="title" class="control-label">{{ Lang::get('global.word.title') }}</label>
            {!! Form::text('title', null, ['class' => 'form-control','placeholder'=>Lang::get('global.word.title') ]) !!}
            {{-- $errors->first('title','<p class="help-block">:message</p>') --}}
        </div>

        <div class="form-group">
            <label for="content" class="control-label">{{ Lang::get('global.word.description') }}</label>
            {!! Form::textarea('description', null, ['class' => 'form-control ckeditor', 'size'=>'3x5']) !!}
        </div>

        <div class="form-group">
            <label for="content" class="control-label">{{ Lang::get('global.word.article') }}</label>
            {!! Form::textarea('content', null, ['class' => 'form-control ckeditor']) !!}
        </div>

        <div class="row">
            <div class="col-lg-12">
                <div class="row">

                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="mediazer" data-input="cover" data-select="single" data-source="id"></div>
                        </div>
                    </div>


                    <div class="col-md-6">
                        @if($product->cover!=null)
                            <label>
                                {!! Form::checkbox('rm_image', '1', null,  ['id' => 'rm_image']) !!}
                                {{ Lang::get('global.text.remove_cover') }}
                            </label> <br/>
                            <img class="img-responsive thumbnail MZ-view-cover" src="{{Helpers::image($product->Cover->FullPath) }}"  />
                        @else
                            <img class="img-responsive thumbnail MZ-view-cover" src="holder.js/400x300?random=yes&text=No-Photo" alt=""/>
                        @endif
                    </div>

                </div>

            </div>



        </div>
    </div>


    <div class="col-lg-6">
        <div class="row">
            <div class="col-lg-6">
                <?php function renderNodes($node,$parent=true,$parent_id=null,$self_id=null,$selected=null) { ?>
                @if($node->id!=$self_id)
                    <option value="{{ $node->id }}" @if($selected==$node->id) selected @endif>
                        @for($i=0;$i<=$node->depth*1;$i++) - @endfor {{ $node->title }}
                    </option>
                @endif
                <?php if ( $node->children()->count() > 0 ) {
                    foreach($node->children as $child) renderNodes($child,false,$node->id,$self_id,$selected);
                } ?> </tr> <?php }?>
                <label for="categories" class="control-label">{{ Lang::get('global.word.categories') }}</label>
                <select class="form-control select2" name="category_id" data-placeholder="Select a Category" style="width: 100%;">
                    <?php $selected=$product?$product->category_id:null; ?>
                    @foreach($categories as $category)
                        {!! renderNodes($category,true,null,null,$selected) !!}
                    @endforeach
                </select>
            </div>

            <div class="col-lg-6">
                <div class="form-group ">
                <label for="brand_id" class="control-label">Brands</label>
                {!! Form::select('brand_id', $brands , null , ['class' => 'form-control']) !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="form-group ">
                    <label for="old_price" class="control-label">Old Price</label>
                    {!! Form::text('old_price', null, ['class' => 'form-control','placeholder'=>'Old Price']) !!}
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group ">
                    <label for="price" class="control-label">Price</label>
                    {!! Form::text('price', null, ['class' => 'form-control','placeholder'=>'Price']) !!}
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group ">
                    <label for="stock" class="control-label">Stock</label>
                    {!! Form::text('stock', null, ['class' => 'form-control','placeholder'=>'Stock']) !!}
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group ">
                    <label for="code" class="control-label">Product Code</label>
                    {!! Form::text('code', null, ['class' => 'form-control','placeholder'=>'Product Code']) !!}
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group ">
                    <label for="suffix" class="control-label">Suffix</label>
                    {!! Form::text('suffix', null, ['class' => 'form-control','placeholder'=>'Suffix']) !!}
                </div>
            </div>
        </div>
    </div>
</div>

{{--CONTENT SECTION END--}}






