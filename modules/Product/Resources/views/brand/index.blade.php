@extends('admin::layouts.master')

@section('panel.header')
    BRAND LIST
@stop

@section('panel.style')
@stop

@section('panel.script')
@stop

@section('panel.content')
    <div class="row">
        @include('product::brand.quick-menu')
        <div class="col-lg-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Product Categories</h3>
                </div><!-- /.box-header -->
                <div class="box-body no-padding">

                    <table class="table table-striped">
                        <tbody>
                        <tr>
                            <th style="width: 20px">#</th>
                            <th>{{ Lang::get('global.word.title') }}</th>
                            <th>Slug</th>
                            <th>{{ Lang::get('global.word.status') }}</th>
                            <th style="width: 100px">{{ Lang::get('global.word.action') }}</th>
                        </tr>
                            @foreach($brands as $brand)
                                <tr>
                                    <td>{{ $brand->id }}</td>
                                    <td>{{ $brand->title }}</td>
                                    <td>{{ $brand->slug }}</td>
                                    <td>{{ $brand->status==true?'ACTIVE':'PASSIVE' }}</td>
                                    <td>
                                        <a class="action badge bg-green" href="/admin/product/brand/{{ $brand->slug }}/edit" data-toggle="tooltip" data-original-title="Edit">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                        {!! Form::open(array('url' => array('/admin/product/brand',$brand->id),'method'=> 'DELETE')) !!}
                                        <button type="submit" class="action badge bg-red"
                                                data-toggle="tooltip" data-original-title="delete"
                                                onclick="if(confirm('{{ Lang::get('global.text.deleteQuestion') }}'))return true;else return false;">
                                            <i class="fa fa-close"></i>
                                        </button>
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                            @endforeach


                        </tbody>
                    </table>
                </div><!-- /.box-body -->
            </div>
        </div>
    </div>


@stop