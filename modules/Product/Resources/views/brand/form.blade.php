@section('panel.style')
    <link rel="stylesheet" href="/assets/admin/plugins/mediazer/mediazer.css"/>
@append

@section('panel.script')
    <script src="/assets/admin/plugins/mediazer/mediazer-components.js"></script>
    <script src="/assets/admin/plugins/mediazer/mediazer.js"></script>
@append
<div class="row">
    <div class="col-lg-6">

        <div class="form-group {{ $errors->first('name','has-error') }}">
            <label for="title" class="control-label">{{ Lang::get('global.word.title') }}</label>
            {!! Form::text('title', null, ['class' => 'form-control','placeholder'=>Lang::get('global.word.title') ]) !!}
        </div>

        <div class="form-group">
            <label for="description" class="control-label">{{ Lang::get('global.word.description') }}</label>
            {!! Form::textarea('description', null, ['class' => 'form-control ckeditor']) !!}
        </div>

        <div class="row">

            <div class="col-lg-12">
                <div class="form-group">
                    <label for="status" class="control-label">{{ Lang::get('global.word.status') }}</label>
                    {!! Form::select('status', [1=>'Active',0=>'Passive'] , null , ['class' => 'form-control']) !!}
                </div>
            </div>
        </div>

    </div>
    <div class="col-lg-6">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <div class="mediazer" data-input="cover" data-select="single" data-source="id"></div>
                </div>
            </div>
            <div class="col-md-12">
                @if($brand->cover!=null)
                    <label>
                        {!! Form::checkbox('rm_image', '1', null,  ['id' => 'rm_image']) !!}
                        {{ Lang::get('global.text.remove_cover') }}
                    </label> <br/>
                    <img class="img-responsive thumbnail MZ-view-cover" src="{{Helpers::image($brand->Cover->FullPath) }}"  />
                @else
                    <img class="img-responsive thumbnail MZ-view-cover" src="holder.js/400x300?random=yes&text=No-Photo" alt=""/>
                @endif
            </div>
        </div>
    </div>
</div>












