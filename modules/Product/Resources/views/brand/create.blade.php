@extends('admin::layouts.master')

@section('panel.header')
    Brand Create
@stop

@section('panel.style')
@stop

@section('panel.script')
    <script src="/assets/global/plugins/ckeditor/ckeditor.js"></script>
@stop

@section('panel.content')
    <div class="row">
        @include('product::brand.quick-menu')
        <div class="col-lg-12">
            {!! Form::open(['url' => '/admin/product/brand', 'method' => 'post', 'class'=>'']) !!}
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#content-form" data-toggle="tab">
                                {{ Lang::get('global.word.category') }} {{ Lang::get('global.word.info') }}
                            </a>
                        </li>
                        <li>
                            <a href="#meta-form" data-toggle="tab">
                                Meta
                            </a>
                        </li>
                        <li class="pull-right"><button type="submit" class="btn btn-primary pull-right">{{ Lang::get('global.word.save') }}</button></li>
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane active" id="content-form">
                            @include('product::brand.form')
                        </div><!-- /.tab-pane -->
                        <div class="tab-pane" id="meta-form">
                            @include('product::brand.form-meta')
                        </div><!-- /.tab-pane -->
                    </div><!-- /.tab-content -->
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary pull-right">{{ Lang::get('global.word.save') }}</button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>

@stop