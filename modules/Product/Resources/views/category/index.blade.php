@extends('admin::layouts.master')

@section('panel.header')
    {{ Lang::get('product::trans.category.index.page_title') }} <small>{{ Lang::get('product::trans.category.index.page_description') }}</small>
@stop

@section('panel.style')
    <link rel="stylesheet" href="/assets/global/plugins/treegrid/css/jquery.treegrid.css"/>
@stop

@section('panel.script')
    <script src="/assets/global/plugins/treegrid/js/jquery.treegrid.min.js"></script>
    <script src="/assets/global/plugins/treegrid/js/jquery.treegrid.bootstrap3.js"></script>
    <script>
        $('.tree').treegrid();
    </script>
@stop

@section('panel.content')
    <div class="row">
        @include('product::category.quick-menu')
        <div class="col-lg-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Product Categories</h3>
                </div><!-- /.box-header -->
                <div class="box-body no-padding">
                    {{-- echo "<ul>";
                            foreach($roots as $root) renderNode($root);
                            echo "</ul>";--}}
                    <?php function renderNode($node,$parent=true,$parent_id=null) { ?>
                            <tr class="treegrid-{{$node->id}} {{$parent==false?"treegrid-parent-".$parent_id:''}}">
                                <td>{{ $node->title }}</td>
                                <td>{{ $node->id }}</td>

                                <td>
                                    <a class="action badge bg-green" href="/admin/product/category/{{ $node->slug }}/edit" data-toggle="tooltip" data-original-title="Edit">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                    {!! Form::open(array('url' => array('/admin/product/category',$node->id),'method'=> 'DELETE')) !!}
                                    <button type="submit" class="action badge bg-red"
                                            data-toggle="tooltip" data-original-title="delete"
                                            onclick="if(confirm('{{ Lang::get('global.text.deleteQuestion') }}'))return true;else return false;">
                                        <i class="fa fa-close"></i>
                                    </button>
                                    {!! Form::close() !!}
                                </td>
                        <?php
                            if ( $node->children()->count() > 0 ) {
                                foreach($node->children as $child) renderNode($child,false,$node->id);
                            }
                        ?>
                            </tr>
                    <?php
                        }
                    ?>

                    <table class="table tree">
                        <thead>
                        <tr>
                            <td>
                                <th style="width: 20px">#ID</th>
                            <th style="width: 100px">{{ Lang::get('global.word.action') }}</th>
                            </td>
                        </tr>
                        </thead>
                        @foreach($categories as $category)
                            {!! renderNode($category) !!}
                        @endforeach
                    </table>

                    {{--
                    <table class="table table-striped">
                        <tbody>
                        <tr>
                            <th style="width: 20px">#</th>
                            <th style="width: 20px">{{ Lang::get('global.word.language') }}</th>
                            <th>{{ Lang::get('global.word.title') }}</th>
                            <th>Slug</th>
                            <th>{{ Lang::get('global.word.status') }}</th>
                            <th style="width: 100px">{{ Lang::get('global.word.action') }}</th>
                        </tr>



                            @foreach($categories as $category)
                                <tr>
                                    <td>{{ $category->id }}</td>
                                    <td><img src="/assets/global/flags/{{ $category->Locale->language }}.png" alt="flag"/></td>
                                    <td>{{ $category->title }}</td>
                                    <td>{{ $category->slug }}</td>
                                    <td>{{ $category->status==true?'ACTIVE':'PASSIVE' }}</td>
                                    <td>
                                        <a class="action badge bg-green" href="/admin/product/category/{{ $category->slug }}/edit" data-toggle="tooltip" data-original-title="Edit">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                        {!! Form::open(array('url' => array('/admin/product/category',$category->id),'method'=> 'DELETE')) !!}
                                        <button type="submit" class="action badge bg-red"
                                                data-toggle="tooltip" data-original-title="delete"
                                                onclick="if(confirm('{{ Lang::get('global.text.deleteQuestion') }}'))return true;else return false;">
                                            <i class="fa fa-close"></i>
                                        </button>
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                            @endforeach


                        </tbody>
                    </table>
                    --}}
                </div><!-- /.box-body -->
            </div>
        </div>
    </div>


@stop