<div class="col-lg-12">
    <div class="box box-solid">
        <div class="box-header with-border">
            <i class="fa fa-coffee"></i>
            <h3 class="box-title">{{ Lang::get('global.word.quick-menu') }}</h3>
        </div><!-- /.box-header -->
        <div class="box-body clearfix">
            <div class="row">
                <div class="col-lg-12 text-right">
                    <a class="btn btn-app" href="/admin/product/category/create">
                        <i class="fa fa-edit"></i> Create Product Category
                    </a>
                    <a class="btn btn-app" href="/admin/product/category">
                        <i class="fa fa-list"></i> List All Product Category
                    </a>
                    <a class="btn btn-app" href="/admin/product">
                        <i class="fa fa-newspaper-o"></i> List Product Items
                    </a>
                </div>
            </div>

        </div><!-- /.box-body -->
    </div>
</div>