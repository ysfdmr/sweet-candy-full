@section('panel.style')
    <link rel="stylesheet" href="/assets/admin/plugins/mediazer/mediazer.css"/>
@append

@section('panel.script')
    <script src="/assets/admin/plugins/mediazer/mediazer-components.js"></script>
    <script src="/assets/admin/plugins/mediazer/mediazer.js"></script>
@append
<div class="row">
    <div class="col-lg-6">

        <div class="form-group {{ $errors->first('name','has-error') }}">
            <label for="title" class="control-label">{{ Lang::get('global.word.title') }}</label>
            {!! Form::text('title', null, ['class' => 'form-control','placeholder'=>Lang::get('global.word.title') ]) !!}
        </div>

        <div class="form-group">
            <label for="description" class="control-label">{{ Lang::get('global.word.description') }}</label>
            {!! Form::textarea('description', null, ['class' => 'form-control ckeditor']) !!}
        </div>

        <div class="row">
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="parent_id" class="control-label">{{ Lang::get('global.word.parent') }} {{ Lang::get('global.word.category') }}</label>
                    {{-- Form::select('parent_id', [''=>'Select Parent Category']+$categories , null , ['class' => 'form-control']) --}}
                    <?php function renderNodes($node,$parent=true,$parent_id=null,$self_id=null) { ?>
                    @if($node->id!=$self_id)
                        <option value="{{ $node->id }}">
                            @for($i=0;$i<=$node->depth*1;$i++) - @endfor {{ $node->title }}
                        </option>
                    @endif
                    <?php if ( $node->children()->count() > 0 ) {
                        foreach($node->children as $child) renderNodes($child,false,$node->id,$self_id);
                    } ?>
                    </tr>
                    <?php }?>
                    <select name="parent_id" class="form-control">
                        <option value="">Select Parent Category</option>
                        <?php
                        $self_id=$category?$category->id:null;
                        ?>
                        @foreach($categories as $category)
                            {!! renderNodes($category,true,null,$self_id) !!}
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label for="status" class="control-label">{{ Lang::get('global.word.status') }}</label>
                    {!! Form::select('status', [1=>'Active',0=>'Passive'] , null , ['class' => 'form-control']) !!}
                </div>
            </div>
        </div>

    </div>
    <div class="col-lg-6">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="mediazer" data-input="cover" data-select="single" data-source="id"></div>
                </div>
            </div>
            <div class="col-md-6">
                @if($category->cover!=null)
                    <label>
                        {!! Form::checkbox('rm_image', '1', null,  ['id' => 'rm_image']) !!}
                        {{ Lang::get('global.text.remove_cover') }}
                    </label> <br/>
                    <img class="img-responsive thumbnail MZ-view-cover" src="{{Helpers::image($category->Cover->FullPath) }}"  />
                @else
                    <img class="img-responsive thumbnail MZ-view-cover" src="holder.js/400x300?random=yes&text=No-Photo" alt=""/>
                @endif
            </div>
        </div>
    </div>
</div>












