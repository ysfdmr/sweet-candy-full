<?php namespace Modules\Product\Http\Controllers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Input;
use Modules\Product\Entities\ProductCategory;
use Modules\Product\Http\Requests\ProductCategoryRequest;
use Pingpong\Modules\Routing\Controller;
class ProductCategoryController extends Controller {
	
	public function index()
	{
        $categories=ProductCategory::roots()->get();
		return view('product::category.index',compact('categories'));
	}

    public function create()
    {
        $categories=ProductCategory::roots()->get();
        return view('product::category.create',compact('categories'));
    }

    public function store(ProductCategoryRequest $request)
    {
        $category=new ProductCategory();
        $category->title        = Input::get('title');
        //$category->parent_id    = Input::get('parent_id')==null?null:Input::get('parent_id');

        $category->status       = Input::get('status');
        $category->description  = Input::get('description');
        //METAS
        $category->meta_title       = Input::get('meta_title');
        $category->meta_description = Input::get('meta_description');
        $category->meta_keywords    = Input::get('meta_keywords');
        if(Input::get('cover') != null){
            $category->cover = Input::get('cover');
        }else{
            $category->cover = null;
        }
        $category->locale_id    = app('app.Languages')->where('language',App::getLocale())->first()->id;
        $category->save();

        if(Input::get('parent_id')==null){
            $category->makeRoot();
        }else{
            $parent=ProductCategory::findBySlugOrId(Input::get('parent_id'));
            $category->makeChildOf($parent);
        }

        Cache::flush();
        //return Redirect::to('admin/product/category/'.$category->slug.'/edit')
        return redirect()->back()
            ->with('message',  array('type' => 'success', 'head' => 'Info Message!', 'text' => 'Create Success'));
    }

    public function edit($slug)
    {
        $category=ProductCategory::findBySlugOrId($slug);
        $categories=ProductCategory::roots()->where('slug','<>',$slug)->get();
        return view('product::category.edit',compact('category','categories'));
    }

    public function update(ProductCategoryRequest $request,$slug)
    {
        $category=ProductCategory::findBySlugOrId($slug);
        $category->title        = Input::get('title');
        //$category->parent_id    = Input::get('parent_id')==null?null:Input::get('parent_id');

        $category->status       = Input::get('status');
        $category->description  = Input::get('description');
        //METAS
        $category->meta_title       = Input::get('meta_title');
        $category->meta_description = Input::get('meta_description');
        $category->meta_keywords    = Input::get('meta_keywords');
        Input::get('rm_image') != true ?:$category->cover = null;
        if(Input::get('cover') != null){
            $category->cover = Input::get('cover');
        }
        $category->locale_id    = app('app.Languages')->where('language',App::getLocale())->first()->id;
        $category->save();

        if(Input::get('parent_id')==null){
            $category->makeRoot();
        }else{
            $parent=ProductCategory::findBySlugOrId(Input::get('parent_id'));
            $category->makeChildOf($parent);
        }

        Cache::flush();
        //return Redirect::to('admin/product/category/'.$category->slug.'/edit')
        return redirect()->back()
            ->with('message',  array('type' => 'success', 'head' => 'Info Message!', 'text' => 'Update Success'));
    }

    public function destroy($slug){

        $category=ProductCategory::findBySlugOrId($slug);
        $category->delete();
        return redirect()->back()
            ->with('message',  array('type' => 'success', 'head' => 'Info Message!', 'text' => 'Delete Success'));
    }
}