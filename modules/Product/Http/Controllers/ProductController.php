<?php namespace Modules\Product\Http\Controllers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Input;
use Modules\Product\Entities\Product;
use Modules\Product\Entities\ProductBrand;
use Modules\Product\Entities\ProductCategory;
use Modules\Product\Entities\ProductCustomField;
use Modules\Product\Entities\ProductGallery;
use Modules\Product\Entities\ProductPivot;
use Modules\Product\Http\Requests\ProductRequest;
use Pingpong\Modules\Routing\Controller;

class ProductController extends Controller {

	public function index()
	{
        $products=Product::all();
		return view('product::product.index',compact('products'));
	}

    public function create()
    {
        $categories=ProductCategory::roots()->get();
        $brands=ProductBrand::lists('title','id');
        return view('product::product.create',compact('categories','brands'));
    }

    public function store(ProductRequest $request){

        $product=new Product();
        $product->title            = Input::get('title');
        $product->description      = Input::get('description');
        $product->content          = Input::get('content');
        if(Input::get('cover') != null){
            $product->cover = Input::get('cover');
        }
        $product->category_id      = Input::get('category_id');
        $product->brand_id         = Input::get('brand_id');
        $product->meta_title       = Input::get('meta_title');
        $product->meta_description = Input::get('meta_description');
        $product->meta_keywords    = Input::get('meta_keywords');
        $product->old_price        = Input::get('old_price')==""?null:Input::get('old_price');
        $product->price            = Input::get('price')==""?null:Input::get('price');
        $product->stock            = Input::get('stock')==""?null:Input::get('stock');
        $product->suffix           = Input::get('suffix');
        $product->code             = Input::get('code');

        //$product->user_id          = $this->user;
        $product->locale_id        = app('app.Languages')->where('language',App::getLocale())->first()->id;
        if($product->save()){
            if(Input::has('custom_field')){
                foreach(Input::get('custom_field') as $key => $value){
                    $customField=new ProductCustomField();
                    $customField->key=$key;
                    $customField->value=$value;
                    $customField->product_id=$product->id;
                    $customField->save();
                }
            }

            if(Input::get('gallery') !=""){
                foreach(explode(',',Input::get('gallery')) as $index => $media){
                    $item = new ProductGallery();
                    $item->product_id = $product->id;
                    $item->image_id = $media;
                    $item->order=$index;
                    $item->save();
                }
            }

            if(Input::has('categories')){
                foreach(Input::get('categories') as $key => $value){
                    $categoryPivot=new ProductPivot();
                    $categoryPivot->category_id=$value;
                    $categoryPivot->product_id=$product->id;
                    $categoryPivot->save();
                }
            }
        }
        Cache::flush();
        return  redirect('/admin/product/'.$product->slug.'/edit/');
    }

    public function edit($slug)
    {
        $product=Product::findBySlugOrId($slug);
        $categories=ProductCategory::roots()->get();
        $brands=ProductBrand::lists('title','id');
        return view('product::product.update',compact('product','categories','brands'));
    }

    public function update(ProductRequest $request,$slug){

        $product=Product::findBySlugOrId($slug);
        $product->title            = Input::get('title');
        $product->description      = Input::get('description');
        $product->content          = Input::get('content');
        if(Input::get('cover') != null){
            $product->cover = Input::get('cover');
        }else{
            $product->cover = null;
        }
        $product->category_id      = Input::get('category_id');
        $product->brand_id         = Input::get('brand_id');
        $product->meta_title       = Input::get('meta_title');
        $product->meta_description = Input::get('meta_description');
        $product->meta_keywords    = Input::get('meta_keywords');
        $product->old_price        = Input::get('old_price')==""?null:Input::get('old_price');
        $product->price            = Input::get('price')==""?null:Input::get('price');
        $product->stock            = Input::get('stock')==""?null:Input::get('stock');
        $product->suffix           = Input::get('suffix');
        $product->code             = Input::get('code');

        //$product->user_id          = $this->user;
        $product->locale_id        = app('app.Languages')->where('language',App::getLocale())->first()->id;

        if($product->save()){
            if(Input::has('custom_field')){
                foreach(Input::get('custom_field') as $key => $value){
                    if(!$customField = ProductCustomField::where('key',$key)->where('product_id',$product->id)->first()) {
                        $customField = new ProductCustomField();
                        $customField->key = $key;
                        $customField->product_id = $product->id;
                    }
                    $customField->value=$value;
                    $customField->save();
                }
            }
            if(Input::get('gallery') !=""){
                $galleryItems = explode(',',Input::get('gallery'));
                foreach($galleryItems as $index => $media){
                    if(!$item = ProductGallery::where('image_id',$media)->where('product_id',$product->id)->first()){
                        $item = new ProductGallery();
                        $item->product_id = $product->id;
                        $item->image_id = $media;
                    }
                    $item->order=$index;
                    $item->save();
                }
            }
        }
        Cache::flush();
        return  redirect('/admin/product/'.$product->slug.'/edit/');
    }


    public function destroy($slug){

        $brand=Product::findBySlugOrId($slug);
        $brand->delete();
        return redirect()->back()
            ->with('message',  array('type' => 'success', 'head' => 'Info Message!', 'text' => 'Delete Success'));
    }

    public function postGalleryDelete($product_id)
    {
        $item=ProductGallery::where('product_id',$product_id)->where('image_id',Input::get('mediaID'))->first();
        if(!$item){
            return response()->Json([
                'status'    => 'success',
                'message'   => 'Media Not Found!'
            ]);
        }else{
            $item->delete();
            Cache::flush();
            return response()->Json([
                'status'    => 'success',
                'message'   => 'Media Deleted!'
            ]);
        }

    }


}