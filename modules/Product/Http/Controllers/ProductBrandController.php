<?php namespace Modules\Product\Http\Controllers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Input;
use Modules\Product\Entities\ProductBrand;
use Modules\Product\Http\Requests\ProductBrandRequest;
use Modules\Product\Http\Requests\ProductCategoryRequest;
use Pingpong\Modules\Routing\Controller;

class ProductBrandController extends Controller {
	
	public function index()
	{
        $brands=ProductBrand::all();
		return view('product::brand.index',compact('brands'));
	}

    public function create()
    {
        return view('product::brand.create');
    }

    public function store(ProductBrandRequest $request)
    {
        $brand=new ProductBrand();
        $brand->title        = Input::get('title');
        $brand->status       = Input::get('status');
        $brand->description  = Input::get('description');
        //METAS
        $brand->meta_title       = Input::get('meta_title');
        $brand->meta_description = Input::get('meta_description');
        $brand->meta_keywords    = Input::get('meta_keywords');
        $brand->locale_id    = app('app.Languages')->where('language',App::getLocale())->first()->id;

        if(Input::get('cover') != null){
            $brand->cover = Input::get('cover');
        }else{
            $brand->cover = null;
        }

        $brand->save();
        $brand->makeRoot();
        Cache::flush();
        return redirect()->to('/admin/product/brand')
            ->with('message',  array('type' => 'success', 'head' => 'Info Message!', 'text' => 'Create Success'));
    }

    public function edit($slug)
    {
        $brand=ProductBrand::findBySlugOrId($slug);
        return view('product::brand.edit',compact('brand'));
    }

    public function update(ProductCategoryRequest $request,$slug)
    {
        $brand=ProductBrand::findBySlugOrId($slug);
        $brand->title        = Input::get('title');
        $brand->status       = Input::get('status');
        $brand->description  = Input::get('description');
        //METAS
        $brand->meta_title       = Input::get('meta_title');
        $brand->meta_description = Input::get('meta_description');
        $brand->meta_keywords    = Input::get('meta_keywords');
        $brand->locale_id    = app('app.Languages')->where('language',App::getLocale())->first()->id;

        Input::get('rm_image') != true ?:$brand->cover = null;
        if(Input::get('cover') != null){
            $brand->cover = Input::get('cover');
        }
        $brand->save();
        Cache::flush();
        return redirect()->back()
            ->with('message',  array('type' => 'success', 'head' => 'Info Message!', 'text' => 'Update Success'));
    }

    public function destroy($slug){

        $brand=ProductBrand::findBySlugOrId($slug);
        $brand->delete();
        return redirect()->back()
            ->with('message',  array('type' => 'success', 'head' => 'Info Message!', 'text' => 'Delete Success'));
    }
}