<?php

Route::group(['prefix' => 'admin','middleware'=>['auth'], 'namespace' => 'Modules\Product\Http\Controllers'], function()
{
    Route::resource('/product/brand', 'ProductBrandController');
    Route::resource('/product/category', 'ProductCategoryController');
    Route::resource('/product', 'ProductController');
    Route::post('/product/gallery-delete/{product_id}','ProductController@postGalleryDelete');
});