<?php namespace Modules\Product\Entities;

use Illuminate\Database\Eloquent\Model;

class ProductGallery extends Model {

    protected $table = 'product_gallery';
    public $timestamps = true;
    protected $fillable = [];


    public function Image()
    {
        return $this->hasOne('Modules\Mediazer\Entities\Mediazer','id','image_id');
    }
}