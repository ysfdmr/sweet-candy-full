<?php namespace Modules\Product\Entities;
   
use bmcms\scopes\languageTrait;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Illuminate\Support\Facades\Input;

class Product extends Model implements SluggableInterface{

    protected $table = 'products';
    public $timestamps = true;
    protected $fillable = [];

    use languageTrait;

    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'title',
        'save_to'    => 'slug',
        'on_update'  => true
    ];

    public function Locale()
    {
        return $this->belongsTo('Modules\Admin\Entities\Locale','locale_id','id');
    }

    public function Category()
    {
        return $this->belongsTo('Modules\Product\Entities\ProductCategory','category_id','id');
    }

    public function Cover()
    {
        return $this->belongsTo('Modules\Mediazer\Entities\Mediazer','cover','id');
    }

    public function CustomField()
    {
        return $this->hasMany('Modules\Product\Entities\ProductCustomField','product_id','id');
    }

    public function Gallery()
    {
        return $this->hasMany('Modules\Product\Entities\ProductGallery','product_id','id')->orderBy('order');
    }

    public function scopeFilter($query)
    {
        if ( Input::has('cat') && trim(Input::get('cat') !== '') ) {
            $query->where('category_id', ProductCategory::findBySlugOrId(Input::get('cat'))->id);
        }

        if ( Input::has('brand') && trim(Input::get('brand') !== '') ) {
            $query->where('brand_id', ProductBrand::findBySlugOrId(Input::get('brand'))->id);
        }
        return $query;
    }

}