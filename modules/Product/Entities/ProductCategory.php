<?php namespace Modules\Product\Entities;
   
use bmcms\scopes\languageTrait;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Baum\Node;

class ProductCategory extends Node  implements SluggableInterface{

    protected $table = 'product_categories';
    public $timestamps = true;
    protected $fillable = [];

    use languageTrait;

    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'title',
        'save_to'    => 'slug',
        'on_update'  => true
    ];

    public function Locale()
    {
        return $this->belongsTo('Modules\Admin\Entities\Locale','locale_id','id');
    }

    public function Cover()
    {
        return $this->belongsTo('Modules\Mediazer\Entities\Mediazer','cover','id');
    }

    public function getSlider()
    {
        return $this->hasOne('Modules\Mediazer\Entities\Gallery','id','slider');
    }


    public function getProducts()
    {
        return $this->hasMany('Modules\Product\Entities\Product','category_id','id');
    }

}