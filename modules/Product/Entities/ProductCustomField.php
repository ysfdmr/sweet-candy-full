<?php namespace Modules\Product\Entities;

use Illuminate\Database\Eloquent\Model;

class ProductCustomField extends Model {

    protected $table = 'product_custom_fields';
    public $timestamps = true;
    protected $fillable = [];

}