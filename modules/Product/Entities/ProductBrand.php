<?php namespace Modules\Product\Entities;
   
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Baum\Node;

class ProductBrand extends Node  implements SluggableInterface{

    protected $table = 'product_brand';
    public $timestamps = true;
    protected $fillable = [];



    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'title',
        'save_to'    => 'slug',
        'on_update'  => true
    ];

    public function Cover()
    {
        return $this->belongsTo('Modules\Mediazer\Entities\Mediazer','cover','id');
    }

    public function getSlider()
    {
        return $this->hasOne('Modules\Mediazer\Entities\Gallery','id','slider');
    }

}