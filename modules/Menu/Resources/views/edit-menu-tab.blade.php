@section('panel.script')

@append

<strong>Select Menu</strong><br/>
<div class="form-group">
    @if(!$menus->isEmpty())
        {!! Form::select('menu_list', [''=>'Please Select Menu']+$menus->lists('name','id')->toArray() , $menu->id , ['class' => 'form-control']) !!}
    @else
        <div class="not-found">
            Menu Not Found! <br/>Please Create Menu.
        </div>
    @endif
</div>
<div class="clearfix"></div>
@if($menu)
<div class="form-group">
    <button class="btn btn-primary btn-block" type="button" data-toggle="collapse" data-target="#createLinkExternal" aria-expanded="false">
        External Link
    </button>
    <div class="collapse" id="createLinkExternal">
        <div class="well">
            {!! Form::open(['method' => 'post','class'=>'new-link']) !!}
            <div class="form-group">
                <label for="name">Name</label>
                {!! Form::text('name', null, ['class' => 'form-control','data-validetta'=>'required,minLength[3]']) !!}
            </div>
            <div class="form-group">
                <label for="url">External Url</label>
                {!! Form::text('url', null, ['class' => 'form-control','data-validetta'=>'required']) !!}
            </div>
            <div class="form-group">
                <label for="suffix">Suffix</label>
                {!! Form::text('suffix', null, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
                <label for="description" class="control-label">description</label>
                {!! Form::textarea('description', null, ['class' => 'form-control','placeholder'=>'Menu description','size'=>'3x4','data-validetta'=>'' ]) !!}
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <div class="mediazer" data-input="cover" data-select="single" data-source="id"></div>
                    </div>
                </div>
                <div class="col-md-12">
                    @if($menu->cover)
                        <label>
                            {!! Form::checkbox('rm_image', '1', null,  ['id' => 'rm_image']) !!}Remove Cover Image
                        </label><br/>
                        <img class="img-responsive thumbnail MZ-view-cover" src="{{ Helpers::image($menu->Cover->FullPath,150,150) }}"  />
                    @else
                        <img class="img-responsive thumbnail MZ-view-cover" src="holder.js/150x150?random=yes&text=No-Photo" alt=""/>
                    @endif
                </div>
            </div>
            <div class="form-group">
                {!! Form::submit('Create Link', ['class' => 'btn btn-success pull-right']) !!}
            </div>
            {!! Form::hidden('type', 'external') !!}
            {!! Form::hidden('menu', $menu->id) !!}
            {!! Form::close() !!}
            <div class="clearfix"></div>
        </div>
    </div>
</div>


<button class="btn btn-success btn-block" type="button" data-toggle="collapse" data-target="#createLinkPage" aria-expanded="false">
    Page Link
</button>
<div class="collapse" id="createLinkPage">
    <div class="well">
        {!! Form::open(['method' => 'post','class'=>'new-link']) !!}
        <div class="form-group">
            <label for="name">Name</label>
            {!! Form::text('name', null, ['class' => 'form-control','data-validetta'=>'required,minLength[3]']) !!}
        </div>
        <div class="form-group">
            <label for="url">Select Page</label>
            {!! Form::select('page', [''=>'Please Select Page']+$pages->toArray() , null , ['class' => 'form-control','data-validetta'=>'required']) !!}
        </div>
        <div class="form-group">
            <label for="suffix">Suffix</label>
            {!! Form::text('suffix', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            <label for="description" class="control-label">description</label>
            {!! Form::textarea('description', null, ['class' => 'form-control','placeholder'=>'Menu description','size'=>'3x4','data-validetta'=>'' ]) !!}
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <div class="mediazer" data-input="cover" data-select="single" data-source="id"></div>
                </div>
            </div>
            <div class="col-md-12">
                @if($menu->cover)
                    <label>
                        {!! Form::checkbox('rm_image', '1', null,  ['id' => 'rm_image']) !!}Remove Cover Image
                    </label><br/>
                    <img class="img-responsive thumbnail MZ-view-cover" src="{{ Helpers::image($menu->Cover->FullPath,150,150) }}"  />
                @else
                    <img class="img-responsive thumbnail MZ-view-cover" src="holder.js/150x150?random=yes&text=No-Photo" alt=""/>
                @endif
            </div>
        </div>
        <div class="form-group">
            {!! Form::submit('Create Link', ['class' => 'btn btn-success pull-right']) !!}
        </div>
        {!! Form::hidden('type', 'page') !!}
        {!! Form::hidden('menu', $menu->id) !!}
        {!! Form::close() !!}
        <div class="clearfix"></div>
    </div>
</div>
@endif


