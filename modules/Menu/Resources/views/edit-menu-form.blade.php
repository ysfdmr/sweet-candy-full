@section('panel.script')

@append
<?php
function recursive_menu($menus , $parent = 0  , $depth = 5 ,  $i = 0  ,  $submenu = NULL , $nested = FALSE ){
    if(empty($menus))
        return;
    if (!$nested){
        foreach($menus as $row):
            $items[$row->parent][] = $row;
        endforeach;
    }else{
        $items  = $menus;
    }
    foreach($items[$parent] as $menuItem){
        $depthStr = str_repeat('&nbsp;',($i*$depth));
        $submenu .= '<li data-id="'.$menuItem->id.'" class="dd-item dd3-item">'.
                '<div class="dd-handle dd3-handle">Drag</div>'.
                '<a class="dd-action action-delete" data-id="'.$menuItem->id.'"> Delete </a>'.
                '<div class="dd3-content">'.$menuItem->name.'</div>'.PHP_EOL;
        if (isset($items[$menuItem->id]))
        {
            $submenu .= '<ol class="dd-list">'.PHP_EOL;
            $submenu = recursive_menu($items , $menuItem->id  , $depth , ($i+1) , $submenu , TRUE);
            $submenu .= '</ol>'.PHP_EOL;
        }
        $submenu    .= '</li>'.PHP_EOL;
    }
    return $submenu;
}
?>

{!! Form::model($menu ,['url' => ['/admin/menu',$menu->id], 'method' => 'post', 'id'=>'edit-menu']) !!}
<div class="row">
    <div class="col-lg-6">
        <div class="form-group">
            <label for="name" class="control-label">Menu Name</label>
            {!! Form::text('name', null, ['class' => 'form-control','placeholder'=>'Menu Name','data-validetta'=>'required,minLength[3]' ]) !!}
        </div>

        <div class="form-group">
            <label for="suffix" class="control-label">Menu Suffix</label>
            {!! Form::text('suffix', null, ['class' => 'form-control','placeholder'=>'Menu Name','data-validetta'=>'' ]) !!}
        </div>

    </div>
    <div class="col-lg-6">
        <div class="form-group">
            <label for="description" class="control-label">Menu description</label>
            {!! Form::textarea('description', null, ['class' => 'form-control','placeholder'=>'Menu description','size'=>'3x4','data-validetta'=>'' ]) !!}
        </div>
    </div>
</div>

<div class="clearfix"></div>
<hr/>

@if($menu->Items->isEmpty())
    Please Add First Link
@endif
<div class="dd" id="nestableMenu">
    <ol class="dd-list">
        @if(!$menu->Items->isEmpty())
        {!! recursive_menu($menu->Items) !!}
        @endif
    </ol>
</div>

{!! Form::hidden('menu', $menu->id) !!}
{!! Form::close() !!}



