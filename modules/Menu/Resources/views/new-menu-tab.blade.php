@section('panel.script')

@append
{{--CONTENT SECTION--}}
<form id="CreateMenuForm">
    <div class="row">
        <div class="col-lg-12">
            <div class="form-group">
                <label for="name" class="control-label">Menu Name</label>
                {!! Form::text('name', null, ['class' => 'form-control','placeholder'=>'Menu Name','data-validetta'=>'required,minLength[3]' ]) !!}
            </div>

            <div class="form-group">
                <label for="description" class="control-label">Menu description</label>
                {!! Form::textarea('description', null, ['class' => 'form-control','placeholder'=>'Menu description','size'=>'3x5','data-validetta'=>'' ]) !!}
            </div>

            <div class="form-group">
                <label for="suffix" class="control-label">Menu Suffix</label>
                {!! Form::text('suffix', null, ['class' => 'form-control','placeholder'=>'Menu Name','data-validetta'=>'' ]) !!}
            </div>


            <div class="form-group">
                <button type="submit" class="form-control btn btn-success">Create Menu</button>
            </div>
        </div>
    </div>
</form>

{{--CONTENT SECTION END--}}






