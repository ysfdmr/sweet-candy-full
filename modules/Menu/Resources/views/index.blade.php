@extends('admin::layouts.master')

@section('panel.header')
    Menu<small> Manage Menu</small>
@stop

@section('panel.style')
    <link rel="stylesheet" href="/assets/admin/plugins/mediazer/mediazer.css"/>
@stop

@section('panel.script')
    <script src="/assets/admin/plugins/mediazer/mediazer-components.js"></script>
    <script src="/assets/admin/plugins/mediazer/mediazer.js"></script>
    <script src="/assets/global/plugins/nested/jquery.nestable.js"></script>
    <script src="/assets/admin/js/menu.js"></script>
@stop

@section('panel.content')
    <div class="row">
        <div class="col-lg-3">
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    <li class="active"><a href="#menu-list" data-toggle="tab" aria-expanded="true">Edit Menu</a></li>
                    <li class=""><a href="#create" data-toggle="tab" aria-expanded="false">Create Menu</a></li></ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="menu-list">
                        @include('menu::edit-menu-tab')
                    </div>
                    <!-- /.tab-pane -->
                    <div class="tab-pane" id="create">
                        @include('menu::new-menu-tab')
                    </div>
                    <!-- /.tab-pane -->
                </div>
                <!-- /.tab-content -->
            </div>
        </div>

        <div class="col-lg-9">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Menu List</h3>
                    @if($menu)
                        <button type="submit" form="edit-menu" class="btn btn-success pull-right">Save</button>
                        <button type="button" id="delete-menu" data-id="{{ $menu->id }}" class="btn btn-danger pull-right" style="margin-right: 15px;">Delete Menu</button>
                    @endif
                </div><!-- /.box-header -->
                <div class="box-body">
                    <div id="menu-edit">
                        @if(!$menu)
                            <h1>Please Select Menu</h1>
                        @else
                            @include('menu::edit-menu-form')
                        @endif
                    </div>
                </div><!-- /.box-body -->
            </div>
        </div>
    </div>


@stop