<?php

Route::group(['prefix' => '/admin','middleware'=>['auth'], 'namespace' => 'Modules\Menu\Http\Controllers'], function()
{
	Route::controller('/menu', 'MenuController');
});