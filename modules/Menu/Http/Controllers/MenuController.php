<?php namespace Modules\Menu\Http\Controllers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Input;
use Modules\Menu\Entities\Menu;
use Modules\Menu\Entities\MenuLink;
use Modules\Menu\Http\Requests\MenuRequest;
use Modules\Page\Entities\Page;
use Pingpong\Modules\Routing\Controller;

class MenuController extends Controller {
	
	public function getIndex()
	{
        $menus=Menu::all();
        $pages=Page::lists('title','id');
        $menu =Menu::where('id',Input::get('edit'))->first();
        return view('menu::index',compact('menus','pages','menu'));
	}


    /**
    Create Link Methods
     **/
    public function postCreateLink(MenuRequest $request)
    {
        $link = new MenuLink();
        $link->menu=Input::get('menu');
        $link->type=Input::get('type');
        $link->parent=0;
        $link->name=Input::get('name');
        $link->description=Input::get('description');
        if(Input::get('cover') != null){
            $link->cover = Input::get('cover');
        }else{
            $link->cover = null;
        }
        $link->suffix=Input::get('suffix');

        switch(Input::get('type'))
        {
            case 'external':{
                $link->url=Input::get('url');
                $link->save();
            }
            case 'page':{
                $link->page=Input::get('page');
                $link->save();
            }
            default:break;
        }


        return response()->Json([
            'message'   => 'Success Add New Link.',
            'item_name' => Input::get('name'),
            'item_id' => $link->id
        ],200);
    }


    public function postDeleteItem()
    {
        if( $menu=MenuLink::where('id',Input::get('item'))->first() ){
            $menu->delete();
            MenuLink::where('parent',Input::get('item'))->delete();
            return response()->Json([
                'message'   => 'Success Deleted Link.'
            ],200);
        }else{
            return response()->Json([
                'message'   => 'Link Not Found!'
            ],422);
        }
    }

    /**
        Create Menu Methods
     **/

    public function postCreateMenu(MenuRequest $request)
    {
        $menu = new Menu();
        $menu->name=Input::get('name');
        $menu->description=Input::get('description');
        $menu->suffix=Input::get('suffix');
        $menu->locale_id= app('app.Languages')->where('language',App::getLocale())->first()->id;
        $menu->save();
        return response()->Json([
            'message'   => 'Success Create Menu.'
        ],200);
    }

    public function postSaveMenu()
    {
        $menu = Menu::find(Input::get('menu'));
        $menu->name=Input::get('name');
        $menu->description=Input::get('description');
        $menu->suffix=Input::get('suffix');
        if($menu->save()){
            //save links
            if(Input::has('menu') && Input::has('serialize')){

                //echo Input::get('menu_serialize');exit;
                $jsonDecoded = json_decode(stripslashes(Input::get('serialize')), true);
                $readbleArray = $this->parseJsonArray($jsonDecoded);
                // Loop through the "readable" array and save changes to DB
                foreach ($readbleArray as $key => $value) {
                    // $value should always be an array, but we do a check
                    if (is_array($value)) {
                        // Update DB
                        $item=MenuLink::where('menu',Input::get('menu'))->where('id',$value['id'])->first();
                        if($item){
                            $item->order=$key;
                            $item->parent=$value['parentID'];
                            $item->save();
                        }
                    }
                }
                return response()->Json([
                    'message'   => 'Success Save Menu.'
                ],200);
            }else{
                return response()->Json([
                    'message'   => 'Please Check Serialize Value.'
                ],422);
            }
        }
    }


    public function postDeleteMenu()
    {
        if( $menu=Menu::where('id',Input::get('item'))->first() ){
            $menu->delete();
            return response()->Json([
                'message'   => 'Success Deleted Menu.'
            ],200);
        }else{
            return response()->Json([
                'message'   => 'Menu Not Found!'
            ],422);
        }
    }

    function parseJsonArray($jsonArray, $parentID = 0)
    {
        $return = array();
        foreach ($jsonArray as $subArray) {
            $returnSubSubArray = array();
            if (isset($subArray['children'])) {
                $returnSubSubArray = $this->parseJsonArray($subArray['children'], $subArray['id']);
            }
            $return[] = array('id' => $subArray['id'], 'parentID' => $parentID);
            $return = array_merge($return, $returnSubSubArray);
        }
        return $return;
    }
}