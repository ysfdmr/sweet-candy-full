<?php namespace Modules\Menu\Entities;
   
use bmcms\scopes\languageTrait;
use Illuminate\Database\Eloquent\Model;

class Menu extends Model {

    protected $table = 'menu';
    public $timestamps = true;
    protected $fillable = [];

    use languageTrait;

    public function Locale()
    {
        return $this->belongsTo('Modules\Admin\Entities\Locale','locale_id','id');
    }

    public function Cover()
    {
        return $this->belongsTo('Modules\Mediazer\Entities\Mediazer','cover','id');
    }

    public function Items()
    {
        return $this->hasMany('Modules\Menu\Entities\MenuLink','menu','id')->orderBy('order');
    }
}