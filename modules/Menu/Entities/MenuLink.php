<?php namespace Modules\Menu\Entities;
   
use bmcms\scopes\languageTrait;
use Illuminate\Database\Eloquent\Model;

class MenuLink extends Model {

    protected $table = 'links';
    public $timestamps = true;
    protected $fillable = [];


    public function Cover()
    {
        return $this->belongsTo('Modules\Mediazer\Entities\Mediazer','cover','id');
    }

    public function Page()
    {
        return $this->belongsTo('Modules\Page\Entities\Page','page','id');
    }

}