<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminSetupTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('locales', function(Blueprint $table)
        {
            $table->increments('id');
            $table->timestamps();

            $table->string('language',2)->unique();
            $table->string('name')->nullable();
            $table->boolean('status')->default(true);
        });

        Schema::create('settings', function(Blueprint $table)
        {
            $table->increments('id');
            $table->timestamps();

            $table->string('key');
            $table->text('value')->nullable()->default(null);

            $table->integer('locale_id')->unsigned()->index()->nullable();
            $table->foreign('locale_id')->references('id')->on('locales')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

        Schema::drop('locales');
        Schema::drop('settings');

    }

}
