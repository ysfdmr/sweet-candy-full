<?php namespace Modules\Admin\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Admin\Entities\Locale;

class AdminDatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();
        Locale::create(array(
            'language'  => 'en',
            'name'      => 'English',
            'status'    => 1
        ));

        Locale::create(array(
            'language'  => 'tr',
            'name'      => 'Türkçe',
            'status'    => 1
        ));

        $this->command->info('AdminDatabaseSeeder table seeded!');
		// $this->call("OthersTableSeeder");
	}

}