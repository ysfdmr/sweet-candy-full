<?php namespace Modules\Admin\Entities;
   
use Illuminate\Database\Eloquent\Model;

class Setting extends Model {

    protected $table = 'settings';
    public $timestamps = true;
    protected $fillable = [];

}