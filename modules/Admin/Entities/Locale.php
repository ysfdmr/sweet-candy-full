<?php namespace Modules\Admin\Entities;
   
use Illuminate\Database\Eloquent\Model;

class Locale extends Model {

    protected $table = 'locales';
    public $timestamps = true;
    protected $fillable = ['language','name','status'];

}