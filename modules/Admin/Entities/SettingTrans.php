<?php namespace Modules\Admin\Entities;
   
use bmcms\scopes\languageTrait;
use Illuminate\Database\Eloquent\Model;

class SettingTrans extends Model {

    protected $table = 'settings';
    public $timestamps = true;
    protected $fillable = [];
    use languageTrait;
}