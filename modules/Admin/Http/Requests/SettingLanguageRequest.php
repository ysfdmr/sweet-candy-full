<?php namespace Modules\Admin\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SettingLanguageRequest extends FormRequest {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
    public function rules()
    {
        //$user = User::find($this->users);

        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                return [
                    'name'=> 'required',
                    'language'=> 'required'
                ];
            }
            case 'PUT':
            case 'PATCH':
            {
                return [
                    'name'=> 'required',
                    'language'=> 'required'
                ];
            }
            default:break;
        }

    }

    /**
     * Get the validation custom messages.
     *
     * @return array
     */

    public function messages()
    {
        return [
            'name.required'=> 'Name is required.',
        ];
    }

}
