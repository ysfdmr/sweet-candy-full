<?php

use Intervention\Image\Facades\Image as Image;
Class Helpers {

    public static function get_gravatar( $email, $s = 80, $d = 'mm', $r = 'g', $img = false, $atts = array() ) {
        $url = 'http://www.gravatar.com/avatar/';
        $url .= md5( strtolower( trim( $email ) ) );
        $url .= "?s=$s&d=$d&r=$r";
        if ( $img ) {
            $url = '<img src="' . $url . '"';
            foreach ( $atts as $key => $val )
                $url .= ' ' . $key . '="' . $val . '"';
            $url .= ' />';
        }
        return $url;
    }

    public static function getImage($imgObj,$w=null,$h=null,$type='resize',$quality=90){
        if($imgObj){
            if (!file_exists(public_path().'/uploads/resized')) {
                mkdir(public_path().'/uploads/resized', 0777, true);
            }elseif((!file_exists(public_path().'/uploads/'.$imgObj->FullPath))){
                if($w == null or $h==null){
                    return "http://placehold.it/800x600?text=image+not+found";
                }else{
                    return "http://placehold.it/".$w."x".$h."?text=image+not+found";
                }
            }else{
                if($w == null or $h==null){
                    //ORIGINAL
                    return '/uploads/'.$imgObj->FullPath;
                }elseif($type=='resize'){
                    //RESIZED
                    $resizedFileName='resize-'.$w.'x'.$h.'-'.$quality.'-'.$imgObj->file_name;
                    $resizedFullPath=public_path().'/uploads/resized/'.$resizedFileName;
                    if (!file_exists($resizedFullPath)) {
                        //Resized İmage Not Found!
                        @Image::make(public_path().'/uploads/'.$imgObj->FullPath)->resize($w,$h)->save($resizedFullPath,$quality);
                    }
                    return '/uploads/resized/'.$resizedFileName;


                }elseif($type=='crop'){
                    //RESIZED
                    $resizedFileName='crop-'.$w.'x'.$h.'-'.$quality.'-'.$imgObj->file_name;
                    $resizedFullPath=public_path().'/uploads/resized/'.$resizedFileName;
                    if (!file_exists($resizedFullPath)) {
                        //Resized İmage Not Found!
                        Image::make(public_path().'/uploads/'.$imgObj->FullPath)->crop($w,$h)->save($resizedFullPath,$quality);
                    }
                    return '/uploads/resized/'.$resizedFileName;
                }
            }
        }else{
            if($w == null or $h==null){
                return "http://placehold.it/800x600?text=image+not+found";
            }else{
                return "http://placehold.it/".$w."x".$h."?text=image+not+found";
            }
        }

    }


    public static function getImageUrl($imgObj,$w=null,$h=null,$type='resize',$quality=90,$return=false){
        if($imgObj){
            $imgObj=end(explode('/',$imgObj));
            //print_r($imgObj);exit;
            if (!file_exists(public_path().'/uploads/resized')) {
                mkdir(public_path().'/uploads/resized', 0777, true);
            }elseif((!file_exists(public_path().'/uploads/'.$imgObj))){
                if($return){
                    if($w == null or $h==null){
                        return "http://placehold.it/800x600?text=image+not+found";
                    }else{
                        return "http://placehold.it/".$w."x".$h."?text=image+not+found";
                    }
                }else{
                    return "";
                }
            }else{
                if($w == null or $h==null){
                    //ORIGINAL
                    return '/uploads/'.$imgObj;
                }elseif($type=='resize'){
                    //RESIZED
                    $resizedFileName='resize-'.$w.'x'.$h.'-'.$quality.'-'.$imgObj;
                    $resizedFullPath=public_path().'/uploads/resized/'.$resizedFileName;
                    if (!file_exists($resizedFullPath)) {
                        //Resized İmage Not Found!
                        @Image::make(public_path().'/uploads/'.$imgObj)->resize($w,$h)->save($resizedFullPath,$quality);
                    }
                    return '/uploads/resized/'.$resizedFileName;


                }elseif($type=='crop'){
                    //RESIZED
                    $resizedFileName='crop-'.$w.'x'.$h.'-'.$quality.'-'.$imgObj;
                    $resizedFullPath=public_path().'/uploads/resized/'.$resizedFileName;
                    if (!file_exists($resizedFullPath)) {
                        //Resized İmage Not Found!
                        Image::make(public_path().'/uploads/'.$imgObj)->crop($w,$h)->save($resizedFullPath,$quality);
                    }
                    return '/uploads/resized/'.$resizedFileName;
                }
            }
        }else{
            return "";
        }

    }


    public static function image($path,$w=null,$h=null,$type='resize'){
        if($w == null or $h==null){
            return '/uploads/'.$path;
        }elseif($type=='resize'){
            return '/photo/'.$w.'/'.$h.'/uploads/'.$path;
        }elseif($type=='crop'){
            return '/photo/crop/'.$w.'/'.$h.'/uploads/'.$path;
        }
    }

    public static function getPageByModuleName($module_name){
        //return  Cache::remember('helpers.'.$module_name.App::getLocale(), 60, function() use($module_name) {
            return \Modules\Page\Entities\Page::with('getSlider.Items.Image')->where('id',app('app.settingTrans')->where('key',Theme::getActive().'_'.$module_name.'_page')->first()->value)->first();
        //});
    }

    public static function getPageSlugById($id){
        return \Modules\Page\Entities\Page::findBySlugOrId($id)->slug;
    }

    public static function getPageByID($id){
        //return  Cache::remember('helpers.'.$module_name.App::getLocale(), 60, function() use($module_name) {
        return \Modules\Page\Entities\Page::with('getSlider.Items.Image')->where('id',$id)->first();
        //});
    }

    public static function getHomePage(){
        //return  Cache::remember('helpers.homepage'.App::getLocale(), 60, function() {
            return \Modules\Page\Entities\Page::with('getSlider.Items.Image')->where('id',app('app.settingTrans')->where('key','home')->first()->value)->first();
        //});
    }

    public static function getGallery($galleryID){
        //return  Cache::remember('helpers.gallery'.App::getLocale(), 60, function() use($galleryID) {
            return \Modules\Mediazer\Entities\Gallery::with('Items.Image')->where('id',$galleryID)->first();
        //});
    }

    public static function getCustomField($obj,$name){
        return $obj->where('key',$name)->first()->value;
    }

    public static function getSettings($key,$default=null){
        $value=app('app.setting')->where('key',$key)->first()->value;
        if(!empty($value)){
            return $value;
        }else{
            return $default;
        }
    }

    public static function getSettingTrans($key){
        return app('app.settingTrans')->where('key',$key)->first()->value;
    }

    public static function getMenuBySettingsKey($key){
        return \Modules\Menu\Entities\Menu::find(app('app.settingTrans')->where('key',$key)->first()->value);
    }

    public static function getModuleList($module_name,$paginate=null){
        if($paginate==null){
            return \Modules\Module\Entities\CustomModule::with('Gallery.Image','CustomField')->where('module',$module_name)->get();
        }else{
            return \Modules\Module\Entities\CustomModule::with('Gallery.Image','CustomField')->where('module',$module_name)->paginate($paginate);
        }
    }

    /**
     * Recursive Function Available Options
     * Depth
     * i
     * sub-menu-list
     * sub-menu-id
     * sub-menu-class
     * menu-attr
     * sub-menu-attr
     * 'page-path'=>'',
     * @param $menus
     * @param array $options
     * @param int $parent
     * @param null $menu
     * @param bool $nested
     * @return null|string
     * @internal param null $submenu
     */
    public  static function recursive_menu($menus ,$options=[],$parent = 0 ,$menu = NULL ,$nested = FALSE){
            $pagePath       =empty($options['page-path'])?'/page/':$options['page-path'];
            $liClass        =empty($options['li-class'])?'parent':"class='".$options['li-class']."'";
            $subMenuLiClass =empty($options['sub-menu-li-class'])?'sub':"class='".$options['sub-menu-li-class']."'";
            $linkClass      =empty($options['link-class'])?null:"class='".$options['link-class']."'";
            $subMenuList    =in_array($options['sub-menu-list'],['ul','ol'])?$options['sub-menu-list']:'ul';
            $subMenuId      =empty($options['sub-menu-id'])?null:"id='".$options['sub-menu-id']."'";
            $subMenuClass   =empty($options['sub-menu-class'])?null:"class='".$options['sub-menu-class']."'";
            $image          =$options['image']==true?:false;
            $description    =$options['description']==true?:false;

            if($nested==false){
                if($menus->isEmpty()){
                    //echo "<li>Please Add First Link</li>";
                    return;
                }
            }
            if (!$nested){
                foreach($menus as $row):
                    $items[$row->parent][] = $row;
                endforeach;
            }else{$items  = $menus;}
            foreach($items[$parent] as $item){
                //$depthStr = str_repeat('&nbsp;',($options['i']*$options['depth']));
                $class=$nested?$subMenuLiClass:$liClass ;
                $menu .="<li ".$class.">";
                if($item->type=='external'){// link types create link
                    $menu .="<a href='$item->url' $linkClass>";
                }elseif($item->type=='page'){
                    $slug=$pagePath.self::getPageSlugById($item->page);
                    $menu .="<a href='".$slug."' $linkClass >";
                }
                // menu content
                $menu.="<span class='link-name'>".$item->name."</span>";
                if($nested){
                    if($image && $item->Cover){
                        $menu.="<img class='link-img' src='".self::image($item->Cover->FullPath,340,200)."' alt='$item->name'>";
                    }
                    if($description){
                        $menu.="<span class='link-description'> $item->name </span>";
                    }
                }
                $menu.="</a>";// end link
                if (isset($items[$item->id])){
                    $menu .= '<'.$subMenuList.' '.$subMenuId.' '.$subMenuClass.'>';
                    $menu = self::recursive_menu($items ,$options, $item->id , $menu , TRUE );
                    $menu .= "</$subMenuList>".PHP_EOL;
                }
                $menu.= '</li>'.PHP_EOL;
            }
            return $menu;
    }

    /**
     * Script Unique

    public static $js=[];
    public static $css=[];
    public static function js($src){
        if(!in_array($src,self::$js)){
            array_push(self::$js,$src);
        }
    }
    public static function css($src){
        if(!in_array($src,self::$css)){
            array_push(self::$css,$src);
        }
    }

    public static function getJs(){
        return self::$js;
    }

    public static function getCss(){
        return self::$css;
    }
    */
}