<?php namespace Modules\Admin\Http\Controllers;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Modules\Admin\Http\Requests\SettingLanguageRequest;
use Pingpong\Modules\Routing\Controller;
use Modules\Admin\Entities\Locale;

class LocaleController extends Controller {
	
	public function index()
	{
        $languages=Locale::all();
		return view('admin::setting-language.index',compact('languages'));
	}

    public function create()
    {
        return view('admin::setting-language.create');
    }

    public function store(SettingLanguageRequest $request)
    {
        $language= new Locale();
        $language->language = Input::get('language');
        $language->name     = Input::get('name');
        $language->status   = Input::get('status');
        $language->save();
        //Cache::forget('app.Languages');
        Cache::flush();
        return Redirect::to('admin/setting-language/'.$language->id.'/edit')
            ->with('message',  array('type' => 'success', 'head' => 'Info Message!', 'text' => 'Create Success'));
    }

    public function edit($id)
    {
        $language=Locale::find($id);
        return view('admin::setting-language.edit',compact('language'));
    }

    public function update(SettingLanguageRequest $request,$id)
    {
        $language= Locale::find($id);
        $language->language     = Input::get('language');
        $language->name     = Input::get('name');
        $language->status   = Input::get('status');
        $language->save();
        //Cache::forget('app.Languages');
        Cache::flush();
        return Redirect::to('admin/setting-language/'.$language->id.'/edit')
            ->with('message',  array('type' => 'success', 'head' => 'Info Message!', 'text' => 'Update Success'));
    }

    public function destroy($id)
    {
        if(Locale::where('status',true)->get()->count()<=1){
            return Redirect::to('admin/setting-language/')
                ->with('message', array('type' => 'success', 'head' => 'Info Message!', 'text' => 'Last Language Not Deleted !'));
        }else{
            Locale::find($id)->delete();
            //Cache::forget('app.Languages');
            Cache::flush();
            return Redirect::to('admin/setting-language/')
                ->with('message', array('type' => 'success', 'head' => 'Info Message!', 'text' => 'Delete Success'));
        }
    }
}