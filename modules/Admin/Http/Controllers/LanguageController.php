<?php namespace Modules\Admin\Http\Controllers;

use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Session;
use Modules\Admin\Entities\Locale;
use Pingpong\Modules\Routing\Controller;

class LanguageController extends Controller {

    public function switchLanguage($lang)
    {

        $avaliable =Locale::lists('language')->toArray();
        if(in_array($lang,$avaliable)){
            Session::set('language',$lang);
            return Redirect::to('/')
                ->with('message',['type'=>'success','head'=>'','text'=>'Success Change Language']);
        }else{
            return Redirect::back()
                ->with('message',['type'=>'danger','head'=>'Info Message','text'=>'Error Change Language']);
        }

    }

}