<?php namespace Modules\Admin\Http\Controllers;

use Caffeinated\Themes\Facades\Theme;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Input;
use Modules\Admin\Entities\Setting;
use Modules\Admin\Entities\SettingTrans;
use Modules\Blog\Entities\Post;
use Modules\Location\Entities\Location;
use Modules\Mediazer\Entities\Gallery;
use Modules\Menu\Entities\Menu;
use Modules\Module\Entities\CustomModule;
use Modules\Page\Entities\Page;
use Pingpong\Modules\Routing\Controller;

class SettingController extends Controller {
	
	public function getIndex()
	{
        $themes=Theme::all();
        $settings=Setting::all();
        $settingTrans=SettingTrans::all();
        /*Sources*/
        $source=[];
        $pages=Page::where('status',true)->lists('title','id');
        $gallery=Gallery::lists('name','id');
        $blogPosts=Post::lists('title','id');
        $menu=Menu::lists('name','id');
        $locations=Location::lists('title','id');
        $source['pages']=$pages;
        $source['gallery']=$gallery;
        $source['blog']=$blogPosts;
        $source['menu']=$menu;
        $source['location']=$locations;

        foreach(\ThemeComponents::getModuleNames() as $modul){
            $source[$modul]=CustomModule::with('Gallery.Image')->where('module',$modul)->lists('title','id');
        }
        //dd($source);exit;
        return view('admin::setting.index',compact('settings','pages','settingTrans','themes','gallery','source'));
	}

    public function postIndex()
    {
        /*debug
            echo $key."<br>".$value;
            dd(Input::get('setting'));
            exit;
        */
        if(Input::get('setting')['global']){
            foreach(Input::get('setting')['global'] as $key => $value){
                if(!$setting = Setting::where('key',$key)->first()){
                    $setting = new Setting();
                    $setting->key=$key;
                    $setting->locale_id=null;
                }
                $setting->value=$value;
                $setting->save();
            }
        }
        if(Input::get('setting')['locale']) {
            foreach(Input::get('setting')['locale'] as $key => $value){
                if(!$setting = Setting::where('key',$key)
                    ->where('locale_id',app('app.Languages')
                    ->where('language',App::getLocale())->first()->id)
                    ->first()
                ){
                    $setting = new Setting();
                    $setting->key=$key;
                    $setting->locale_id=app('app.Languages')->where('language',App::getLocale())->first()->id;
                }
                $setting->value=$value;
                $setting->save();
            }
        }
        //Cache::forget('app.setting');
        //Cache::forget('app.settingTrans');
        Cache::flush();
        return redirect('admin/setting');
    }

}