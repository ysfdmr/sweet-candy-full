<?php
$themeSettings=public_path().'/themes/'.\Caffeinated\Themes\Facades\Theme::getActive().'/Settings.php';
if (file_exists($themeSettings)) {
    require_once $themeSettings;
}
Class ThemeComponents {

    /**
     * Set Settings Fields Functions
     */
    public static $sourceModuleList=['pages','gallery','blog','menu','location'];
    public static $settingFields=[];

    public static function setSettingField($trans='global',$type='text',$name,$view,$class=[],$source=null){
        if(in_array($trans,['global','locale'])){
            $regex = "/^[a-zA-Z][a-zA-Z0-9_]+$/";
            if($name!=null && $view!=null && !@preg_match($regex, $name) === false){
                if(in_array($type,['text','select','textarea','mediazer'])){
                    /*SUCCESS VALIDATION*/

                    /*Classes To String*/
                    if(!empty($class) && is_array($class)){
                        $class = implode(" ", $class);
                    }
                    /*Source Validation*/
                    if($type=='select'){
                        /*
                        if(!empty($source) && !is_array($source) && !in_array($source,['pages','gallery'])){
                            $source=null;
                        }elseif(is_array($source)){
                            $source=[null=>'Please select '.$view]+$source;
                        }*/
                        if(empty($source)){
                            $source=[null=>'1-Please select '.$view];
                        }else{
                            $sourceList=self::$sourceModuleList;
                            foreach(self::getModuleNames() as $module){
                                array_push($sourceList,$module);
                            }
                            if(!is_array($source)){
                                if(!in_array($source,$sourceList)){
                                    $source=[null=>$view.' <-- Source Not Found.'];
                                }
                            }else{
                                $source=[null=>'3-Please select '.$view]+$source;
                            }
                        }
                    }

                    if($type=='mediazer'){

                    }
                    /*Old Input Source*/
                    $old=$trans=='global'?'settings':'settingTrans';
                    array_push(self::$settingFields,[
                        'trans'=>$trans,
                        'name'=>\Caffeinated\Themes\Facades\Theme::getActive().'_'.$name,
                        'view'=>$view,
                        'type'=>$type,
                        'class'=>$class,
                        'source'=>$source,
                        'old'=>$old
                    ]);

                }
            }
        }
    }

    public static function getSettingFields(){
        return self::$settingFields;
    }

    /**
     * Set Modules Functions
     */
    public static $Modules=[];

    public static function setModule($name,$view,$fields=[]){
        array_push(self::$Modules,[
            'name'=>$name,
            'view'=>$view,
            'fields'=>$fields
            ]);
    }

    public static function getModules(){
        return self::$Modules;
    }

    public static function getModuleNames(){
        $moduleNames=[];
        foreach(self::$Modules as $module){
            array_push($moduleNames,$module['name']);
        }
        return $moduleNames;
    }

    public static function getModuleFields($moduleName=null){
        $moduleFields=[];
        foreach(self::$Modules as $module){
            if($module['name']==$moduleName){
                foreach($module['fields'] as $field){
                    array_push($moduleFields,$field);
                }
            }
        }

        return $moduleFields;
    }
 /**
 * Blog Custom Fields Functions
 */
    public static $blogCustomFields=[];
    public static function setBlogCustomFields($key){
        if(!empty($key)){
            array_push(self::$blogCustomFields,$key);
        }
    }

    public static function getBlogCustomFields(){
        return self::$blogCustomFields;
    }


    /**
     * Blog Custom Fields Functions
     */

    public static $productCustomFields=[];
    public static function setProductCustomFields($key,$description){
        if(!empty($key)){
            array_push(self::$productCustomFields,['key'=>$key,'description'=>$description]);
        }
    }

    public static function getProductCustomFields(){
        return self::$productCustomFields;
    }

    /**
     * IN COMING ...
     * Global Attrbute Set System
     * $AttributeSetName
     * $type = Select,selectrange,text,textarea,radio,checkbox
     */


}