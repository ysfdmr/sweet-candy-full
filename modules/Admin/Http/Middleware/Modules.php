<?php namespace Modules\Admin\Http\Middleware; 

use Closure;
use Pingpong\Modules\Facades\Module;

class Modules {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $Modules =Module::getByStatus(1);
        view()->share('MODULES',$Modules);
        return $next($request);
    }

}
