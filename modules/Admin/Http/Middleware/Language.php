<?php namespace Modules\Admin\Http\Middleware; 

use Closure;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;

class Language {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $locales=app('app.Languages');
        if(Session::has('language')){
            $lang=Session::get('language');
            if(in_array( $lang,$locales->lists('language')->toArray() ) ){
                App::setlocale($lang);
            }
        }
        return $next($request);
    }
    
}
