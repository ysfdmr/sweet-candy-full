<?php
use \Collective\Html\HtmlFacade;

Shortcode::register('a', function($attr, $content = null, $name = null)
{
    $text = Shortcode::compile($content);
    return '<a'.HtmlFacade::attributes($attr).'>'. $text .'</a>';
});

//$src = array_get($attr, 'type');

Shortcode::register('map', function($attr, $content = null, $name = null)
{
    $text = Shortcode::compile($content);
    return view('admin::shortcode.map',compact('attr','text'));
});


Shortcode::register('blog-gallery', function($attr, $content = null, $name = null)
{
    $text = Shortcode::compile($content);
        return view('admin::shortcode.blog-gallery',compact('attr','text'));
});