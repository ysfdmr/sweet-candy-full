<?php

Route::group(['namespace' => 'Modules\Admin\Http\Controllers'], function()
{
    Route::get('/switch-language/{lang}', 'LanguageController@switchLanguage');

});


Route::group(['prefix' => '/admin','middleware'=>['auth'], 'namespace' => 'Modules\Admin\Http\Controllers'], function()
{
    Route::get('/', 'AdminController@index');
    Route::get('/a/{slug}', 'AdminController@slug');
    Route::controller('/setting','SettingController');
    Route::resource('/setting-language','LocaleController');

});


