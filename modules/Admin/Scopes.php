<?php
namespace bmcms\scopes;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\ScopeInterface;
use Illuminate\Support\Facades\App;

trait languageTrait {
    /*
    public static function bootlanguageTrait()
    {
        static::addGlobalScope(new languageScope());
    }
    */
    public static function boot()
    {
        parent::boot();
        static::addGlobalScope(new languageScope());
    }
}

class languageScope implements ScopeInterface {
    public function apply(Builder $builder, Model $model)
    {
        //$column = $model->getQualifiedPublishedAtColumn();
        $builder->where('locale_id',app('app.Languages')->where('language',App::getLocale())->first()->id );
        //$this->addWithDrafts($builder);
    }

    // Remains unchanged - it's just a helper method
    protected function addWithDrafts(Builder $builder)
    {
        $builder->macro('withDrafts', function (Builder $builder) {
            $this->remove($builder, $builder->getModel());
            return $builder;
        });
    }

    /**
     * Remove the scope from the given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder $builder
     * @param  \Illuminate\Database\Eloquent\Model $model
     *
     * @return void
     */
    public function remove(Builder $builder, Model $model)
    {
        // TODO: Implement remove() method.
    }
}