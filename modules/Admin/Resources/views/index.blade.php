@extends('admin::layouts.master')

@section('panel.content')

<div class="row">
    <div class="col-md-4">
        <!-- Widget: user widget style 1 -->
        <div class="box box-widget widget-user-2">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-aqua">
                <h3 class="widget-user-username">Modules</h3>
                <h5 class="widget-user-desc"> Active Module Lists </h5>
            </div>
            <div class="box-footer no-padding">
                <ul class="nav nav-stacked">
                    <li><a href="#">Your Blog Post<span class="pull-right badge bg-blue">31</span></a></li>
                    <li><a href="#">Your Modules Posts <span class="pull-right badge bg-aqua">5</span></a></li>
                </ul>
            </div>
        </div><!-- /.widget-user -->
    </div>

    <div class="col-md-4">
        <!-- Widget: user widget style 1 -->
        <div class="box box-widget widget-user-2">
            <!-- Add the bg color to the header using any of the bg-* classes -->
            <div class="widget-user-header bg-yellow">
                <div class="widget-user-image">
                    <img class="img-circle" src="{{ Helpers::get_gravatar(Auth::user()->email) }}" alt="User Avatar">
                </div><!-- /.widget-user-image -->
                <h3 class="widget-user-username">{{ Auth::user()->FullName }}</h3>
                <h5 class="widget-user-desc">TODO: user.group</h5>
            </div>
            <div class="box-footer no-padding">
                <ul class="nav nav-stacked">
                    <li><a href="#">Your Blog Post<span class="pull-right badge bg-blue">31</span></a></li>
                    <li><a href="#">Your Modules Posts <span class="pull-right badge bg-aqua">5</span></a></li>
                </ul>
            </div>
        </div><!-- /.widget-user -->
    </div>

    <div class="col-md-4 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-flag-o"></i></span>
            <div class="info-box-content">
                <span class="info-box-text">Bookmarks</span>
                <span class="info-box-number">410</span>
            </div><!-- /.info-box-content -->
        </div><!-- /.info-box -->
    </div>
</div>

@stop