<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
    {{-- Media SOURCES--}}
    <div class="box-header with-border text-center">
        <h2 class="box-title">Media Settings</h2>
    </div>

    <div class="form-group">
        <label for="content" class="control-label">
            Page Logo <img src="{{ $settings->where('key','media_logo')->first()->value }}" width="50" height="50px"/>
        </label>
        <div class="mediazer" data-input="setting[global][media_logo]" data-oldinput="{{ $settings->where('key','media_logo')->first()->value }}" data-select="single" data-source="link"></div>
    </div>

    <div class="form-group">
        <label for="content" class="control-label">{{ Lang::get('global.word.default') }} {{ Lang::get('global.word.gallery') }}</label>
        {!! Form::select('setting[global][gallery]', $gallery , $settings->where('key','gallery')->first()->value , ['class' => 'form-control']) !!}
    </div>






</div>

<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

</div>
<div class="clearfix"></div>
