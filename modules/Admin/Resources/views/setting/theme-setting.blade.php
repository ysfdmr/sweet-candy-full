<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
    {{-- PAGE SOURCES--}}
    <div class="box-header with-border text-center">
        <h2 class="box-title">Theme Settings</h2>
    </div>


    @foreach(ThemeComponents::getSettingFields() as $field)
        <div class="form-group">
            <label for="{{ $field['name'] }}" class="control-label">{{ $field['view'] }}</label>
            @if($field['type']=='select')
                @if(is_array($field['source']))
                    {!! Form::select('setting['.$field['trans'].']['.$field['name'].']', $field['source'] , $$field['old']->where('key',$field['name'])->first()->value , ['class' => $field['class'] ]) !!}
                @else
                    {!! Form::select('setting['.$field['trans'].']['.$field['name'].']', [''=>'Select Source']+$source[$field['source']]->toArray() , $$field['old']->where('key',$field['name'])->first()->value , ['class' => $field['class']]) !!}
                @endif
            @elseif(in_array($field['type'],['text','textarea']))
                {!! Form::$field['type']('setting['.$field['trans'].']['.$field['name'].']', $$field['old']->where('key',$field['name'])->first()->value , ['class' => $field['class'],'placeholder'=>$field['view'] ]) !!}
            @elseif($field['type']=='mediazer')
                <div class="clearfix"></div>
                <img src="{{ $$field['old']->where('key',$field['name'])->first()->value }}" width="50" height="50px"/>
                <div class="mediazer" data-input="setting[{{$field['trans']}}][{{$field['name']}}]" 
                    data-oldinput="{{ $$field['old']->where('key',$field['name'])->first()->value }}" 
                    data-select="single" 
                    data-source="link">
                </div>
            @endif
        </div>
    @endforeach

</div>
<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
    <div class="box-header with-border text-center">
        <h2 class="box-title">Theme Custom Settings</h2>
    </div>
    <div class="form-group">
        <label for="content" class="control-label">Custom CSS</label>
        {!! Form::textarea('setting[global][custom_css]', $settings->where('key','custom_css')->first()->value , ['class' => 'form-control','size'=>'5x10']) !!}
    </div>

    <div class="form-group">
        <label for="content" class="control-label">Custom JS</label>
        {!! Form::textarea('setting[global][custom_js]', $settings->where('key','custom_js')->first()->value , ['class' => 'form-control','size'=>'5x10']) !!}
    </div>
</div>


<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
</div>
<div class="clearfix"></div>
