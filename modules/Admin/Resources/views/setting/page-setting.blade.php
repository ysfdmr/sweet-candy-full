<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
    {{-- PAGE SOURCES--}}
    <div class="box-header with-border text-center">
        <h2 class="box-title">Page & Module Page Sources Settings (**Language Based)</h2>
    </div>

    <div class="form-group">
        <label for="content" class="control-label">{{ Lang::get('global.word.default') }} {{ Lang::get('global.word.homepage') }}</label>
        {!! Form::select('setting[locale][home]', [''=>'Please Select Home Page']+$pages->toArray() , $settingTrans->where('key','home')->first()->value , ['class' => 'form-control','required' => 'required']) !!}
    </div>

    @foreach(ThemeComponents::getModules() as $module)
    <div class="form-group">
        <label for="content" class="control-label" style="text-transform: capitalize">{{ $module['name'] }} {{ Lang::get('global.word.source') }} {{ Lang::get('global.word.page') }}</label>
        {!! Form::select('setting[locale]['.$module['name'].'_page]', [''=>'Please Select '.$module['name'].' Page']+$pages->toArray() , $settingTrans->where('key',$module['name'].'_page')->first()->value , ['class' => 'form-control']) !!}
    </div>
    @endforeach

</div>

<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

</div>
<div class="clearfix"></div>
