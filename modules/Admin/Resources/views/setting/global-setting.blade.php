<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
    <div class="box-header with-border text-center"><h2 class="box-title">Global Settings Settings</h2></div>

    {{--Fields--}}

    <div class="form-group">
        <label for="content" class="control-label">Site {{ Lang::get('global.word.title') }} (**Language Base)</label>
        {!! Form::text('setting[locale][site_title]', $settingTrans->where('key','site_title')->first()->value , ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        <label for="content" class="control-label">{{ Lang::get('global.word.default') }} {{ Lang::get('global.word.language') }}</label>
        {!! Form::select('setting[global][language]', $APP['LOCALE'] , $settings->where('key','language')->first()->value , ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        <label for="content" class="control-label">{{ Lang::get('global.word.default') }} {{ Lang::get('global.word.theme') }}</label>
        <select name="setting[global][theme]" class="form-control">
            @foreach($themes as $theme)
                @if($theme == $settings->where('key','theme')->first()->value)
                    <option value="{{ $theme }}" selected="selected">{{ $theme }}</option>
                @else
                    <option value="{{ $theme }}">{{ $theme }}</option>
                @endif
            @endforeach
        </select>
    </div>

    <div class="box-header with-border text-center"><h2 class="box-title">{{ Lang::get('global.word.company') }} {{ Lang::get('global.word.info') }}</h2></div>
    <div class="form-group">
        <label for="content" class="control-label">{{ Lang::get('global.word.company') }} {{ Lang::get('global.word.name') }}</label>
        {!! Form::text('setting[global][company_name]', $settings->where('key','company_name')->first()->value , ['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        <label for="content" class="control-label">{{ Lang::get('global.word.company') }} {{ Lang::get('global.word.email') }}</label>
        {!! Form::email('setting[global][company_email]', $settings->where('key','company_email')->first()->value , ['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        <label for="content" class="control-label">{{ Lang::get('global.word.company') }} {{ Lang::get('global.word.phone') }}</label>
        {!! Form::text('setting[global][company_phone]', $settings->where('key','company_phone')->first()->value , ['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        <label for="content" class="control-label">{{ Lang::get('global.word.company') }} {{ Lang::get('global.word.fax') }}</label>
        {!! Form::text('setting[global][company_fax]', $settings->where('key','company_fax')->first()->value , ['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        <label for="content" class="control-label">{{ Lang::get('global.word.company') }} {{ Lang::get('global.word.info') }}</label>
        {!! Form::textarea('setting[global][company_info]', $settings->where('key','company_info')->first()->value , ['class' => 'form-control ckeditor']) !!}
    </div>

    <div class="form-group">
        <label for="content" class="control-label">{{ Lang::get('global.word.company') }} {{ Lang::get('global.word.address') }}</label>
        {!! Form::textarea('setting[global][company_address]', $settings->where('key','company_address')->first()->value , ['class' => 'form-control ckeditor']) !!}
    </div>
</div>

<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">



    {{-- MAP START--}}
    <div class="box-header with-border text-center"><h2 class="box-title">{{ Lang::get('global.word.company') }} {{ Lang::get('global.word.location') }}</h2></div>

    <div class="row">
        <div class="col-lg-12">
            <label for="content" class="control-label">Search Address</label>
            <div class="input-group input-group-sm">
                <input type="text" id="address" class="form-control"/>
                <span class="input-group-btn"><button class="btn btn-info btn-flat" type="button" id="btn-address-search">Search!</button></span>
            </div>
            <div id="companyMap" style="width: 100%;height: 300px;"></div>
        </div>

        <div class="col-lg-12">
            <div class="form-group">
                <label for="content" class="control-label">Map Address</label>
                {!! Form::text('setting[global][company_map_address]', $settings->where('key','company_map_address')->first()->value , ['class' => 'form-control']) !!}
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                <label for="content" class="control-label">long</label>
                {!! Form::text('setting[global][company_long]', $settings->where('key','company_long')->first()->value , ['class' => 'form-control']) !!}
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                <label for="content" class="control-label">lat</label>
                {!! Form::text('setting[global][company_lat]', $settings->where('key','company_lat')->first()->value , ['class' => 'form-control']) !!}
            </div>
        </div>
    </div>
    {{-- MAP END--}}

    {{-- Social Start--}}
    <div class="box-header with-border text-center"><h2 class="box-title">{{ Lang::get('global.word.social') }} {{ Lang::get('global.word.links') }}</h2></div>

    <div class="form-group">
        <label for="content" class="control-label">Facebook</label>
        {!! Form::text('setting[global][social_facebook]', $settings->where('key','social_facebook')->first()->value , ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        <label for="content" class="control-label">Twitter</label>
        {!! Form::text('setting[global][social_twitter]', $settings->where('key','social_twitter')->first()->value , ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        <label for="content" class="control-label">Google+</label>
        {!! Form::text('setting[global][social_google]', $settings->where('key','social_google')->first()->value , ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        <label for="content" class="control-label">Youtube</label>
        {!! Form::text('setting[global][social_youtube]', $settings->where('key','social_youtube')->first()->value , ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        <label for="content" class="control-label">Instagram</label>
        {!! Form::text('setting[global][social_instagram]', $settings->where('key','social_instagram')->first()->value , ['class' => 'form-control']) !!}
    </div>

    <div class="form-group">
        <label for="content" class="control-label">Youtube Channel ID</label>
        {!! Form::text('setting[global][social_youtube_channel]', $settings->where('key','social_youtube_channel')->first()->value , ['class' => 'form-control']) !!}
    </div>
    {{-- Social END--}}




</div>

<div class="clearfix"></div>




{{--
<div class="form-group {{ $errors->first('site.title','has-error') }}">
    <label for="title" class="control-label">Site {{ Lang::get('global.word.title') }}</label>
    {!! Form::text('setting[site.title]', null, ['class' => 'form-control','placeholder'=>Lang::get('global.word.title') ]) !!}
    $errors->first('title','<p class="help-block">:message</p>')
</div>

--}}