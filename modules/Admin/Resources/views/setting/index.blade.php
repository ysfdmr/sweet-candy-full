@extends('admin::layouts.master')

@section('panel.style')
    <link rel="stylesheet" href="/assets/modules/blog/css/blog.css"/>
    <link rel="stylesheet" href="/assets/admin/plugins/mediazer/mediazer.css"/>
@stop

@section('panel.script')
    <script src="/assets/admin/plugins/mediazer/mediazer-components.js"></script>
    <script src="/assets/admin/plugins/mediazer/mediazer.js"></script>
    <script src="/assets/global/plugins/ckeditor/ckeditor.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBGWMg9-5Vss0jfxdSrEWEvVBGSu6Ax2nE"></script>
    <script src="/assets/global/plugins/gmap/gmaps.js"></script>
    <script>

        var map = new GMaps({
            div: '#companyMap',
            lat: 41.0082376,
            lng: 28.97835889999999
        });

        $('#address').keypress(function(e) {
            if (e.keyCode == 13) {
                $('#btn-address-search').trigger( "click" );
                return false;
            }
        });

        $('body').on('click', '#btn-address-search', function () {
            GMaps.geocode({
                address: $('#address').val(),
                callback: function(results, status) {
                    if (status == 'OK') {
                        var latlng = results[0].geometry.location;
                        map.setCenter(latlng.lat(), latlng.lng());
                        updateCompanyMapData(results)
                        map.addMarker({
                            lat: latlng.lat(),
                            lng: latlng.lng(),
                            draggable: true,
                            click: function (e) {
                                var latlng = e.position;
                                GMaps.geocode({
                                    location: latlng,
                                    callback: function (results, status) {
                                        updateCompanyMapData(results)
                                    }
                                });
                            },
                            dragend: function (e) {
                                GMaps.geocode({
                                    location: e.latLng,
                                    callback: function (results, status) {
                                        updateCompanyMapData(results)
                                    }
                                });
                            }
                        });
                    }
                }
            });

        });

        function updateCompanyMapData(results){
            var latlng = results[0].geometry.location;
            $('input[name="setting[global][company_map_address]"]').val(results[0].formatted_address);
            $('input[name="setting[global][company_long]"]').val(latlng.lng());
            $('input[name="setting[global][company_lat]"]').val(latlng.lat());
        }
    </script>
@stop

@section('panel.content')
    <div class="row">
        <div class="col-lg-12">
            {!! Form::open(['url' => '/admin/setting', 'method' => 'post', 'class'=>'','files'=>true]) !!}
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">

                    <li class="active">
                        <a href="#global-setting" data-toggle="tab">{{ Lang::get('global.word.global') }}</a>
                    </li>

                    <li>
                        <a href="#theme-setting" data-toggle="tab">{{ Lang::get('global.word.theme') }}</a>
                    </li>

                    <li>
                        <a href="#page-setting" data-toggle="tab">{{ Lang::get('global.word.pages') }}</a>
                    </li>

                    <li>
                        <a href="#media-setting" data-toggle="tab">{{ Lang::get('global.word.media') }}</a>
                    </li>
                    {{--
                    <li>
                        <a href="#location-setting" data-toggle="tab">Location</a>
                    </li>
                    --}}
                    <li>
                        <a href="#meta-setting" data-toggle="tab">Meta</a>
                    </li>

                    <li class="pull-right">
                        <button type="submit" class="btn btn-primary pull-right">{{ Lang::get('global.word.save') }}</button>
                    </li>
                </ul>
                <div class="tab-content">

                    <div class="tab-pane active" id="global-setting">
                        @include('admin::setting.global-setting')
                    </div><!-- /.tab-pane -->

                    <div class="tab-pane" id="theme-setting">
                        @include('admin::setting.theme-setting')
                    </div><!-- /.tab-pane -->

                    <div class="tab-pane" id="page-setting">
                        @include('admin::setting.page-setting')
                    </div><!-- /.tab-pane -->

                    <div class="tab-pane" id="media-setting">
                        @include('admin::setting.media-setting')
                    </div><!-- /.tab-pane -->

                    <div class="tab-pane" id="location-setting">
                        @include('admin::setting.location-setting')
                    </div><!-- /.tab-pane -->

                    <div class="tab-pane" id="meta-setting">
                        @include('admin::setting.meta-setting')
                    </div><!-- /.tab-pane -->

                </div><!-- /.tab-content -->
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-primary pull-right">{{ Lang::get('global.word.save') }}</button>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@stop