<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
    {{-- Media SOURCES--}}
    <div class="box-header with-border text-center">
        <h2 class="box-title">Site Default Meta Settings (**Language Base)</h2>
    </div>

    <div class="form-group">
        <label for="meta_title" class="control-label">Meta Title</label>
        {!! Form::text('setting[locale][meta_title]', $settingTrans->where('key','meta_title')->first()->value, ['class' => 'form-control','placeholder'=>'Meta Title']) !!}
    </div>

    <div class="form-group">
        <label for="content" class="control-label">Meta Description</label>
        {!! Form::textarea('setting[locale][meta_description]', $settingTrans->where('key','meta_description')->first()->value, ['class' => 'form-control','size'=>'2x5']) !!}
    </div>

    <div class="form-group">
        <label for="meta_title" class="control-label">Meta Keywords</label>
        {!! Form::text('setting[locale][meta_keywords]', $settingTrans->where('key','meta_keywords')->first()->value, ['class' => 'form-control','placeholder'=>'Meta Keywords']) !!}
    </div>
</div>

<div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">

</div>
<div class="clearfix"></div>
