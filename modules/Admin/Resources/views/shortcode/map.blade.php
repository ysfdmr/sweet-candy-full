@section('page.script')
    <script src="https://maps.googleapis.com/maps/api/js"></script>
    <script>
        $(document).ready(function () {
            <?php $info= str_replace(array("\r", "\n"), '', Helpers::getSettings('company_info','Please Set Campany Info'))?>
            var MapOptions= {
                latlng:{lat: {{ Helpers::getSettings('company_lat') }}, lng: {{ Helpers::getSettings('company_long') }}},
                name: '{{ Helpers::getSettings('company_name','Please Set Campany Name') }}',
                info    : '{{ $info }}'
            };
            var map{{ $attr['id'] }} = new google.maps.Map(document.getElementById('sc_map_{{ $attr['id'] }}'), {
                zoom: 15,
                center: MapOptions.latlng
            });
            var marker{{ $attr['id'] }} = new google.maps.Marker({
                position: MapOptions.latlng,
                map: map{{ $attr['id'] }},
                customName: 'baseMap',
                animation: google.maps.Animation.DROP,
                title: MapOptions.name
            });
        });
    </script>
@append
<?php
    $height=!isset($attr['height'])?:'height:'.$attr['height'].';';
    $width=!isset($attr['width'])?:'width:'.$attr['width'].';';
?>
<div id="sc_map_{{ $attr['id'] }}" class="sc_map" style="min-height: 150px;{{ $height.$with }};"></div>