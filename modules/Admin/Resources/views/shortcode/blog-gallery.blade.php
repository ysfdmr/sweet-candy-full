@section('page.script')
@append
<?php
    $height=!isset($attr['height'])?:'height:'.$attr['height'].';';
    $width=!isset($attr['width'])?:'width:'.$attr['width'].';';
?>
<div class="fotorama" data-width="100%"  data-nav="false" data-click="false" data-autoplay="true">
    @if($post->getSlider)
        @foreach($post->getSlider->Items as $index => $slide)
            <div data-img="{{ Helpers::image($slide->Image->FullPath,1800,700) }}">
                <div class="caption-wrapper">
                    <div class="caption">
                        <div class="title"> {{ $slide->Image->title }} </div>
                        <div class="description"> {{ $slide->Image->description }} </div>
                        @if( !empty($slide->Image->alt) )
                            <a href="{{ $slide->Image->alt }}" class="link"> View </a>
                        @endif
                    </div>
                </div>
            </div>
        @endforeach
    @endif
</div>