
        </div><!-- ./wrapper -->
        <div id="load-ajax"></div>
        <!-- jQuery 2.1.4 -->
        <script src="/assets/global/plugins/jQuery/jQuery-2.1.4.min.js"></script>
        <!-- Bootstrap 3.3.5 -->
        <script src="/assets/global/bootstrap/js/bootstrap.min.js"></script>
        <!-- Select2 -->
        <script src="/assets/global/plugins/select2/select2.full.min.js"></script>
        <!-- Holderjs -->
        <script src="/assets/global/plugins/holder.min.js"></script>
        <!-- Toastr -->
        <script src="/assets/global/plugins/toastr/toastr.js"></script>
        <!-- Validate -->
        <script src="/assets/global/plugins/validate/validetta.min.js"></script>
        <!-- AdminLTE App -->
        <script src="/assets/admin/js/app.js"></script>
        <script>
            @if (Session::has('message'))
                toastr.{{ Session::get('message.type') }}('{{ Session::get('message.text') }}','{{ Session::get('message.head') }}');
            @endif
        </script>
        @yield('panel.script')
    </body>
</html>