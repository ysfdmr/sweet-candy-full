@include('admin::layouts.head')
@include('admin::layouts.header')
@include('admin::layouts.sidebar')

<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">
    <!-- Content Header (Page header) -->

    <section class="content-header">
        <h1>@yield('panel.header')</h1>
    {{--
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
            <li class="active">Here</li>
        </ol>
    --}}
    </section>

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <div class="col-lg-12">
                @if($errors->has())
                    <div class="alert alert-danger alert-dismissable">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        <h4><i class="icon fa fa-ban"></i> {{ Lang::get('global.word.error') }}!</h4>
                        {!! Html::ul($errors->all()) !!}
                    </div>
                @endif
            </div>
        </div>
        @yield('panel.content')
        <!-- Your Page Content Here -->
    </section><!-- /.content -->
</div><!-- /.content-wrapper -->
{{--@include('admin::layouts.panel')--}}
@include('admin::layouts.end')

