<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ Helpers::get_gravatar(Auth::user()->email,150) }}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{ Auth::user()->FullName }}</p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> TODO :Get Group</a>
            </div>
        </div>
        {{--
        <!-- search form (Optional) -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
        </form>
        <!-- /.search form -->
--}}
        <!-- Sidebar Menu -->
        <?php $activeModules=$MODULES; ?>
        <ul class="sidebar-menu">
            <li class="header">{{ Lang::get('admin::panel.sidebar.title.menu') }}</li>
            <li class="active"><a href="/admin"><i class="fa fa-dashboard"></i> <span>{{ Lang::get('admin::panel.sidebar.link.dashboard') }}</span></a></li>

            <li class="header">{{ Lang::get('admin::panel.sidebar.title.pages') }}</li>
            @if(in_array("Page", $activeModules))
                <li>
                    <a href="/admin/page/">
                        <i class="fa fa-sticky-note-o"></i>
                        <span>Pages</span>
                    </a>
                </li>
            @endif
            <li class="header">Blog</li>
            @if(in_array("Blog", $activeModules))
                <li>
                    <a href="/admin/blog/">
                        <i class="fa fa-newspaper-o"></i>
                        <span>Posts</span>
                    </a>
                </li>
                <li>
                    <a href="/admin/blog-category/">
                        <i class="fa fa-tags "></i>
                        <span>Categories</span>
                    </a>
                </li>
            @endif

            <li class="header">Custom Modules</li>
            @foreach(ThemeComponents::getModules() as $module)
                <li>
                    <a href="/admin/modules/{{$module['name']}}">
                        <i class="fa fa-cubes"></i>
                        <span>{{$module['view']}}</span>
                    </a>
                </li>
            @endforeach
            @if(Helpers::getSettings(Theme::getActive().'_products',false))
            <li class="header">Product Module</li>
            <li><a href="/admin/product"><i class="fa fa-picture-o"></i> <span>Product</span></a></li>
            <li><a href="/admin/product/category"><i class="fa fa-picture-o"></i> <span>Product Categories</span></a></li>
            <li><a href="/admin/product/brand"><i class="fa fa-picture-o"></i> <span>Product Brands</span></a></li>
            @endif
            <li class="header">Media</li>
            <li><a href="/admin/gallery"><i class="fa fa-picture-o"></i> <span>Gallery</span></a></li>

            <li class="header">Locations</li>
            <li><a href="/admin/location"><i class="fa fa-picture-o"></i> <span>Location</span></a></li>


            <li class="header">Menu</li>
            <li><a href="/admin/menu"><i class="fa fa-picture-o"></i> <span>Menu</span></a></li>

            <li class="header">User Menu</li>
            <li><a href="/admin/user/{{ Auth::id() }}/edit"><i class="fa fa-user"></i> <span>Profile</span></a></li>
            <li><a href="/admin/user"><i class="fa fa-users"></i> <span>Users</span></a></li>
            <li><a href="/admin/role"><i class="fa fa-legal"></i> <span>Roles</span></a></li>

            <li class="header">{{ Lang::get('admin::panel.sidebar.title.settings') }} Menu</li>
            <li><a href="/admin/setting"><i class="fa fa-cogs"></i> <span>Setting</span></a></li>
            <li><a href="/admin/setting-language"><i class="fa fa-flag-o"></i> <span>Language</span></a></li>
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>