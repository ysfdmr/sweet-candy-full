<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ Helpers::get_gravatar(Auth::user()->email,150) }}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{ Auth::user()->FullName }}</p>
                <!-- Status -->
                <a href="#"><i class="fa fa-circle text-success"></i> TODO :Get Group</a>
            </div>
        </div>
        {{--
        <!-- search form (Optional) -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
        </form>
        <!-- /.search form -->
--}}
        <!-- Sidebar Menu -->
        <?php $activeModules=Module::getByStatus(1); ?>
        <ul class="sidebar-menu">
            @foreach($MENU as $module)
                @if(!empty($menus=config(strtolower($module->name).'.menu')))
                    @foreach($menus as $menu)
                        @if(isset($menu['header']))
                            <li class="header">{{ $menu['name'] }}</li>
                        @elseif(isset($menu['sub']))
                            @if(Auth::user()->can($menu['permission']))
                                <li class="treeview">
                                    <a href="#">
                                        <i class="fa  {{ $menu['icon'] }}"></i> <span>{{ $menu['name'] }}</span>
                                        <i class="fa fa-angle-left pull-right"></i>
                                    </a>
                                    <ul class="treeview-menu">
                                        @foreach($menu['sub'] as $submenu)
                                            <li><a href="{{ $submenu['url'] }}">{{ $submenu['name'] }}</a></li>
                                        @endforeach
                                    </ul>
                                </li>
                            @endif
                        @else
                            @if(Auth::user()->can($menu['permission']))
                                <li><a href="{{ $menu['url'] }}"><i class="fa  {{ $menu['icon'] }}"></i>{{ $menu['name'] }}</a></li>
                            @endif
                        @endif
                    @endforeach
                @endif
            @endforeach

            <li class="header">{{ Lang::get('admin::panel.sidebar.title.menu') }}</li>
            <li class="active"><a href="/admin"><i class="fa fa-dashboard"></i> <span>{{ Lang::get('admin::panel.sidebar.link.dashboard') }}</span></a></li>

            <li class="header">{{ Lang::get('admin::panel.sidebar.title.modules') }}</li>
            @if(in_array("Page", $activeModules))
            <li class="treeview">
                <a href="#">
                    <i class="fa  fa-sticky-note-o"></i> <span>Page</span>
                    <i class="fa fa-angle-left pull-right"></i>
                </a>
                <ul class="treeview-menu">
                    <li><a href="/admin/page">All Pages</a></li>
                    <li><a href="/admin/page/create">Add Page</a></li>
                </ul>
            </li>
            @endif
            @if(in_array("Blog", $activeModules))
                <li class="treeview">
                    <a href="#"><i class="fa fa-newspaper-o"></i> <span>Blog</span> <i class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="/admin/blog">All Post</a></li>
                        <li><a href="/admin/blog/create">Add Post</a></li>
                        <li><a href="/admin/blog-category">All Category</a></li>
                        <li><a href="/admin/blog-category/create">Add Category</a></li>
                    </ul>
                </li>
            @endif

            <li class="header">User Menu</li>
            <li><a href="/admin/user/{{ Auth::id() }}/edit"><i class="fa fa-user"></i> <span>Profile</span></a></li>
            <li><a href="/admin/user"><i class="fa fa-users"></i> <span>Users</span></a></li>
            <li><a href="/admin/groups"><i class="fa fa-legal"></i> <span>Groups</span></a></li>

            <li class="header">{{ Lang::get('admin::panel.sidebar.title.settings') }} Menu</li>
            <li><a href="/admin/setting"><i class="fa fa-cogs"></i> <span>Setting</span></a></li>
            <li><a href="/admin/setting-language"><i class="fa fa-flag-o"></i> <span>Language</span></a></li>
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>