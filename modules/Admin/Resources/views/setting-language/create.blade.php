@extends('admin::layouts.master')

@section('panel.header')
    {{ Lang::get('global.word.language') }} <small>{{ Lang::get('global.word.create') }}</small>
@stop

@section('panel.style')
    <link rel="stylesheet" href="/assets/modules/blog/css/blog.css"/>
@stop

@section('panel.script')

    <script src="/assets/global/plugins/ckeditor/ckeditor.js"></script>
    <script src="/assets/modules/blog/js/blog.js"></script>
@stop

@section('panel.content')
    <div class="row">
        <div class="col-lg-12">
            {!! Form::open(['url' => '/admin/setting-language', 'method' => 'post', 'class'=>'']) !!}
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#content-form" data-toggle="tab">{{ Lang::get('global.word.language') }} {{ Lang::get('global.word.setting') }}</a></li>
                        <li class="pull-right"><button type="submit" class="btn btn-primary pull-right">{{ Lang::get('global.word.save') }}</button></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="content-form">
                            @include('admin::setting-language.form')
                        </div><!-- /.tab-pane -->
                    </div><!-- /.tab-content -->
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary pull-right">{{ Lang::get('global.word.save') }}</button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>

@stop