<div class="row">
    <div class="col-lg-6">
        <div class="form-group">
            <label for="code" class="control-label">{{ Lang::get('global.word.language') }}</label>
            @include('admin::locale-select')
        </div>
    </div>
    <div class="col-lg-6">
        <div class="form-group">
            <label for="status" class="control-label">{{ Lang::get('global.word.status') }}</label>
            {!! Form::select('status', [1=>'Active',0=>'Passive'] , null , ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="form-group {{ $errors->first('name','has-error') }}">
    <label for="title" class="control-label">{{ Lang::get('global.word.name') }}</label>
    {!! Form::text('name', null, ['class' => 'form-control','placeholder'=>Lang::get('global.word.name') ]) !!}
</div>










