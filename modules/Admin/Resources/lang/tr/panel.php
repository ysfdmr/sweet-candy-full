<?php
return [

    /*
    |--------------------------------------------------------------------------
    | Panel Language Lines
    |--------------------------------------------------------------------------
    */

    'sidebar.title.menu' => 'Menü',
    'sidebar.title.modules' => 'Modüller',
    'sidebar.title.settings' => 'Ayarlar',

    /*Link translate*/
    'sidebar.link.dashboard' => 'Yönetim Paneli',
];