<?php
return [

    /*
    |--------------------------------------------------------------------------
    | Panel Language Lines
    |--------------------------------------------------------------------------
    */

    'sidebar.title.menu' => 'Menu',
    'sidebar.title.modules' => 'Modules',
    'sidebar.title.settings' => 'Settings',

    /*Link translate*/
    'sidebar.link.dashboard' => 'Dashboard',

];