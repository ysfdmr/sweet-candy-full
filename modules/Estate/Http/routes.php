<?php

Route::group(['prefix' => 'estate', 'namespace' => 'Modules\Estate\Http\Controllers'], function()
{
	Route::get('/', 'EstateController@index');
});