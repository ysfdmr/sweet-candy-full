<?php namespace Modules\Estate\Http\Controllers;

use Pingpong\Modules\Routing\Controller;

class EstateController extends Controller {
	
	public function index()
	{
		return view('estate::index');
	}
	
}