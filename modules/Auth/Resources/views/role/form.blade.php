<div class="row">
    <div class="col-lg-6">
        <div class="form-group {{ $errors->first('name','has-error') }}">
            <label for="name" class="control-label">{{ Lang::get('global.word.name') }} ({{ Lang::get('global.word.optional') }})</label>
            {!! Form::text('name', null, ['class' => 'form-control','placeholder'=>Lang::get('global.word.name') ]) !!}
        </div>

        <div class="form-group {{ $errors->first('display_name','has-error') }}">
            <label for="display_name" class="control-label">{{ Lang::get('global.word.display_name') }}</label>
            {!! Form::text('display_name', null, ['class' => 'form-control','placeholder'=>Lang::get('global.word.display_name') ]) !!}
        </div>

        <div class="form-group {{ $errors->first('description','has-error') }}">
            <label for="description" class="control-label">{{ Lang::get('global.word.description') }}</label>
            {!! Form::textarea('description', null , ['class' => 'form-control','placeholder'=>Lang::get('global.word.description') ]) !!}
        </div>

    </div>
    <div class="col-lg-6">
            @foreach($perms as $key => $name)
                <label for="{{ $key }}"> <input type="checkbox" name="{{ $key }}"/>{{ $name }}</label><br/>
            @endforeach
    </div>

</div>










