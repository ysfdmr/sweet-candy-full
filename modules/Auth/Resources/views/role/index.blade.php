@extends('admin::layouts.master')

@section('panel.header')
    {{ Lang::get('auth::trans.role.index.page_title') }} <small>{{ Lang::get('auth::trans.role.index.page_description') }}</small>
@stop

@section('panel.style')
    <link rel="stylesheet" href="/assets/modules/blog/css/blog.css"/>
@stop

@section('panel.script')
    <script src="/assets/modules/blog/js/blog.js"></script>
@stop

@section('panel.content')
    <div class="row">
        @include('auth::role.quick-menu')
        <div class="col-lg-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">{{ Lang::get('auth::trans.role.index.table_title') }}</h3>
                </div><!-- /.box-header -->
                <div class="box-body no-padding">
                    <table class="table table-striped">
                        <tbody>
                        <tr>
                            <th>{{ Lang::get('global.word.name') }}</th>
                            <th>{{ Lang::get('global.word.display_name') }}</th>
                            <th>{{ Lang::get('global.word.description') }}</th>
                            <th style="width: 100px">{{ Lang::get('global.word.action') }}</th>
                        </tr>

                            @foreach($roles as $role)
                                <tr>
                                    <td>{{ $role->name }}</td>
                                    <td>{{ $role->display_name }}</td>
                                    <td>{{ $role->description }}</td>
                                    <td>
                                        <a class="action badge bg-green" href="/admin/role/{{ $role->id }}/edit" data-toggle="tooltip" data-original-title="Edit">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                        @if($role->name!='admin')
                                        {!! Form::open(array('url' => array('/admin/role',$role->id),'method'=> 'DELETE')) !!}
                                        <button type="submit" class="action badge bg-red"
                                                data-toggle="tooltip" data-original-title="delete"
                                                onclick="if(confirm('{{ Lang::get('global.text.deleteQuestion') }}'))return true;else return false;">
                                            <i class="fa fa-close"></i>
                                        </button>
                                        {!! Form::close() !!}
                                        @endif
                                    </td>
                                </tr>
                            @endforeach


                        </tbody>
                    </table>
                </div><!-- /.box-body -->
            </div>
        </div>
    </div>


@stop