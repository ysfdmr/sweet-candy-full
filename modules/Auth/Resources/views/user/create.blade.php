@extends('admin::layouts.master')

@section('panel.header')
    {{ Lang::get('auth::trans.user.create.page_title') }} <small>{{ Lang::get('auth::trans.user.create.page_description') }}</small>
@stop

@section('panel.style')
@stop

@section('panel.script')
@stop

@section('panel.content')
    <div class="row">
        @include('blog::category.quick-menu')
        <div class="col-lg-12">
            {!! Form::open(['url' => '/admin/user', 'method' => 'post', 'class'=>'']) !!}
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#content-form" data-toggle="tab">
                                {{ Lang::get('global.word.user') }} {{ Lang::get('global.word.create') }}
                            </a>
                        </li>
                        <li class="pull-right"><button type="submit" class="btn btn-primary pull-right">{{ Lang::get('global.word.save') }}</button></li>
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane active" id="content-form">
                            @include('auth::user.form')
                        </div><!-- /.tab-pane -->
                    </div><!-- /.tab-content -->
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary pull-right">{{ Lang::get('global.word.save') }}</button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>

@stop