<div class="row">
<div class="col-md-6 col-lg-4">
    <div class="row">
        <div class="col-lg-6">
            <div class="form-group {{ $errors->first('first_name','has-error') }}">
                <label for="first_name" class="control-label">{{ Lang::get('global.word.first_name') }}</label>
                {!! Form::text('first_name', null, ['class' => 'form-control','placeholder'=>Lang::get('global.word.first_name') ]) !!}
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group {{ $errors->first('last_name','has-error') }}">
                <label for="last_name" class="control-label">{{ Lang::get('global.word.last_name') }}</label>
                {!! Form::text('last_name', null, ['class' => 'form-control','placeholder'=>Lang::get('global.word.last_name') ]) !!}
            </div>
        </div>
    </div>

    <div class="form-group {{ $errors->first('username','has-error') }}">
        <label for="username" class="control-label">{{ Lang::get('global.word.username') }} ({{ Lang::get('global.word.optional') }})</label>
        {!! Form::text('username', null, ['class' => 'form-control','placeholder'=>Lang::get('global.word.username') ]) !!}
    </div>

    <div class="form-group {{ $errors->first('email','has-error') }}">
        <label for="email" class="control-label">{{ Lang::get('global.word.email') }}</label>
        {!! Form::text('email', null, ['class' => 'form-control','placeholder'=>Lang::get('global.word.email') ]) !!}
    </div>

    <div class="form-group {{ $errors->first('password','has-error') }}">
        <label for="password" class="control-label">{{ Lang::get('global.word.password') }}</label>
        {!! Form::password('password', ['class' => 'form-control','placeholder'=>Lang::get('global.word.password') ]) !!}
    </div>

    <div class="form-group {{ $errors->first('password_confirmation','has-error') }}">
        <label for="password_confirmation" class="control-label">{{ Lang::get('global.word.password_confirmation') }}</label>
        {!! Form::password('password_confirmation',['class' => 'form-control','placeholder'=>Lang::get('global.word.password_confirmation') ]) !!}
    </div>

    <div class="row">
        <div class="col-lg-6">
            <div class="form-group">
                <label for="role" class="control-label">{{ Lang::get('global.word.role') }}</label>
                {!! Form::select('role', $roles , null , ['class' => 'form-control']) !!}
            </div>
        </div>
        <div class="col-lg-6">
            <div class="form-group">
                <label for="status" class="control-label">{{ Lang::get('global.word.status') }}</label>
                {!! Form::select('active', [1=>'Active',0=>'Passive'] , null , ['class' => 'form-control']) !!}
            </div>
        </div>
    </div>
</div>

</div>










