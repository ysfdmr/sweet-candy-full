@extends('admin::layouts.master')

@section('panel.header')
    {{ Lang::get('auth::trans.user.index.page_title') }} <small>{{ Lang::get('auth::trans.user.index.page_description') }}</small>
@stop

@section('panel.style')
    <link rel="stylesheet" href="/assets/modules/blog/css/blog.css"/>
@stop

@section('panel.script')
    <script src="/assets/modules/blog/js/blog.js"></script>
@stop

@section('panel.content')
    <div class="row">
        @include('auth::user.quick-menu')
        <div class="col-lg-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">{{ Lang::get('auth::trans.user.index.table_title') }}</h3>
                </div><!-- /.box-header -->
                <div class="box-body no-padding">
                    <table class="table table-striped">
                        <tbody>
                        <tr>
                            <th style="width: 20px">#</th>
                            <th>{{ Lang::get('global.word.first_name') }}</th>
                            <th>{{ Lang::get('global.word.last_name') }}</th>
                            <th>{{ Lang::get('global.word.username') }}</th>
                            <th>{{ Lang::get('global.word.email') }}</th>
                            <th>{{ Lang::get('global.word.status') }}</th>
                            <th style="width: 100px">{{ Lang::get('global.word.action') }}</th>
                        </tr>

                            @foreach($users as $user)
                                <tr>
                                    <td><img src="{{ Helpers::get_gravatar($user->email,20) }}" class="img-circle"/></td>
                                    <td>{{ $user->first_name }}</td>
                                    <td>{{ $user->last_name }}</td>
                                    <td>{{ $user->username }}</td>
                                    <td>{{ $user->email }}</td>
                                    <td>{{ $user->active==true?'ACTIVE':'PASSIVE' }}</td>
                                    <td>
                                        <a class="action badge bg-green" href="/admin/user/{{ $user->id }}/edit" data-toggle="tooltip" data-original-title="Edit">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                        {!! Form::open(array('url' => array('/admin/user',$user->id),'method'=> 'DELETE')) !!}
                                        <button type="submit" class="action badge bg-red"
                                                data-toggle="tooltip" data-original-title="delete"
                                                onclick="if(confirm('{{ Lang::get('global.text.deleteQuestion') }}'))return true;else return false;">
                                            <i class="fa fa-close"></i>
                                        </button>
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                            @endforeach


                        </tbody>
                    </table>
                </div><!-- /.box-body -->
            </div>
        </div>
    </div>


@stop