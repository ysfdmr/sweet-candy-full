<div class="col-lg-12">
    <div class="box box-solid">
        <div class="box-header with-border">
            <i class="fa fa-coffee"></i>
            <h3 class="box-title">{{ Lang::get('global.word.quick-menu') }}</h3>
        </div><!-- /.box-header -->
        <div class="box-body clearfix">
            <div class="row">
                <div class="col-lg-12 text-right">
                    <a class="btn btn-app" href="/admin/user/create">
                        <i class="fa fa-edit"></i> {{ Lang::get('auth::trans.btn.create_user') }}
                    </a>
                    <a class="btn btn-app" href="/admin/user">
                        <i class="fa fa-list"></i> {{ Lang::get('auth::trans.btn.show_users') }}
                    </a>
                </div>
            </div>

        </div><!-- /.box-body -->
    </div>
</div>