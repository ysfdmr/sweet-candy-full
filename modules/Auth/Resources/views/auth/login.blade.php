<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>BMCMS - Log in</title>
    <meta name="csrf-token" content="{!! csrf_token() !!}" />
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="/assets/global/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="stylesheet" href="/assets/admin/css/AdminLTE.min.css">
    <link rel="stylesheet" href="/assets/global/plugins/iCheck/square/blue.css">
    <link rel="stylesheet" href="/assets/global/plugins/toastr/toastr.css"/>
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body class="hold-transition login-page">
<div class="login-box">
    <div class="login-logo">
        <a href="../../index2.html"><b>BM</b>CMS</a>
    </div><!-- /.login-logo -->
    <div class="login-box-body">
        <p class="login-box-msg">Sign in to start your session</p>
        <div class="alert alert-danger alert-dismissible form-errors" style="display: none;">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            <h4><i class="icon fa fa-ban"></i> Errors!</h4>
            <div class="errors"></div>
        </div>
        {!! Form::open(['url' => '/auth/login', 'method' => 'post','id'=>'login']) !!}
            <div class="form-group has-feedback">
                <input type="text" name="login" class="form-control" placeholder="Username or Email">
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
                <input type="password" name="password" class="form-control" placeholder="Password">
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="row">
                <div class="col-xs-8">
                    <div class="checkbox icheck">
                        <label>
                            <input type="checkbox" name="remember"> Remember Me
                        </label>
                    </div>
                </div><!-- /.col -->
                <div class="col-xs-4">
                    <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
                </div><!-- /.col -->
            </div>
        {!! Form::close() !!}
        {{--
        <div class="social-auth-links text-center">
            <p>- OR -</p>
            <a href="#" class="btn btn-block btn-social btn-facebook btn-flat"><i class="fa fa-facebook"></i> Sign in using Facebook</a>
            <a href="#" class="btn btn-block btn-social btn-google btn-flat"><i class="fa fa-google-plus"></i> Sign in using Google+</a>
        </div><!-- /.social-auth-links -->

        <a href="#">I forgot my password</a><br>
        <a href="register.html" class="text-center">Register a new membership</a>
        --}}
    </div><!-- /.login-box-body -->
</div><!-- /.login-box -->

<script src="/assets/global/plugins/jQuery/jQuery-2.1.4.min.js"></script>
<script src="/assets/global/bootstrap/js/bootstrap.min.js"></script>
<script src="/assets/global/plugins/iCheck/icheck.min.js"></script>
<script src="/assets/global/plugins/toastr/toastr.js"></script>
<script>
    $(function () {
        $('input').iCheck({
            checkboxClass: 'icheckbox_square-blue',
            radioClass: 'iradio_square-blue',
            increaseArea: '20%' // optional
        });
        var errorArea=$('.form-errors');
        var errorText=$('.form-errors .errors');
        $( "#login" ).submit( function() {
            $.ajax({
                url: '/auth/login',
                type: 'post',
                data: $('#login').serialize(),
                success: function(response) {
                    if(response.status=='success'){
                        errorArea.hide();
                        toastr.success(response.message,'',{positionClass: "toast-top-center"});
                        window.location.href =response.redirect;
                    }else{
                        if(response.field != null){
                            errorArea.show();
                            errorText.html(null);
                            $.each(response.field, function(key,value) {
                                errorText.append(value+"<br>");
                            });
                        }
                        toastr.error(response.message,'',{positionClass: "toast-top-center"});
                    }
                },
                error:function(){
                    errorArea.hide();
                    toastr.error('Connection Error Please Refresh Page And Try Again!','',{positionClass: "toast-top-center"});
                }
            });
            return false;
        });

    });
</script>
</body>
</html>