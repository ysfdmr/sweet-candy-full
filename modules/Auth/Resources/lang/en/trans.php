<?php
return [

    /*
    |--------------------------------------------------------------------------
    | Language Module Name -> Page Name -> Trans Key
    |--------------------------------------------------------------------------
    */
    /*User index page*/
    'user.index.page_title' => 'Users',
    'user.index.page_description' => 'Manage users ...',
    'user.index.table_title' => 'User list',
    /*User Create page*/
    'user.create.page_title' => 'User Create',
    'user.create.page_description' => 'Create new user',
    /*User Edit page*/
    'user.edit.page_title' => 'User Edit',
    'user.edit.page_description' => 'Update user',

    /*User Module Buttons*/
    'btn.create_user'       => 'Create User',
    'btn.show_users'        => 'Show Users',


];