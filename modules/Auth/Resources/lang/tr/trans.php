<?php
return [

    /*
    |--------------------------------------------------------------------------
    | Panel Language Lines
    |--------------------------------------------------------------------------
    */
    /*User index page*/
    'user.index.page_title' => 'Üyeler',
    'user.index.page_description' => 'Üye yönetimi ...',
    'user.index.table_title' => 'Üye Listesi',
    /*User Create page*/
    'user.create.page_title' => 'Üye Oluştur',
    'user.create.page_description' => 'Üye oluşturma ekranı...',
    /*User Edit page*/
    'user.edit.page_title' => 'Üye düzenle',
    'user.edit.page_description' => 'Üye düzenleme ekranı...',

    /*User Module Buttons*/
    'btn.create_user'       => 'Üye Ekle',
    'btn.show_users'        => 'Tüm Üyeler',

];