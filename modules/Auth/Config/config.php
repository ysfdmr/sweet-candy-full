<?php

return [
	'name' => 'Auth',
    "permissions" => [
        "admin.dashboard" => 'Admin Dashboard Page',
        "admin.settings" => 'Admin Settings Page',
        "admin.language" => 'Site Language Settings'
    ],
    'permission'=>'admin.menu',
    'menu' => [
        [
            'header'=>true,
            'name' => 'Dashboard',
        ],
        [   //item
            'name' => 'Dashboard',
            'icon' => 'fa-dashboard',
            'url' => '/admin',
            'permission'=>'admin.dashboard'
        ],
        [   //item
            'name' => 'Dashboard',
            'icon' => 'fa-dashboard',
            'url' => '/admin',
            'permission'=>'admin.dashboard',
            'sub' => [
                [
                    'name' => 'link name',
                    'icon' => '',
                    'url' => '',
                    'permission'=>'admin.dashboard'
                ],
            ]
        ],
    ]
];