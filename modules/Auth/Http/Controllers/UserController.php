<?php namespace Modules\Auth\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Input;
use Modules\Auth\Entities\Role;
use Modules\Auth\Http\Requests\UserRequest;
use Pingpong\Modules\Routing\Controller;

class UserController extends Controller {
    public function index()
    {
        $users=User::all();
        return view('auth::user.index',compact('users'));
    }

    public function create()
    {
        $roles=Role::lists('name','id');
        return view('auth::user.create',compact('roles'));
    }

    public function store(UserRequest $request)
    {
        $user = new User();
        $user->first_name=Input::get('first_name');
        $user->last_name=Input::get('last_name');
        $user->username=Input::get('username');
        $user->email=Input::get('email');
        $user->active=Input::get('active');
        $user->password=bcrypt(Input::get('password'));
        $user->save();
        Cache::flush();
        return redirect()->to('admin/user/'.$user->id)
            ->with('message',  array('type' => 'success', 'head' => 'Info Message!', 'text' => 'Create Success'));
    }

    public function edit($id)
    {
        $user=User::find($id);
        $roles=Role::lists('name','id');
        return view('auth::user.edit',compact('roles','user'));
    }

    public function update(UserRequest $request,$id)
    {
        $user = User::find($id);
        $user->first_name=Input::get('first_name');
        $user->last_name=Input::get('last_name');
        $user->username=Input::get('username');
        $user->email=Input::get('email');
        $user->active=Input::get('active');
        if(Input::get('password')!=""){
            $user->password=bcrypt(Input::get('password'));
        }
        $user->save();
        Cache::flush();
        return redirect()->back()
            ->with('message',  array('type' => 'success', 'head' => 'Info Message!', 'text' => 'Update Success'));
    }
}