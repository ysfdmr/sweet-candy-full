<?php namespace Modules\Auth\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Validator;
use Pingpong\Modules\Routing\Controller;

class AuthController extends Controller {
    public function __construct()
    {
        //$this->middleware('<your-middleware-name>');
        $this->middleware('guest', ['only' => ['getLogin']]);
        //$this->middleware('<more-middleware>', ['except' => ['<another-action-name>']]);
    }
    /*
    public function getIndex()
    {
        return view('auth::auth.login');
    }
    */
	public function getLogin()
	{

		return view('auth::auth.login');
	}

    public function postLogin()
    {
        $rules= [
            'login'=> 'required|min:4',
            'password'=> 'required|min:4'
        ];
        $messages= [
            'login.required'=> 'User name or email is required.',
            'login.min'=> 'User name is min 4 character required.',
            'password.required'=> 'Password is required.',
        ];
        $validator = Validator::make(Input::all(), $rules, $messages);
        if ($validator->fails()) {
            return response()->Json([
                'status'    => 'error',
                'message'   => 'Incorrect email or password!',
                'field'     => $validator->errors()
            ]);
        }else{
            $login = filter_var(Input::get('login'), FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
            Input::merge([$login => Input::get('login')]);
            $credentials = [
                $login      => Input::get($login),
                'password'  => Input::get('password'),
            ];
            if(Auth::validate($credentials)){
                if(Input::get('remember')){
                    Auth::attempt($credentials, true);
                }else{
                    Auth::attempt($credentials);
                }
                return response()->Json([
                    'status'    => 'success',
                    'message'   => 'Login Success. Redirecting Please Wait.',
                    'redirect'  => '/admin/'
                ]);
            }else{
                return response()->Json([
                    'status'    => 'error',
                    'message'   => 'Incorrect email or password!',
                    'field'     => null
                ]);
            }
        }

    }

    public function anyLogout()
    {
        Auth::logout();
        return redirect('/auth/login');
    }
}