<?php

Route::group(['prefix' => 'auth', 'namespace' => 'Modules\Auth\Http\Controllers'], function()
{
	Route::controller('/', 'AuthController');
});

Route::group(['prefix' => 'admin', 'middleware'=>'auth','namespace' => 'Modules\Auth\Http\Controllers'], function()
{
    Route::resource('/user', 'UserController');
    Route::resource('/role', 'RoleController');
});