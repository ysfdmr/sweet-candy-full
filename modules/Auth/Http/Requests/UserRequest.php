<?php namespace Modules\Auth\Http\Requests;

use App\User;
use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
        //dd($this);
        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                return [
                    'first_name'=> 'required',
                    'last_name'=> 'required',
                    'username'=> 'min:5|unique:users',
                    'password'=> 'required|min:5|confirmed',
                    'email'=> 'required|email|unique:users'
                ];
            }
            case 'PUT':
            case 'PATCH':
            {
                return [
                    'first_name'=> 'required',
                    'last_name'=> 'required',
                    'username'=> 'min:5|unique:users,username,'.$this->route('user'),
                    'password'=> 'min:5|confirmed',
                    'email'=> 'required|email|unique:users,email,'.$this->route('user'),
                ];
            }
            default:break;
        }
	}

}
