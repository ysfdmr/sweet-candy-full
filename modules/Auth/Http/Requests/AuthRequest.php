<?php namespace Modules\Auth\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AuthRequest extends FormRequest {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
        return [
            'login'=> 'required|min:4',
            'password'=> 'required|min:6'
        ];
	}

    public function messages()
    {
        return [
            'login.required'=> 'User name or email is required.',
            'login.min'=> 'User name is min 4 character required.',
            'password.required'=> 'Password is required.',
        ];
    }
}
