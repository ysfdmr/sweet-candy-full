<?php namespace Modules\Auth\Database\Seeders;

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Auth\Entities\Role;

class AuthDatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

        $adminGroup = Role::firstOrNew([
            'name'  => 'admin',
            'display_name'  =>'Administrator',
            'description'   => ''
        ]);

        $adminUser = User::firstOrCreate([
            'first_name' => 'admin',
            'last_name' => 'user',
            'username' => 'admin',
            'email' => 'admin@admin.com',
            'password' => bcrypt('admin')
        ]);

        $adminUser->attachRole($adminGroup);

        $this->command->info('Auth table seeded!');
	}

}