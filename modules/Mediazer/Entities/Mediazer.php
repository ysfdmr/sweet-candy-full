<?php namespace Modules\Mediazer\Entities;

use Illuminate\Database\Eloquent\Model;

class Mediazer extends Model{

    protected $table = 'images';
    public $timestamps = true;
    protected $fillable = [];

    public function getFullPathAttribute() {
        return $this->path . $this->file_name;
    }
}