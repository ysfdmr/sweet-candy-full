<?php namespace Modules\Mediazer\Entities;
   
use Illuminate\Database\Eloquent\Model;

class GalleryItem extends Model {

    protected $table = 'gallery_images';
    public $timestamps = true;
    protected $fillable = [];

    public function Image()
    {
        return $this->hasOne('Modules\Mediazer\Entities\Mediazer','id','image_id');
    }
}