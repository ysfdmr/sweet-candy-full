<?php namespace Modules\Mediazer\Entities;
   
use Illuminate\Database\Eloquent\Model;

class Gallery extends Model {

    protected $table = 'galleries';
    public $timestamps = true;
    protected $fillable = [];

    public function Items()
    {
        return $this->hasMany('Modules\Mediazer\Entities\GalleryItem','gallery_id','id')->orderBy('order');
    }

}