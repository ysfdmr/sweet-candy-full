<?php
Route::group(['prefix' => 'admin','middleware'=>['auth'], 'namespace' => 'Modules\Mediazer\Http\Controllers'], function()
{

	Route::get('/mediazer', 'MediazerController@index');
	Route::get('/mediazer/load', 'MediazerController@start');
    /*jQtree Connector */
	Route::any('/mediazer/connector', 'MediazerController@connector');
    /*List directory ajax */
	Route::post('/mediazer/list', 'MediazerController@listDir');

    /*Ajax CRUD Folder*/
    Route::get('/mediazer/forms/create-folder', 'MediazerController@formCreateFolder');
    Route::post('/mediazer/folder/create-folder', 'MediazerController@CreateFolder');
    Route::post('/mediazer/folder/delete', 'MediazerController@postDeleteFolder');

    /*Upload Images*/
    Route::post('/mediazer/upload', 'MediazerController@upload');

    /*Edit Media*/
    Route::get('/mediazer/edit', 'MediazerController@getEdit');
    Route::post('/mediazer/edit', 'MediazerController@postEdit');
    Route::post('/mediazer/delete', 'MediazerController@postDelete');

    /*Mediazer Gallery*/
    Route::controller('/gallery','GalleryController');
    Route::get('/mediazer/item', 'MediazerController@getItem');

});
