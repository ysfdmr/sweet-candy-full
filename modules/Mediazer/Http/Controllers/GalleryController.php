<?php namespace Modules\Mediazer\Http\Controllers;

use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Modules\Mediazer\Entities\Gallery;
use Modules\Mediazer\Entities\GalleryItem;
use Pingpong\Modules\Routing\Controller;

class GalleryController extends Controller {
	
	public function getIndex()
	{
        $galleries=Gallery::all();
		return view('mediazer::gallery.index',compact('galleries'));
	}

    public function getCreate()
    {
        return view('mediazer::gallery.create');
    }

    public function postIndex()
    {
        $rules = array(
            'name' => 'required|unique:galleries',
            'medias' => 'required',
        );

        $messages = array(
            'name.required' => 'Gallery name is required!',
            'medias.required' => 'Please Select Media Files!',
        );

        $validator = Validator::make(Input::all(), $rules, $messages);
        if ($validator->fails()) {
            return response()->Json([
                'status'=>'error',
                'errors'=>$validator->messages(),
            ]);
        } else {
            $gallery = new Gallery();
            $gallery->name=Input::get('name');
            $gallery->description=Input::get('description');
            if($gallery->save()){
                foreach(explode(',',Input::get('medias')) as $index=>$media){
                    $item = new GalleryItem();
                    $item->gallery_id = $gallery->id;
                    $item->image_id = $media;
                    $item->order=$index;
                    $item->save();
                }
            }
            Cache::flush();
            return response()->Json([
                'status'=>'success',
                'return'=>$gallery->id,
            ]);
        }
    }

    public function getEdit($id)
    {
        $gallery=Gallery::with('Items.Image')->where('id',$id)->first();
        return view('mediazer::gallery.edit',compact('gallery'));
    }
    public function postEdit($id)
    {
        $rules = array(
            'name' => 'required|unique:galleries,name,'.$id,
            'medias' => 'required',
        );

        $messages = array(
            'name.required' => 'Gallery name is required!',
            'medias.required' => 'Please Select Media Files!',
        );

        $validator = Validator::make(Input::all(), $rules, $messages);
        if ($validator->fails()) {
            return response()->Json([
                'status'=>'error',
                'errors'=>$validator->messages(),
            ]);
        } else {
            $gallery = Gallery::find($id);
            $gallery->name=Input::get('name');
            $gallery->description=Input::get('description');
            if($gallery->save()){
                foreach(explode(',',Input::get('medias')) as $index => $media){
                    if(!$item = GalleryItem::where('image_id',$media)->first()) {
                        $item = new GalleryItem();
                        $item->gallery_id = $gallery->id;
                        $item->image_id = $media;
                    }
                    $item->order=$index;
                    $item->save();
                }
            }
            Cache::flush();
            return response()->Json([
                'status'=>'success',
                'return'=>$gallery->id,
            ]);
        }
    }

    public function postDeleteMedia($mediaID,$galleryID)
    {
        $item=GalleryItem::where('gallery_id',$galleryID)->where('image_id',$mediaID)->first();
        if($item->delete()){
            Cache::flush();
            return response()->Json([
                'status'=>'success'
            ]);
        }
    }
}