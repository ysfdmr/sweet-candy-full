<?php namespace Modules\Mediazer\Http\Controllers;

use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Modules\Mediazer\Entities\Mediazer;
use Pingpong\Modules\Routing\Controller;

class MediazerController extends Controller {

	public function index()
	{
		return view('mediazer::index');
	}
    public function start()
    {
        return view('mediazer::start');
    }
    public function connector()
	{
        $root = public_path().config('mediazer.basePath');
        if( !$root ) exit("ERROR: Root filesystem directory not set in jqueryFileTree.php");
        $postDir = rawurldecode($root.(isset($_POST['dir']) ? $_POST['dir'] : null ));
        // set checkbox if multiSelect set to true
        $checkbox = ( isset($_POST['multiSelect']) && $_POST['multiSelect'] == 'true' ) ? "<input type='checkbox' />" : null;
        $onlyFolders = ( isset($_POST['onlyFolders']) && $_POST['onlyFolders'] == 'true' ) ? true : false;
        $onlyFiles = ( isset($_POST['onlyFiles']) && $_POST['onlyFiles'] == 'true' ) ? true : false;
        if( file_exists($postDir) ) {
            $files		= scandir($postDir);
            $returnDir	= substr($postDir, strlen($root));
            natcasesort($files);
            if( count($files) > 2 ) { // The 2 accounts for . and ..
                echo "<ul class='jqueryFileTree'>";
                //echo "<li class='directory collapsed'><a href='#' rel='/'>Root</a></li>";
                foreach( $files as $file ) {
                    $htmlRel	= htmlentities($returnDir . $file);
                    $htmlName	= htmlentities($file);
                    $ext		= preg_replace('/^.*\./', '', $file);
                    if( file_exists($postDir . $file) && $file != '.' && $file != '..' ) {
                        if( is_dir($postDir . $file) && (!$onlyFiles || $onlyFolders) )
                            echo "<li class='directory collapsed'>{$checkbox}<a href='#' rel='" .$htmlRel. "/'>" . $htmlName . "</a></li>";
                        else if (!$onlyFolders || $onlyFiles)
                            echo "<li class='file ext_{$ext}'>{$checkbox}<a href='#' rel='" . $htmlRel . "'>" . $htmlName . "</a></li>";
                    }
                }
                echo "</ul>";
            }
        }
	}

    public function listDir(){
        $subDir = Input::get('dir',null);
        $images=Mediazer::where('path',$subDir)->get();
        return view('mediazer::list-image',compact('images'));
    }

    /*FORMS*/
    /*create folder*/
    public function formCreateFolder(){
        return view('mediazer::forms.createFolder');
    }

    public function CreateFolder(){
        $newDir=Input::get('folder_name');
        $dir = public_path().config('mediazer.basePath').Input::get('dir');
        $directories = array_map('basename', File::directories($dir));
        if(in_array($newDir,$directories)){
            return response()->Json([
                'status'    => 'error',
                'directory' => $directories,
                'inputs'    => Input::all(),
                'message'   => 'Bu isimde Bir Klasör Mevcut'
            ]);
        }else{
            if($newDir==""){
                return response()->Json([
                    'status'    => 'error',
                    'message'   => 'Klasör ismi boş bırakılamaz'
                ]);
            }else{
                File::makeDirectory($dir.$newDir, 0777, true);
                return response()->Json([
                    'status'    => 'success',
                    'folder'    => $newDir,
                    'dir'    => $dir,
                    'message'   => 'Klasör Oluşturuldu'
                ]);
            }
        }
    }

    public function postDeleteFolder(){
        $folder = Input::get('folder');
        if($folder=='/'){
            return response()->Json([
                'status'    => 'error',
                'message'   => 'Base folder can not be deleted.'
            ]);
        }
        if(Mediazer::where('path',$folder)->count()>0){
            return response()->Json([
                'status'    => 'error',
                'message'   => 'Folder is not empty!'
            ]);
        }else{
            $folderFullPath=public_path().config('mediazer.basePath').$folder;
            rmdir($folderFullPath);
            return response()->Json([
                'status'    => 'success',
                'message'   => 'Deleted folder successfully.'
            ]);
        }
    }

    public function upload(){
        if(Input::hasFile('file') && Input::get('path')!=""){
            $folder=Input::get('path');
            $path=public_path().config('mediazer.basePath').$folder;
            $file=Input::file('file');
            $file_original_name = $file->getClientoriginalName();
            $file_name = pathinfo($file_original_name, PATHINFO_FILENAME);
            $extension = pathinfo($file_original_name, PATHINFO_EXTENSION);
            $newfilename   = time() . '-'.Str::slug($file_name).'.' . $extension;
            // Image::make($file->getRealPath())->save($path.$newfilename);
            move_uploaded_file($file->getRealPath(),$path.$newfilename);
            $media=new Mediazer();
            $media->path=$folder;
            $media->alt="";
            $media->title="";
            $media->file_name=$newfilename;
            $media->save();
        }else{
            return http_response_code(330);
        }
        return http_response_code(200);
    }

    public function getEdit(){
        $id = Input::get('mediaID',null);
        $image=Mediazer::where('id',$id)->first();
        return view('mediazer::edit-image',compact('image'));
    }

    public function getItem(){
        $id = Input::get('mediaID',null);
        $image=Mediazer::where('id',$id)->first();
        $link='/uploads'.$image->path.$image->file_name;
        return response()->Json($image);
    }

    public function postEdit(){
        $id = Input::get('id',null);
        $media=Mediazer::find($id);
        $media->alt=Input::get('alt');
        $media->title=Input::get('title');
        $media->description=Input::get('description');
        $media->save();
        return http_response_code(200);
    }

    public function postDelete(){
        $id = Input::get('id',null);
        $media=Mediazer::find($id);
        if($media->delete()){
            $path=public_path().config('mediazer.basePath').$media->FullPath;
            unlink($path);
            return http_response_code(200);
        }else{
            return http_response_code(400);
        }
    }
}