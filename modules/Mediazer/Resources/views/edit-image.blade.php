@if(empty($image))
    <div class="text-center">
        <p>İmage is deleted!</p>
    </div>
@else
    <div class="col-xs-12">
        <img class="img-responsive thumbnail MZ-thumb-edit " src="{{ config('mediazer.basePath').$image->FullPath }}"/>
        <a id="MZ-delete-media" data-id="{{ $image->id }}" >Delete</a>
        <small class="hide">{{ $image->path }}{{ $image->file_name }}</small>
        <div class="clearfix"></div>
        <div class="form-group">
            <label for="edit_alt">Alt:</label>
            <input type="text" name="alt" class="form-control" value="{{ $image->alt }}"/>
        </div>
        <div class="form-group">
            <label for="edit_title">Title:</label>
            <input type="text" name="title" class="form-control" value="{{ $image->title }}"/>
        </div>
        <div class="form-group">
            <label for="edit_description">Description:</label>
            <textarea name="description" cols="20" rows="3" class="form-control">{{ $image->description }}</textarea>
        </div>
        <button id="MZ-btn-update" class="btn btn-success form-control" data-id="{{ $image->id }}">Update Media</button>
    </div>
@endif
