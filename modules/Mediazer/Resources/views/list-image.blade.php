@if($images->isEmpty())
    <div class="text-center">
        <p>Folder is empty. Upload media or select another folder.</p>
    </div>
@else
    @foreach($images as $image)
    <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
        <label>
            <input class="MZ-selector" type="checkbox" name="image[]" data-id="{{$image->id}}" value="{{$image->id}}"/>
            <img class="img-responsive thumbnail MZ-thumb " src="{{ config('mediazer.basePath').$image->FullPath }}" width="150" height="150" data-id="{{ $image->id }}"/>
            <button class="MZ-edit-btn" data-id="{{ $image->id }}"><i class="fa fa-pencil"></i></button>
        </label>
    </div>

    @endforeach
@endif
{{--<input class="MZ-selector" type="radio" name="image[]" id="{{image_id}}"/>--}}
