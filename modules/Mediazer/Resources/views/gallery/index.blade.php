@extends('admin::layouts.master')

@section('panel.header')
    {{ Lang::get('admin::lang.gallery.index.title') }} <small>{{ Lang::get('admin::lang.gallery.index.title-small') }}</small>
@stop

@section('panel.style')
@stop

@section('panel.script')
    <script src="/assets/modules/blog/js/gallery.js"></script>
@stop

@section('panel.content')
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <i class="fa fa-coffee"></i>
                    <h3 class="box-title">{{ Lang::get('global.word.quick-menu') }}</h3>
                </div><!-- /.box-header -->
                <div class="box-body clearfix">
                    <div class="row">
                        <div class="col-lg-12 text-right">
                            <a class="btn btn-app" href="/admin/gallery/create">
                                <i class="fa fa-edit"></i> Add Gallery
                            </a>
                        </div>
                    </div>

                </div><!-- /.box-body -->
            </div>
        </div>

        <div class="col-lg-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">Language List</h3>
                </div><!-- /.box-header -->
                <div class="box-body no-padding">
                    <table class="table table-striped">
                        <tbody>
                        <tr>
                            <th style="width: 20px">#</th>
                            <th>{{ Lang::get('global.word.name') }}</th>
                            <th>{{ Lang::get('global.word.count') }}</th>
                            <th style="width: 100px">{{ Lang::get('global.word.action') }}</th>
                        </tr>
                        @foreach($galleries as $gallery)
                            <tr>
                                <td>{{ $gallery->id }}</td>
                                <td>{{ $gallery->name }}</td>
                                <td>{{ $gallery->name }}</td>
                                <td>
                                    <a class="action badge bg-green" href="/admin/gallery/edit/{{ $gallery->id }}" data-toggle="tooltip" data-original-title="Edit">
                                        <i class="fa fa-pencil"></i>
                                    </a>

                                    {!! Form::open(array('url' => array('/admin/gallery',$gallery->id),'method'=> 'DELETE')) !!}
                                    <button type="submit" class="action badge bg-red"
                                            data-toggle="tooltip" data-original-title="delete"
                                            onclick="if(confirm('Are you sure you want to delete?'))return true;else return false;">
                                        <i class="fa fa-close"></i>
                                    </button>
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div><!-- /.box-body -->
            </div>
        </div>
    </div>


@stop