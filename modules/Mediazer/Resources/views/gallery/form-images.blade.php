<div class="row">
    <div class="col-lg-12">
        <div id="MZ-gallery" class="col-sm-12">

            <div class="upload-box">
                <div class="mediazer mediazer-wrapper" data-input="gallery" data-select="multiple" data-source="id"></div>
                <span class="btn btn-primary gallery-add-items">Add Photos</span>
            </div>

            <div class="items col-sm-12 col-lg-12">
                @if($gallery)
                    @foreach($gallery->Items as $item)
                    <div class="item col-xs-4 col-sm-3 col-md-3 col-lg-2" id="{{$item->Image->id}}">
                        <img class="img-rounded img-responsive img-bordered-sm"
                             src="{{ '/photo/250/250/'.'uploads/'.$item->Image->FullPath }}"
                             alt=""/>
                        <i class="fa fa-close" data-id="{{$item->Image->id}}"></i>
                        <i class="fa fa-edit" data-id="{{$item->Image->id}}"></i>
                    </div>
                    @endforeach
                @endif
            </div>

            <div class="item-edit col-sm-3 col-md-3 close" style="display: none;">
                <div class="gallery-edit-close">Edit Media <i class="fa fa-close"></i></div>
                <div class="gallery-edit-box">
                    loading...
                </div>
            </div>

        </div>
    </div>
</div>






