<input name="galleryID" type="hidden" value="{{ $gallery->id?$gallery->id:0 }}"/>
<div class="row">
    <div class="col-lg-12">
        {{--Gallery info SECTION--}}
        <div class="form-group {{ $errors->first('name','has-error') }}">
            <label for="name" class="control-label">{{ Lang::get('global.word.name') }}</label>
            {!! Form::text('name', null, ['class' => 'form-control','placeholder'=>Lang::get('global.word.name') ]) !!}
            {{-- $errors->first('title','<p class="help-block">:message</p>') --}}
        </div>

        <div class="form-group">
            <label for="description" class="control-label">{{ Lang::get('global.word.description') }}</label>
            {!! Form::textarea('description', null, ['class' => 'form-control ckeditor']) !!}
        </div>
        {{--Gallery info SECTION END--}}
    </div>
</div>






