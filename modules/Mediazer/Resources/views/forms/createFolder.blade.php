<div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
    <h5 class="modal-title" id="mediazer-form">Mediazer Form</h5>
</div>
<form id="create-folder" role="form">
    <div class="modal-body">

        <div class="form-group">
            <label for="folder_name">Folder Name</label>
            <input type="text" class="form-control" name="folder_name" id="folder_name" placeholder="Folder Name">
        </div>

    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
        <input type="submit" class="btn btn-primary" value="Add Folder"/>
    </div>
</form>