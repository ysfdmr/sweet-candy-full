<!-- Default bootstrap modal example -->
<div class="modal mediazer-modal fade" id="mediazer" aria-labelledby="mediazer" data-keyboard="false" data-backdrop="static"
     tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="mediazer">Mediazer - Media Files Management</h4>
            </div>
            <div class="modal-body">
                <input name="_input" type="hidden" value="image"/>
                <input name="_select" type="hidden" value="single"/>
                <input name="_source" type="hidden" value="id"/>
                <div class="row MZ-row">
                    <div class="col-xs-12 MZ-actions">
                        <div class="MZ-info">
                            Selected Folder: <span class="MZ-selected-folder">/</span>
                        </div>
                        <div class="actions">
                            <i class="fa fa-refresh MZ-btn" id="refresh-medias" data-toggle="tooltip" data-placement="bottom"
                               data-original-title="Refresh Medias"></i>
                            <i class="fa fa-plus MZ-btn MZ-add-folder" data-toggle="tooltip" data-placement="bottom"
                               data-original-title="Create Folder"></i>
                            <i class="fa fa-minus MZ-btn MZ-delete-folder" data-toggle="tooltip" data-placement="bottom"
                               data-original-title="Delete Folder"></i>
                            <i class="fa fa-cloud-upload MZ-btn MZ-upload-switcher" data-toggle="tooltip"
                               data-placement="bottom" data-original-title="Upload Images"></i>
                        </div>
                    </div>
                    {{--Uploader SECTION--}}
                    <div class="col-xs-12 MZ-colums MZ-uploader close" style="display: none;">
                        <div class="row">
                            <div class="col-md-6">
                                <div id="drag-and-drop-zone" class="uploader">
                                    <div>Drag &amp; Drop Images Here</div>
                                    <div class="or">-or-</div>
                                    <div class="browser">
                                        <label>
                                            <span>Click to open the file Browser</span>
                                            <input type="file" name="files[]" accept="image/*" multiple="multiple"
                                                   title='Click to add Images'>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="panel panel-default MZ-upload-info-box">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">Uploads</h3>
                                    </div>
                                    <div class="panel-body demo-panel-files" id='demo-files'>
                                        <span class="demo-note">No Files have been selected/droped yet...</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{--Uploader SECTION--}}

                    {{--LEFT SECTION--}}
                    <div class="col-xs-12 col-sm-3 col-md-3  MZ-colums MZ-left">
                        <div class="MZ-root"><i class="fa fa-list"></i> Root</div>
                        <div class="MZ-filetree"></div>
                    </div>
                    {{--Body SECTION--}}
                    <div class="col-xs-12 col-sm-9 col-md-9  MZ-colums MZ-body">
                        <div class="MZ-file-box">
                            <blockquote>Please Select Folder & manage files</blockquote>
                        </div>
                    </div>

                    {{--Right Hide SECTION--}}
                    <div class="col-xs-12 col-sm-3 col-md-3 MZ-colums MZ-right close" style="display: none;">
                        <div class="MZ-right-close">Edit Media <i class="fa fa-close"></i></div>
                        <div class="MZ-edit-box">
                            loading...
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id="getMedias" class="btn btn-primary">Select <span class="label label-success MZ-count-label">0</span></button>
            </div>
        </div>
    </div>
</div>

<div class="modal mediazer-modal fade" id="mediazer-form" aria-labelledby="mediazer-form" data-keyboard="false" data-backdrop="static"
     tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">

        </div>
    </div>
</div>