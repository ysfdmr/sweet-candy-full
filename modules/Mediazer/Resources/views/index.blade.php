@extends('admin::layouts.master')

@section('panel.style')

    <link rel="stylesheet" href="/assets/admin/plugins/mediazer/mediazer.css"/>
@stop
@section('panel.script')
    <script src="/assets/admin/plugins/mediazer/mediazer-components.js"></script>
    <script src="/assets/admin/plugins/mediazer/mediazer.js"></script>
@stop
@section('panel.content')

    <h1>Mediazer Test Page</h1>
    <div class="form-group">
        <label for="test">Test Cover Image</label>
        <div class="mediazer" data-input="test" data-select="multiple" ></div>
    </div>
    <div class="form-group">
        <label for="image">Test Image</label>
        <div class="mediazer" data-input="image" data-select="single" data-source="id"></div>
    </div>
    {{--
        <div class="mediazer" data-select="single" data-source="link"></div>@include('mediazer::start')
    --}}
@stop