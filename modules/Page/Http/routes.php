<?php

Route::group(['prefix' => 'admin','middleware'=>['auth'], 'namespace' => 'Modules\Page\Http\Controllers'], function()
{
    Route::resource('/page', 'PageController');
});