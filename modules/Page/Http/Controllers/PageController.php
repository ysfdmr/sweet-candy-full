<?php namespace Modules\Page\Http\Controllers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Input;
use Modules\Mediazer\Entities\Gallery;
use Modules\Page\Entities\Page;
use Modules\Page\Http\Requests\PageRequest;
use Pingpong\Modules\Routing\Controller;

class PageController extends Controller {
	
	public function index()
	{
        $pages=Page::all();
		return view('page::page.index',compact('pages'));
	}

    public function create()
    {
        $sliders=Gallery::lists('name','id');
        return view('page::page.create',compact('sliders'));
    }

    public function store(PageRequest $request)
    {
        $page= new Page();
        $page->title            = Input::get('title');
        $page->description      = Input::get('description');
        $page->content          = Input::get('content');
        if(Input::get('cover') != null){
            $page->cover = Input::get('cover');
        }else{
            $page->cover = null;
        }
        $page->meta_title       = Input::get('meta_title');
        $page->meta_description = Input::get('meta_description');
        $page->meta_keywords    = Input::get('meta_keywords');
        
        $page->slider           = Input::get('slider')==null?null:Input::get('slider');
        $page->user_id          = $this->user;
        $page->locale_id        = app('app.Languages')->where('language',App::getLocale())->first()->id;
        $page->save();
        //Cache::forget('helpers.pages'.App::getLocale());
        Cache::flush();
        return  redirect()->to('/admin/page/'.$page->slug.'/edit');
    }

    public function edit($slug)
    {
        $page= Page::findBySlugOrIdOrFail($slug);
        $sliders=Gallery::lists('name','id');
        return view('page::page.edit',compact('page','sliders'));
    }

    public function update(PageRequest $request,$slug)
    {
        $page= Page::findBySlugOrIdOrFail($slug);
        $page->title            = Input::get('title');
        $page->description      = Input::get('description');
        $page->content          = Input::get('content');
        Input::get('rm_image') != true ?:$page->cover = null;
        if(Input::get('cover') != null){
            $page->cover = Input::get('cover');
        }
        /*
        if(Input::hasFile('cover')){
            $image      = Input::file('cover');
            $filename   = time() . '.' . $image->getClientOriginalExtension();
            Image::make($image->getRealPath())->save(public_path('uploads/').$filename);
            $page->cover=$filename;
        }*/
        $page->meta_title       = Input::get('meta_title');
        $page->meta_description = Input::get('meta_description');
        $page->meta_keywords    = Input::get('meta_keywords');
        $page->slider           = Input::get('slider')==null?null:Input::get('slider');
        $page->user_id          = $this->user;
        $page->save();
        //Cache::forget('helpers.pages'.App::getLocale());
        Cache::flush();
        return redirect()->to('/admin/page/'.$page->slug.'/edit')
            ->with('message', 'Update Success');
    }

    public function destroy($slug)
    {
        $page=Page::findBySlugOrId($slug);
        $page->delete();
        Cache::flush();
        return redirect()->back();
    }
    
}