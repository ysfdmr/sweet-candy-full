<?php namespace Modules\Page\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PageRequest extends FormRequest {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
    public function rules()
    {
        switch($this->method())
        {
            case 'GET':
            case 'DELETE':
            {
                return [];
            }
            case 'POST':
            {
                return [
                    'title'=> 'required'
                ];
            }
            case 'PUT':
            case 'PATCH':
            {
                return [
                    'title'=> 'required'
                ];
            }
            default:break;
        }
    }

    /**
     * Get the validation custom messages.
     *
     * @return array
     */

    public function messages()
    {
        return [
            'title.required'=> 'Title is required.',
        ];
    }

}
