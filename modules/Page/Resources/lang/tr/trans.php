<?php
return [

    /*
    |--------------------------------------------------------------------------
    | Language Module Name -> Page Name -> Trans Key  page::trans.page.key
    |--------------------------------------------------------------------------
    */

    /* Index list page*/
    'index.page_title' => 'Pages',
    'index.page_description' => 'Management pages.',
    'index.table_title' => 'Page List',
    /* Create page*/
    'create.page_title' => 'Sayfa Oluştur',
    'create.page_description' => 'Yeni sayfa ekleme',
    /* Edit page*/
    'edit.page_title' => 'Sayfa Düzenle',
    'edit.page_description' => 'Sayfa güncelleme',

    'btn.create_page'   => 'Sayfa Oluştur',
    'btn.show_pages'    => 'Tüm Sayfalar',
];

