<?php
return [

    /*
    |--------------------------------------------------------------------------
    | Language Module Name -> Page Name -> Trans Key  page::trans.page.key
    |--------------------------------------------------------------------------
    */

    /* Index list page*/
    'index.page_title' => 'Pages',
    'index.page_description' => 'Management pages.',
    'index.table_title' => 'Page List',
    /* Create page*/
    'create.page_title' => 'Create Page',
    'create.page_description' => 'Add new page.',
    /* Edit page*/
    'edit.page_title' => 'Edit Page',
    'edit.page_description' => 'Update page.',

    'btn.create_page'   => 'Create Page',
    'btn.show_pages'    => 'Show Pages',
];