@extends('admin::layouts.master')

@section('panel.header')
    {{ Lang::get('page::trans.create.page_title') }} <small>{{ Lang::get('page::trans.create.page_description') }}</small>
@stop

@section('panel.style')
    <link rel="stylesheet" href="/assets/admin/plugins/mediazer/mediazer.css"/>
@stop

@section('panel.script')
    <script src="/assets/admin/plugins/mediazer/mediazer-components.js"></script>
    <script src="/assets/admin/plugins/mediazer/mediazer.js"></script>
    <script src="/assets/global/plugins/ckeditor/ckeditor.js"></script>
@stop

@section('panel.content')
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <i class="fa fa-coffee"></i>
                    <h3 class="box-title">{{ Lang::get('global.word.quick-menu') }}</h3>
                </div><!-- /.box-header -->
                <div class="box-body clearfix">
                    <div class="row">
                        <div class="col-lg-12 text-right">
                            <a class="btn btn-app" href="/admin/page">
                                <i class="fa fa-list"></i> {{ Lang::get('page::trans.btn.show_pages') }}
                            </a>
                        </div>
                    </div>

                </div><!-- /.box-body -->
            </div>
        </div>

        <div class="col-lg-12">
            {!! Form::open(['url' => '/admin/page', 'method' => 'post', 'class'=>'','files'=>true]) !!}
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#content-form" data-toggle="tab">Content</a></li>
                        <li><a href="#meta-form" data-toggle="tab">Meta</a></li>
                        <li class="pull-right"><button type="submit" class="btn btn-primary pull-right">{{ Lang::get('global.word.submit') }}</button></li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="content-form">
                            @include('page::page.form')
                        </div><!-- /.tab-pane -->
                        <div class="tab-pane" id="meta-form">
                            @include('page::page.form-meta')
                        </div><!-- /.tab-pane -->
                    </div><!-- /.tab-content -->
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary pull-right">{{ Lang::get('global.word.submit') }}</button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>

@stop