{{--META SECTION START--}}

<div class="form-group">
    <label for="slug" class="control-label">Custom Slug (*{{ Lang::get('global.word.optional') }})</label>
    {!! Form::text('slug', null, ['class' => 'form-control','placeholder'=>'Slug' ]) !!}
</div>

<div class="form-group">
    <label for="meta_title" class="control-label">Meta Title</label>
    {!! Form::text('meta_title', null, ['class' => 'form-control','placeholder'=>'Meta Title']) !!}
</div>

<div class="form-group">
    <label for="content" class="control-label">Meta Description</label>
    {!! Form::textarea('meta_description', null, ['class' => 'form-control','size'=>'2x5']) !!}
</div>

<div class="form-group">
    <label for="meta_title" class="control-label">Meta Keywords</label>
    {!! Form::text('meta_keywords', null, ['class' => 'form-control','placeholder'=>'Meta Keywords']) !!}
</div>
{{--META SECTION END--}}

