<?php

Route::group(['prefix' => '/admin','middleware'=>['auth'], 'namespace' => 'Modules\Blog\Http\Controllers'], function()
{
	Route::resource('/blog', 'BlogController');
    Route::post('/blog/cf-delete','BlogController@cfDelete');

    Route::resource('/blog-category', 'BlogCategoryController');
});