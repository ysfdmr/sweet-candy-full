<?php namespace Modules\Blog\Http\Controllers;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Intervention\Image\Facades\Image;
use Modules\Blog\Entities\Category;
use Modules\Blog\Entities\CustomField;
use Modules\Blog\Entities\Post;
use Modules\Blog\Http\Requests\BlogRequest;
use Pingpong\Modules\Routing\Controller;

class BlogController extends Controller {

    public $user=null;

	public function index()
	{
        $posts=Post::all();
        $categories=Category::lists('title','id');
		return view('blog::blog.index',compact('posts','categories'));
	}

    public function create()
    {
        $categories=Category::lists('title','id');
        return view('blog::blog.create',compact('posts','categories'));
    }

    public function store(BlogRequest $request)
    {
        $post= new Post();
        $post->title            = Input::get('title');
        $post->description      = Input::get('description');
        $post->content          = Input::get('content');
        if(Input::get('cover') != null){
            $post->cover = Input::get('cover');
        }else{
            $post->cover = null;
        }
        $post->meta_title       = Input::get('meta_title');
        $post->meta_description = Input::get('meta_description');
        $post->meta_keywords    = Input::get('meta_keywords');

        $post->category_id      = Input::get('category_id');
        $post->user_id          = $this->user;
        $post->locale_id        = app('app.Languages')->where('language',App::getLocale())->first()->id;
        if($post->save()){
            if(Input::has('custom_field')){
                foreach(Input::get('custom_field') as $key => $value){
                    $customField=new CustomField();
                    $customField->key=$key;
                    $customField->value=$value;
                    $customField->post_id=$post->id;
                    $customField->save();
                }
            }
        }
        Cache::flush();
        return  Redirect::to('/admin/blog/'.$post->slug.'/edit');
    }

    public function edit($slug)
    {
        $post= Post::findBySlugOrIdOrFail($slug);
        $categories=Category::lists('title','id');
        return view('blog::blog.edit',compact('post','categories'));
    }

    public function update(BlogRequest $request,$slug)
    {
        $post= Post::findBySlugOrIdOrFail($slug);
        $post->title            = Input::get('title');
        $post->description      = Input::get('description');
        $post->content          = Input::get('content');
        Input::get('rm_image') != true ?:$post->cover = null;
        if(Input::get('cover') != null){
            $post->cover = Input::get('cover');
        }
        $post->meta_title       = Input::get('meta_title');
        $post->meta_description = Input::get('meta_description');
        $post->meta_keywords    = Input::get('meta_keywords');

        $post->category_id      = Input::get('category_id');
        $post->user_id          = $this->user;
        //$post->locale_id        = app('app.Languages')->where('language',App::getLocale())->first()->id;
        if($post->save()){
            if(Input::has('custom_field')){
                foreach(Input::get('custom_field') as $key => $value){
                    if(!$customField = CustomField::where('key',$key)->where('post_id',$post->id)->first()){
                        $customField = new CustomField();
                        $customField->key=$key;
                        $customField->post_id=$post->id;
                    }
                    $customField->value=$value;
                    $customField->save();
                }
            }
        }
        Cache::flush();
        return redirect()->to('/admin/blog/'.$post->slug.'/edit')->with('message', 'Update Success');
    }

    public function destroy($slug)
    {
        $post=Post::findBySlugOrId($slug);
        $post->delete();
        Cache::flush();
        return redirect()->back();
    }

    public function cfDelete()
    {
        $customField=CustomField::where('key',Input::get('key'));
        $customField->delete();
        Cache::flush();
        return response()->json([
            'data'      =>json_encode(Input::all()),
            'status'    =>'success'
        ]);
    }
}