<?php namespace Modules\Blog\Http\Controllers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Modules\Admin\Entities\Locale;
use Modules\Blog\Entities\Category;
use Modules\Blog\Http\Requests\BlogCategoryRequest;
use Pingpong\Modules\Routing\Controller;

class BlogCategoryController extends Controller {

	public function index()
	{
        $categories=Category::with('Locale')->get();
		return view('blog::category.index',compact('categories'));
	}

    public function create()
    {
        $categories=[null=>'Select Parent Category']+Category::where('status',true)->get()->lists('title','id')->toArray();
        return view('blog::category.create',compact('locales','categories'));
    }

    public function store(BlogCategoryRequest $request)
    {
        $category=new Category();
        $category->title        = Input::get('title');
        $category->parent_id    = Input::get('parent_id')==null?null:Input::get('parent_id');

        $category->status       = Input::get('status');
        $category->description  = Input::get('description');
        //METAS
        $category->meta_title       = Input::get('meta_title');
        $category->meta_description = Input::get('meta_description');
        $category->meta_keywords    = Input::get('meta_keywords');

        $category->locale_id    = app('app.Languages')->where('language',App::getLocale())->first()->id;
        $category->user_id      = null;
        $category->save();
        Cache::flush();

        return Redirect::to('admin/blog-category/'.$category->slug.'/edit')
            ->with('message',  array('type' => 'success', 'head' => 'Info Message!', 'text' => 'Create Success'));
    }

    public function edit($slug)
    {
        $category=Category::findBySlugOrId($slug);
        $categories=[null=>'Select Parent Category']+Category::where('status',true)->get()->lists('title','id')->toArray();
        return view('blog::category.edit',compact('category','locales','categories'));
    }

    public function update(BlogCategoryRequest $request,$slug)
    {
        $category=Category::findBySlugOrId($slug);
        $category->title        = Input::get('title');
        $category->parent_id    = Input::get('parent_id')==null?null:Input::get('parent_id');

        $category->status       = Input::get('status');
        $category->description  = Input::get('description');
        //METAS
        $category->meta_title       = Input::get('meta_title');
        $category->meta_description = Input::get('meta_description');
        $category->meta_keywords    = Input::get('meta_keywords');

        //$category->locale_id    = app('app.Languages')->where('language',App::getLocale())->first()->id;//Locale::where('language',App::getLocale())->first()->id;
        $category->user_id      = null;
        $category->save();
        Cache::flush();
        return Redirect::to('admin/blog-category/'.$category->slug.'/edit')
            ->with('message',  array('type' => 'success', 'head' => 'Info Message!', 'text' => 'Create Success'));
    }

    public function destroy($slug)
    {
        $category=Category::findBySlugOrId($slug);
        $category->delete();
        Cache::flush();
        return redirect()->back();
    }

}