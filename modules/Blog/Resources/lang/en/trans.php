<?php
return [

    /*
    |--------------------------------------------------------------------------
    | Language Module Name -> Page Name -> Trans Key
    |--------------------------------------------------------------------------
    */
    /*blog index page*/
    'blog.index.page_title' => 'Blog Posts',
    'blog.index.page_description' => 'Create, edit and publish new posts ...',
    'blog.index.table_title' => 'Post list',
    /*blog Create page*/
    'blog.create.page_title' => 'Post Create',
    'blog.create.page_description' => 'Create new post',
    /*blog Edit page*/
    'blog.edit.page_title' => 'Post Edit',
    'blog.edit.page_description' => 'Update post',

    /*category index page*/
    'category.index.page_title' => 'Blog Categories',
    'category.index.page_description' => 'Create, edit and publish new Categories ...',
    'category.index.table_title' => 'Category list',
    /*category Create page*/
    'category.create.page_title' => 'Category Create',
    'category.create.page_description' => 'Create new Category',
    /*category Edit page*/
    'category.edit.page_title' => 'Category Edit',
    'category.edit.page_description' => 'Update Category',

    /*Blog Module Buttons*/
    'btn.create_post'       => 'Create Post',
    'btn.show_posts'        => 'Show Posts',
    'btn.create_category'   => 'Create Category',
    'btn.show_categories'   => 'Show Categories',

];