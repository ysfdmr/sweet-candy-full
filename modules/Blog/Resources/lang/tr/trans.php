<?php
return [

    /*
    |--------------------------------------------------------------------------
    | Panel Language Lines
    |--------------------------------------------------------------------------
    */
    /*blog index page*/
    'blog.index.page_title' => 'Blog Yazıları',
    'blog.index.page_description' => 'Yayınlarını oluştur, düzenle ve yayınla ...',
    'blog.index.table_title' => 'Tüm Yazılar',
    /*blog Create page*/
    'blog.create.page_title' => 'Yeni Yazı',
    'blog.create.page_description' => 'Yeni yazı oluşturma.',
    /*blog Edit page*/
    'blog.edit.page_title' => 'Yazı Düzenle',
    'blog.edit.page_description' => 'Yazı güncelleme',

    /*category index page*/
    'category.index.page_title' => 'Yazı Kategorileri',
    'category.index.page_description' => 'Kategori yönetimi',
    'category.index.table_title' => 'Tüm Kategoriler',
    /*category Create page*/
    'category.create.page_title' => 'Kategori Oluştur',
    'category.create.page_description' => 'Yeni kategori oluşturma.',
    /*category Edit page*/
    'category.edit.page_title' => 'Kategori Düzenle',
    'category.edit.page_description' => 'Kategori güncelleme.',

    /*Blog Module Buttons*/
    'btn.create_post'       => 'Yeni Yazı',
    'btn.show_posts'        => 'Tüm Yazılar',
    'btn.create_category'   => 'Yeni Kategori',
    'btn.show_categories'   => 'Tüm Kategoriler',

];