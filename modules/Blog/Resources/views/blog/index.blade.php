@extends('admin::layouts.master')

@section('panel.header')
    {{ Lang::get('blog::trans.blog.index.page_title') }} <small>{{ Lang::get('blog::trans.blog.index.page_description') }}</small>
@stop

@section('panel.style')
    <link rel="stylesheet" href="/assets/modules/blog/css/blog.css"/>
@stop

@section('panel.script')
    <script src="/assets/modules/blog/js/blog.js"></script>
@stop

@section('panel.content')
    <div class="row">
        <div class="col-lg-12">
            <div class="box box-solid">
                <div class="box-header with-border">
                    <i class="fa fa-coffee"></i>
                    <h3 class="box-title">{{ Lang::get('global.word.quick-menu') }}</h3>
                </div><!-- /.box-header -->
                <div class="box-body clearfix">
                    <div class="row">
                        <div class="col-lg-12 text-right">
                            <a class="btn btn-app" href="/admin/blog/create">
                                <i class="fa fa-edit"></i> {{ Lang::get('blog::trans.btn.create_post') }}
                            </a>
                            <a class="btn btn-app" href="/admin/blog-category">
                                <i class="fa fa-list"></i> {{ Lang::get('blog::trans.btn.show_categories') }}
                            </a>
                        </div>
                    </div>

                </div><!-- /.box-body -->
            </div>
        </div>

        <div class="col-lg-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">{{ Lang::get('blog::trans.blog.index.table_title') }}</h3>
                    <a class="custom-toggle" data-fire="table-filter"><i class="fa fa-filter"></i>{{ Lang::get('global.word.filter') }}</a>
                </div><!-- /.box-header -->
                <div class="box-body no-padding">
                    <table class="table table-striped">
                        {{--Filter Section--}}
                        <thead class="table-filter" style="display: none;">
                        <th></th>
                        <th><input class="form-control" type="text" name="title" id="title" placeholder="Search Post Title"/></th>
                        <th>{!! Form::select('status', ['published'=>'Published','draft'=>'Draft','passive'=>'Passive'] , null , ['class' => 'form-control']) !!}</th>
                        <th>{!! Form::select('categories', $categories , null , ['class' => 'form-control']) !!}</th>
                        <th></th>
                        <th><button class="btn btn-block btn-primary">{{ Lang::get('global.word.update') }} <i class="fa fa-search"></i></button></th>
                        </thead>
                        {{--End Filter Section--}}
                        <tbody>
                        <tr>
                            <th style="width: 20px">#</th>
                            <th style="width: 20px">{{ Lang::get('global.word.language') }}</th>
                            <th>{{ Lang::get('global.word.title') }}</th>
                            <th>{{ Lang::get('global.word.status') }}</th>
                            <th>{{ Lang::get('global.word.category') }}</th>
                            <th style="width: 150px">{{ Lang::get('global.word.user') }}</th>
                            <th style="width: 100px">{{ Lang::get('global.word.action') }}</th>
                        </tr>
                        @foreach($posts as $post)
                            <tr>
                                <td>{{ $post->id }}</td>
                                <td><img src="/assets/global/flags/{{ $post->Locale->language }}.png" alt="flag"/></td>
                                <td>{{ $post->title }}</td>
                                <td>{{ $post->status }}</td>
                                <td>{{ $post->category_id }}</td>
                                <td>{{ $post->user_id }}</td>
                                <td>
                                    <a class="action badge bg-green" href="/admin/blog/{{ $post->slug }}/edit" data-toggle="tooltip" data-original-title="Edit">
                                        <i class="fa fa-pencil"></i>
                                    </a>
                                    {!! Form::open(array('url' => array('/admin/blog',$post->id),'method'=> 'DELETE')) !!}
                                    <button type="submit" class="action badge bg-red"
                                            data-toggle="tooltip" data-original-title="delete"
                                            onclick="if(confirm('{{ Lang::get('global.text.deleteQuestion') }}'))return true;else return false;">
                                        <i class="fa fa-close"></i>
                                    </button>
                                    {!! Form::close() !!}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div><!-- /.box-body -->
            </div>
        </div>
    </div>


@stop