{{--META SECTION START--}}
<div class="page-header">
    <h1>Custom Fields <small> Add optional fields for blog post</small></h1>
</div>
<div class="row">
    <div class="col-lg-3" >
        <div class="well">
            <div class="form-group" id="cf-manage">
                <label for="cf-field-name" class="control-label">Add Custom Field</label>
                {!! Form::text('cf-field-name', null, ['class' => 'form-control','placeholder'=>'Key' ]) !!}
                <a class="btn btn-adn form-control cf-add">Add Custom Field</a>
                <i id="cf-status" data-status="{!! $post->id==null?'new':$post->id !!}">
                <h5>Dikkat Edilmesi Gerekenler </h5>
                <ul>
                    <li>Türkçe yada özel karakter kullanılmamalı.</li>
                    <li>Özel karakter yada sayı ile başlamamalı</li>
                    <li>Boşluk olmamalı</li>
                    <li>kelimeler "_" işareti ile bağlanmalıdır.</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="col-lg-9" id="custom-fields">
        @if($post)
            @foreach($post->CustomField as $field)

                    <div class="form-group cf cf-{{ $field->key }}" data-key="{{ $field->key }}">
                        <label class="control-label">
                            Key: {{ $field->key }}
                            <a class="cf-delete badge bg-red" data-key="{{ $field->key }}"
                               data-toggle="tooltip" data-original-title="Delete">
                                <i class="fa fa-close"></i>
                            </a>
                        </label>
                        <input type="text" name="custom_field[{{ $field->key }}]" class="form-control" placeholder="{{ $field->key }}" value="{{ $field->value }}">
                    </div>
            @endforeach
        @else
            @foreach(ThemeComponents::getBlogCustomFields() as $field)
                    <div class="form-group cf cf-{{ $field }}" data-key="{{ $field }}">
                        <label class="control-label">
                            Key: {{ $field }}
                            <a class="cf-delete badge bg-red" data-key="{{ $field }}"
                               data-toggle="tooltip" data-original-title="Delete">
                                <i class="fa fa-close"></i>
                            </a>
                        </label>
                        <input type="text" name="custom_field[{{ $field }}]" class="form-control" placeholder="{{ $field }}" value="">
                    </div>
            @endforeach
        @endif


    </div>
</div>


{{--META SECTION END--}}

