{{--CONTENT SECTION--}}
<div class="form-group {{ $errors->first('title','has-error') }}">
    <label for="title" class="control-label">{{ Lang::get('global.word.title') }}</label>
    {!! Form::text('title', null, ['class' => 'form-control','placeholder'=>Lang::get('global.word.title') ]) !!}
    {{-- $errors->first('title','<p class="help-block">:message</p>') --}}
</div>

<div class="form-group">
    <label for="content" class="control-label">{{ Lang::get('global.word.article') }}</label>
    {!! Form::textarea('content', null, ['class' => 'form-control ckeditor']) !!}
</div>

<div class="row">
    <div class="col-lg-6">
        <div class="row">

            <div class="col-md-6">
                <div class="form-group">
                    <div class="mediazer" data-input="cover" data-select="single" data-source="id"></div>
                </div>
            </div>

            
                <div class="col-md-6">
                    @if($post->cover)
                        <label>
                            {!! Form::checkbox('rm_image', '1', null,  ['id' => 'rm_image']) !!}
                            Remove Cover Image
                        </label> <br/>
                        <img class="img-responsive thumbnail MZ-view-cover" src="{{ Helpers::image($post->Cover->FullPath) }}"  />
                    @else
                        <img class="img-responsive thumbnail MZ-view-cover" src="holder.js/400x300?random=yes&text=No-Photo" alt=""/>
                    @endif
                </div>

        </div>

    </div>

    <div class="col-lg-6">
        <div class="form-group">
            <label for="category_id" class="control-label">{{ Lang::get('global.word.category') }}</label>
            {!! Form::select('category_id',  [''=>'Please Select Category']+$categories->toArray() , null , ['class' => 'form-control','required'=>'required']) !!}
        </div>
    </div>
</div>
{{--CONTENT SECTION END--}}






