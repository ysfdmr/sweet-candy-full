<div class="col-lg-12">
    <div class="box box-solid">
        <div class="box-header with-border">
            <i class="fa fa-coffee"></i>
            <h3 class="box-title">{{ Lang::get('global.word.quick-menu') }}</h3>
        </div><!-- /.box-header -->
        <div class="box-body clearfix">
            <div class="row">
                <div class="col-lg-12 text-right">
                    <a class="btn btn-app" href="/admin/blog-category/create">
                        <i class="fa fa-edit"></i> {{ Lang::get('blog::trans.btn.create_category') }}
                    </a>
                    <a class="btn btn-app" href="/admin/blog-category">
                        <i class="fa fa-list"></i> {{ Lang::get('blog::trans.btn.show_categories') }}
                    </a>
                    <a class="btn btn-app" href="/admin/blog">
                        <i class="fa fa-newspaper-o"></i> {{ Lang::get('blog::trans.btn.show_posts') }}
                    </a>
                </div>
            </div>

        </div><!-- /.box-body -->
    </div>
</div>