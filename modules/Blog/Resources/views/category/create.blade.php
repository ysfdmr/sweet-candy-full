@extends('admin::layouts.master')

@section('panel.header')
    {{ Lang::get('blog::trans.category.create.page_title') }} <small>{{ Lang::get('blog::trans.category.create.page_description') }}</small>
@stop

@section('panel.style')
    <link rel="stylesheet" href="/assets/modules/blog/css/blog.css"/>
@stop

@section('panel.script')

    <script src="/assets/global/plugins/ckeditor/ckeditor.js"></script>
    <script src="/assets/modules/blog/js/blog.js"></script>
@stop

@section('panel.content')
    <div class="row">
        @include('blog::category.quick-menu')
        <div class="col-lg-12">
            {!! Form::open(['url' => '/admin/blog-category', 'method' => 'post', 'class'=>'']) !!}
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a href="#content-form" data-toggle="tab">
                                {{ Lang::get('global.word.category') }} {{ Lang::get('global.word.info') }}
                            </a>
                        </li>
                        <li>
                            <a href="#meta-form" data-toggle="tab">
                                Meta
                            </a>
                        </li>
                        <li class="pull-right"><button type="submit" class="btn btn-primary pull-right">{{ Lang::get('global.word.save') }}</button></li>
                    </ul>

                    <div class="tab-content">
                        <div class="tab-pane active" id="content-form">
                            @include('blog::category.form')
                        </div><!-- /.tab-pane -->
                        <div class="tab-pane" id="meta-form">
                            @include('blog::category.form-meta')
                        </div><!-- /.tab-pane -->
                    </div><!-- /.tab-content -->
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary pull-right">{{ Lang::get('global.word.save') }}</button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>

@stop