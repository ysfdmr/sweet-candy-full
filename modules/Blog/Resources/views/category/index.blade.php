@extends('admin::layouts.master')

@section('panel.header')
    {{ Lang::get('blog::trans.category.index.page_title') }} <small>{{ Lang::get('blog::trans.category.index.page_description') }}</small>
@stop

@section('panel.style')
    <link rel="stylesheet" href="/assets/modules/blog/css/blog.css"/>
@stop

@section('panel.script')
    <script src="/assets/modules/blog/js/blog.js"></script>
@stop

@section('panel.content')
    <div class="row">
        @include('blog::category.quick-menu')
        <div class="col-lg-12">
            <div class="box box-primary">
                <div class="box-header">
                    <h3 class="box-title">{{ Lang::get('blog::trans.category.index.table_title') }}</h3>
                </div><!-- /.box-header -->
                <div class="box-body no-padding">
                    <table class="table table-striped">
                        <tbody>
                        <tr>
                            <th style="width: 20px">#</th>
                            <th style="width: 20px">{{ Lang::get('global.word.language') }}</th>
                            <th>{{ Lang::get('global.word.title') }}</th>
                            <th>Slug</th>
                            <th>{{ Lang::get('global.word.status') }}</th>
                            <th style="width: 100px">{{ Lang::get('global.word.action') }}</th>
                        </tr>

                            @foreach($categories as $category)
                                <tr>
                                    <td>{{ $category->id }}</td>
                                    <td><img src="/assets/global/flags/{{ $category->Locale->language }}.png" alt="flag"/></td>
                                    <td>{{ $category->title }}</td>
                                    <td>{{ $category->slug }}</td>
                                    <td>{{ $category->status==true?'ACTIVE':'PASSIVE' }}</td>
                                    <td>
                                        <a class="action badge bg-green" href="/admin/blog-category/{{ $category->slug }}/edit" data-toggle="tooltip" data-original-title="Edit">
                                            <i class="fa fa-pencil"></i>
                                        </a>
                                        {!! Form::open(array('url' => array('/admin/blog-category',$category->id),'method'=> 'DELETE')) !!}
                                        <button type="submit" class="action badge bg-red"
                                                data-toggle="tooltip" data-original-title="delete"
                                                onclick="if(confirm('{{ Lang::get('global.text.deleteQuestion') }}'))return true;else return false;">
                                            <i class="fa fa-close"></i>
                                        </button>
                                        {!! Form::close() !!}
                                    </td>
                                </tr>
                            @endforeach


                        </tbody>
                    </table>
                </div><!-- /.box-body -->
            </div>
        </div>
    </div>


@stop