
<div class="form-group {{ $errors->first('name','has-error') }}">
    <label for="title" class="control-label">{{ Lang::get('global.word.title') }}</label>
    {!! Form::text('title', null, ['class' => 'form-control','placeholder'=>Lang::get('global.word.title') ]) !!}
</div>

<div class="form-group">
    <label for="description" class="control-label">{{ Lang::get('global.word.description') }}</label>
    {!! Form::textarea('description', null, ['class' => 'form-control ckeditor']) !!}
</div>

<div class="row">
    <div class="col-lg-6">
        <div class="form-group">
            <label for="parent_id" class="control-label">{{ Lang::get('global.word.parent') }} {{ Lang::get('global.word.category') }}</label>
            {!! Form::select('parent_id', $categories , null , ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-lg-6">
        <div class="form-group">
            <label for="status" class="control-label">{{ Lang::get('global.word.status') }}</label>
            {!! Form::select('status', [1=>'Active',0=>'Passive'] , null , ['class' => 'form-control']) !!}
        </div>
    </div>
</div>











