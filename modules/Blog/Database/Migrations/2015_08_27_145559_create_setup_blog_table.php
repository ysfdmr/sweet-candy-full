<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSetupBlogTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('categories', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');

            //nested
            $table->integer('parent_id')->nullable()->index();
            $table->integer('lft')->nullable()->index();
            $table->integer('rgt')->nullable()->index();
            $table->integer('depth')->nullable();
            //nested

            $table->string('title');
            $table->string('slug');
            $table->text('description')->nullable()->default(null);
            $table->string('meta_title')->nullable();
            $table->string('meta_description')->nullable();
            $table->string('meta_keywords')->nullable();

            $table->boolean('status')->default(true);
            //relation
            $table->integer('user_id')->unsigned()->nullable()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');

            $table->integer('locale_id')->unsigned()->index();
            $table->foreign('locale_id')->references('id')->on('locales')->onDelete('cascade');

            $table->timestamps();
        });

        /*
        Schema::create('category_translations', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');

            // Translatable attributes
            $table->string('title');
            $table->string('slug');
            $table->text('description')->nullable()->default(null);
            $table->string('meta_title');
            $table->string('meta_description');
            $table->string('meta_keywords');
            // Translatable attributes

            $table->integer('category_id')->unsigned();
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');
            $table->integer('locale_id')->unsigned()->index();
            $table->foreign('locale_id')->references('id')->on('locales')->onDelete('cascade');

            $table->unique(['category_id','locale_id']);

            $table->timestamps();
        });
*/
        Schema::create('blog_posts', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->string('title');
            $table->string('slug');
            $table->text('description')->nullable()->default(null);
            $table->longText('content');
            $table->string('meta_title')->nullable();
            $table->string('meta_description')->nullable();
            $table->string('meta_keywords')->nullable();
            $table->enum('status', array('PUBLISHED','DRAFT', 'PASSIVE'))->default('DRAFT');


            $table->unsignedInteger('category_id')->nullable()->index();
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('set null');

            $table->unsignedInteger('cover')->nullable()->index();
            $table->foreign('cover')->references('id')->on('images')->onDelete('set null');

            $table->unsignedInteger('user_id')->nullable()->index();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('set null');

            $table->integer('locale_id')->unsigned()->index();
            $table->foreign('locale_id')->references('id')->on('locales')->onDelete('cascade');
            $table->timestamps();
        });

        Schema::create('blog_custom_fields', function(Blueprint $table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');

            $table->string('key');
            $table->text('value')->nullable()->default(null);

            $table->unsignedInteger('post_id')->nullable()->index();
            $table->foreign('post_id')->references('id')->on('blog_posts')->onDelete('cascade');

            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('categories');
        //Schema::drop('category_translations');
        Schema::drop('blog_posts');
        Schema::drop('blog_custom_fields');
    }

}
