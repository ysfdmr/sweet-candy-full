<?php namespace Modules\Blog\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use Modules\Blog\Entities\Category;

class BlogDatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();
        Category::create(array(
            'locale_id'  => 1,
            'title'      => 'News',
            'status'    => 1
        ));

        Category::create(array(
            'locale_id'  => 2,
            'title'      => 'Haberler',
            'status'    => 1
        ));

        $this->command->info('BlogDatabaseSeeder table seeded!');
		// $this->call("OthersTableSeeder");
	}

}