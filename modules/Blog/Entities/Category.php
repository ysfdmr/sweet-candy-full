<?php namespace Modules\Blog\Entities;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use bmcms\scopes\languageTrait;

class Category extends Model implements SluggableInterface{

    protected $table = 'categories';
    public $timestamps = true;
    protected $fillable = [];



    use SluggableTrait;

    protected $sluggable = [
        'build_from' => 'title',
        'save_to'    => 'slug',
        'on_update'  => true
    ];
    use languageTrait;
    public function Locale()
    {
        return $this->belongsTo('Modules\Admin\Entities\Locale','locale_id','id');
    }
}