<?php namespace Modules\Blog\Entities;

use Illuminate\Database\Eloquent\Model;

class CustomField extends Model {

    protected $table = 'blog_custom_fields';
    public $timestamps = true;
    protected $fillable = [];

}