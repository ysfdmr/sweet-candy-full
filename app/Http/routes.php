<?php
/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

/*
Route::get('/photo/{size}/{name}', function($size = NULL, $name = NULL){
    if(!is_null($size) && !is_null($name)){
        $size = explode('x', $size);
        $cache_image = Image::cache(function($image) use($size, $name){
            return $image->make(url('/photos/'.$name))->resize($size[0], $size[1]);
        }, 40); // cache for 10 minutes

        return Response::make($cache_image, 200, ['Content-Type' => 'image']);
    } else {
        abort(404);
    }
}); ^\d{3}+[a-z]{1}+\d{3}$
*/
Route::group(array('prefix' => 'photo'), function() {
    ini_set('memory_limit','256M');
    Route::get('{w}/{h}/{src}', function($w,$h,$src) {
        if(!is_null($w) && !is_null($h) && !is_null($src)){
            $cache_image = Image::cache(function($image) use($w,$h, $src){
                return $image->make(url($src))->resize($w, $h);
            }, 60);
            return Response::make($cache_image, 200, ['Content-Type' => 'image/jpeg']);
        } else {
            abort(404);
        }
    })  ->where('w','[0-9]+')
        ->where('h','[0-9]+')
        ->where('src','.*');

    Route::get('crop/{w}/{h}/{src}', function($w,$h,$src) {
        if(!is_null($w) && !is_null($h) && !is_null($src)){
            $cache_image = Image::cache(function($image) use($w,$h, $src){
                return $image->make(url($src))->crop($w, $h);
            }, 60);
            return Response::make($cache_image, 200, ['Content-Type' => 'image/jpeg']);
        } else {
            abort(404);
        }
    })  ->where('w','[0-9]+')
        ->where('h','[0-9]+')
        ->where('src','.*');


    Route::get('{src}', function($src) {
        //$cache_image = Image::cache(function($image) use($src){
            return $src->make(url($src));
        //}, 60);
        //return Response::make($cache_image, 200, ['Content-Type' => 'image']);
    })->where('src','.*');
});

Route::get('sc', function() {


    $text = '[a href="test"]Click here[/a]';
    $text .= '<br>HARİTA ALTTA<br>';
    $text .= '[map type="base"]HERE[/map]';
    echo Shortcode::compile($text);

});
if(app()->isLocal()){
    Route::get('bmlog', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
}


