<?php namespace App\Http\Controllers;
//TODO: Eğer Theme Dosyasını Bulamazsa 404 Atsın;
$themePath = public_path().'/themes/'.\Caffeinated\Themes\Facades\Theme::getActive().'/Theme.php';
use Illuminate\Support\Facades\App;
use Theme\Methods;

//App::abort(404);
if (file_exists($themePath)) {
    include_once $themePath;
} else {
    App::abort(404);
}

class ThemeController extends Controller {
    use Methods\Actions;
}