<?php

namespace App\Providers;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Modules\Admin\Entities\Locale;
use Caffeinated\Themes\Facades\Theme;
use Modules\Admin\Entities\Setting;
use Modules\Admin\Entities\SettingTrans;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        error_reporting(E_ERROR | E_WARNING | E_PARSE);


        App::singleton('app.Languages', function(){
            return  Cache::remember('app.Languages'.App::getLocale(),10080, function() {
                return Locale::all();
            });
        });
        App::singleton('app.setting', function(){
            return  Cache::remember('app.setting'.App::getLocale(),10080, function() {
                return Setting::all();
            });
        });
        App::singleton('app.settingTrans', function(){
            return  Cache::remember('app.settingTrans'.App::getLocale(),10080, function() {
                return SettingTrans::all();
            });
        });


        /**
        * Set Data Global Variable
        */
        $Data=[];
        if (Schema::hasTable('settings')){
            $Data['SETTINGS']=app('app.setting');
            Theme::setActive($Data['SETTINGS']->where('key','theme')->first()->value);
        }

        $activeTheme=Theme::getActive();
        $this->loadTranslationsFrom(public_path('themes/' . $activeTheme .'/lang/') , 'theme' );
        // dd(public_path('themes/' . $activeTheme .'/lang/'));

        /**
        * Share Data All Views
        */

        if (Schema::hasTable('locales')){
            $Data['LOCALE']=app('app.Languages')->lists('name','language');
        }

        view()->share('APP', $Data);

        // If you use this line of code then it'll be available in any view
        // as $site_settings but you may also use app('globalLanguages') as well
        //View::share('globalLanguages', app('globalLanguages'));
        Event::listen('illuminate.query.saving', function($query)
        {
            var_dump($query);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
