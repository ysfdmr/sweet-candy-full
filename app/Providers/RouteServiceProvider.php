<?php

namespace App\Providers;

use Caffeinated\Themes\Facades\Theme;
use Illuminate\Routing\Router;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\App;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * This namespace is applied to the controller routes in your routes file.
     *
     * In addition, it is set as the URL generator's root namespace.
     *
     * @var string
     */
    protected $namespace = 'App\Http\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function boot(Router $router)
    {
        //

        parent::boot($router);
    }

    /**
     * Define the routes for the application.
     *
     * @param  \Illuminate\Routing\Router  $router
     * @return void
     */
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function ($router) {
            require app_path('Http/routes.php');


            $themePath = public_path().'/themes/'.\Caffeinated\Themes\Facades\Theme::getActive();
            $themeController    = $themePath.'/Theme.php';
            $themeRoute         = $themePath.'/Routes.php';
            if (file_exists($themeController) && file_exists($themeRoute)) {
                require_once $themeController;
                require_once $themeRoute;
            } else {
                return view('theme-crash');
            }
        });
    }
}
