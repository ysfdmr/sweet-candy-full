<?php $theme = Helpers::getSettings('theme'); ?>
@extends("$theme::layout.master")

@section('body.class')
@stop

@section('page.script')
<script>

</script>
@stop

@section('page.body')

    <div class="container">
        <div class="row">
            <div class="col-lg-3 sidebar">
                <span class="title">
                @if($category->parent_id == null)
                    {{$category->title}} - <a href="/products/" style="color:black;text-decoration:none;">All</a>
                @else
                    {{ Modules\Product\Entities\ProductCategory::where('id',$category->parent_id)->first()->title }} 
                    - <a href="/products/" style="color:black;text-decoration:none;">All</a>
                @endif
                </span>
                <ul class="menu-1">
                    @foreach($categories as $cat)
                        <?php
                            $catID=$category->id;
                            if($category->parent_id != null){
                                $catID=$category->parent_id;
                            }
                        ?>
                        {{-- @if($category->parent_id != null) --}}
                            @if($cat->id == $catID)
                                {{-- <li class=" @if($cat->id==$category->id) active @endif ">
                                    <a href="/products/category/{{ $cat->slug }}">
                                        {{ $cat->title }}
                                    </a> --}}
                                    @if($cat->children)
                                        @foreach($cat->children as $child)
                                        <li class=" @if($child->id==$category->id) active @endif ">
                                            <a href="/products/category/{{ $child->slug }}">
                                                {{ $child->title }}
                                            </a>
                                        </li>
                                        @endforeach
                                    @endif
                                {{-- </li>     --}}
                            @endif
                        {{-- @endif --}}
                        
                        
                    @endforeach
                </ul>
            </div>
            <div class="col-lg-9 list">
                <div class="row">
                {{-- {{ dd($gallery) }} --}}
                        @if($gallery)

                            @foreach($gallery as $slide)
                                <div class="col-lg-4">
                                    <a href="{{ Helpers::getImage($slide->Image) }}" data-fancybox="group" data-caption="{{ $product->title }}">
                                        <img src="{{Helpers::getImage($slide->Image,260,200)}}" alt="{{ $product->title }}" class="img-responsive full-img img-thumbnail" />
                                    </a>
                                </div>
                            @endforeach

                        @endif
                </div>
            </div>

        </div>
    </div>
                   
@stop