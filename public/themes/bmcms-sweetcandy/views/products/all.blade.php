<?php $theme = Helpers::getSettings('theme'); ?>
@extends("$theme::layout.master")

@section('body.class')
@stop

@section('page.script')
<script>

</script>
@stop

@section('page.body')

    <div class="container">
        <div class="row">
            <div class="col-lg-3 sidebar">
                <span class="title">
                    Categories
                </span>
                <ul class="menu-1">
                    <li class=" @if(!$category) active @endif ">
                        <a href="/products/">
                            All
                        </a>
                    </li>
                    @foreach($categories as $cat)
                        @if(is_null($cat->children->first()))
                        <li>
                            <a href="/products/category/{{ $cat->slug }}">
                                {{ $cat->title }}
                            </a>
                        </li>
                        @else
                        <li>
                            <a href="/products/category/{{ $cat->children->first()->slug }}">
                                {{ $cat->title }}
                            </a>
                        </li>
                        @endif

                    @endforeach
                    {{-- @foreach($categories as $cat)
                        <li class=" @if($cat->id==$category->id) active @endif ">
                            <a href="/products/category/{{ $cat->slug }}">
                                {{ $cat->title }}
                            </a>
                            @if($cat->children)

                                @foreach($cat->children as $child)
                                <li class=" @if($child->id==$category->id) active @endif ">
                                    <a href="/products/category/{{ $child->slug }}">
                                        &nbsp;&nbsp;&nbsp;&nbsp;{{ $child->title }}
                                    </a>
                                </li>
                                @endforeach
                            @endif
                        </li>
                            
                    @endforeach --}}
                </ul>
            </div>
            <div class="col-lg-9 list">
                <div class="row">
                    @foreach($products as $product)
                        @if($product->Gallery)
                            @foreach($product->Gallery as $slide)
                                <div class="col-lg-4">
                                    <a href="{{Helpers::getImage($slide->Image)}}" data-fancybox="group" data-caption="{{ $slide->Image->title }}">
                                        <img src="{{Helpers::getImage($slide->Image,260,200)}}" alt="{{ $slide->Image->title }}" class="img-responsive full-img img-thumbnail" />
                                    </a>
                                </div>
                            @endforeach
                        @endif
                    @endforeach
                </div>
            </div>

        </div>
    </div>
                   
@stop