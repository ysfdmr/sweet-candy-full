<?php $theme=Helpers::getSettings('theme'); ?>
@extends("$theme::layout.master")

@section('page.style')
    <style>
        .menu-toggle{
            background-color: #a7a7a7;
            border-radius: 0px;
            color: black;
        }
        #locations-panel {
            background-color: #ffffff;
            width: 150px;
        }
        #locations-panel span {
            display: block;
            font-size: 20px;
            color: black;
        }
        #locations-panel select {
            height: 50px;
            line-height: 50px;
            border: none;
            width: 100%;
            background-color: rgba(0, 0, 0, 0.09);
            border-radius: 0px !important;
        }

    </style>
@stop
@section('page.script')
    <script src="https://maps.googleapis.com/maps/api/js?callback=initMap&key=AIzaSyBGWMg9-5Vss0jfxdSrEWEvVBGSu6Ax2nE" async defer></script>
    <!-- GOOGLE MAP -->
    <script>
        <?php $info= str_replace(array("\r", "\n"), '', Helpers::getSettings('company_info','Please Set Campany Info'))?>
        var BaseMap= {
            latlng:{lat: {{ Helpers::getSettings('company_lat') }}, lng: {{ Helpers::getSettings('company_long') }}},
            long: {{ Helpers::getSettings('company_long') }},
            lat: {{ Helpers::getSettings('company_lat') }},
            name: '{{ Helpers::getSettings('company_name','Please Set Campany Name') }}',
            info    : '{{ $info }}',
        };
    </script>

    <script>

        function initMap() {
            var directionsDisplay = new google.maps.DirectionsRenderer;
            var directionsService = new google.maps.DirectionsService;
            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 15,
                center: BaseMap.latlng,
                zoomControl: true,
                mapTypeControl: false,
                scaleControl: false,
                streetViewControl: false,
                rotateControl: false,
                fullscreenControl: true,
                fullscreenControlOptions: {
                    position: google.maps.ControlPosition.RIGHT_BOTTOM
                },
                scrollwheel: false,


                //disableDefaultUI: true
            });

            var marker = new google.maps.Marker({
                position: BaseMap.latlng,
                map: map,
                customName: 'baseMap',
                animation: google.maps.Animation.DROP,
                title: BaseMap.name
            });
            var control = document.getElementById('locations-panel');

            map.controls[google.maps.ControlPosition.LEFT_CENTER].push(control);
            setTimeout(function () {control.style.display = 'block';}, 200)
            directionsDisplay.setMap(map);

            var onChangeHandler = function () {
                calculateAndDisplayRoute(directionsService, directionsDisplay);
            };
            document.getElementById('start').addEventListener('change', onChangeHandler);
            //document.getElementById('end').addEventListener('change', onChangeHandler);

            function calculateAndDisplayRoute(directionsService, directionsDisplay) {
                directionsService.route({
                    origin: document.getElementById('start').value,
                    destination: BaseMap.latlng,
                    travelMode: google.maps.TravelMode.DRIVING
                }, function (response, status) {
                    if (status === google.maps.DirectionsStatus.OK) {
                        directionsDisplay.setDirections(response);
                    } else {
                        window.alert('Directions request failed due to ' + status);
                    }
                });
            }
        }

    </script>
@stop

@section('page.body')

    <div id="locations" class="hero is-bg-image is-text-light is-centered">
        <div id="locations-panel" style="display: none">
            <span>Where ? </span>
            <select id="start" onchange="">
                <option value="">Select Point</option>
                @foreach($locations as $location)
                    <option value="{{ $location->map_address }}">{{ $location->title }}</option>
                @endforeach
            </select>
        </div>
        <div id="map" style="height: 600px;"></div>
    </div>
    <div class="container">
        <div class="columns">
            <div class="column is-12">
                <div id="primary" class="content-area">
                    <main id="main" class="site-main" role="main">
                        <div  class="room type-room status-publish has-post-thumbnail hentry roomcategory-suites">
                            <div class="entry-content">
                                <h2>{{ $page->title }}</h2>
                                <p>{!! $page->content !!}</p>
                            </div>
                        </div>
                    </main>
                </div>
            </div>

        </div>
    </div>


@stop


