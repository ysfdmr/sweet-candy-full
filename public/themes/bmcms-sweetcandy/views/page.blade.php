<?php $theme=Helpers::getSettings('theme'); ?>
@extends("$theme::layout.master")


@section('body.class') page-templaste @stop

@section('page.style')
@stop

@section('page.script')
    <script type="text/javascript">
        var tpj=jQuery;
        var revapi6;
        tpj(document).ready(function() {
            if(tpj("#rev_slider_6_1").revolution == undefined){
                revslider_showDoubleJqueryError("#rev_slider_6_1");
            }else{
                revapi6 = tpj("#rev_slider_6_1").show().revolution({
                    sliderType:"standard",
                    jsFileLocation:"/themes/{{$theme}}/assets/build/",
                    sliderLayout:"auto",
                    dottedOverlay:"none",
                    delay:9000,
                    navigation: {
                        keyboardNavigation:"off",
                        keyboard_direction: "horizontal",
                        mouseScrollNavigation:"off",
                            mouseScrollReverse:"default",
                        onHoverStop:"on",
                        touch:{
                            touchenabled:"on",
                            swipe_threshold: 75,
                            swipe_min_touches: 50,
                            swipe_direction: "horizontal",
                            drag_block_vertical: false
                        }
                        ,
                        arrows: {
                            style:"uranus",
                            enable:true,
                            hide_onmobile:true,
                            hide_under:600,
                            hide_onleave:false,
                            tmp:'',
                            left: {
                                h_align:"left",
                                v_align:"center",
                                h_offset:30,
                                v_offset:0
                            },
                            right: {
                                h_align:"right",
                                v_align:"center",
                                h_offset:30,
                                v_offset:0
                            }
                        }
                        ,
                        thumbnails: {
								style:"gyges",
								enable:true,
								width:50,
								height:50,
								min_width:50,
								wrapper_padding:5,
								wrapper_color:"transparent",
								wrapper_opacity:"1",
								tmp:'<span class="tp-thumb-img-wrap">  <span class="tp-thumb-image"></span></span>',
								visibleAmount:5,
								hide_onmobile:false,
								hide_onleave:false,
								direction:"horizontal",
								span:false,
								position:"inner",
								space:5,
								h_align:"center",
								v_align:"bottom",
								h_offset:0,
                                v_offset:20
                        }
                    },
                    responsiveLevels:[1240,1024,778,480],
                    visibilityLevels:[1240,1024,778,480],
                    gridwidth:[1240,1024,778,480],
                    gridheight:[600,600,500,400],
                    lazyType:"smart",
                    parallax: {
                        type:"mouse",
                        origo:"slidercenter",
                        speed:2000,
                        levels:[2,3,4,5,6,7,12,16,10,50,47,48,49,50,51,55],
                        type:"mouse",
                    },
                    shadow:0,
                    spinner:"spinner2",
                    stopLoop:"on",
                    stopAfterLoops:0,
                    stopAtSlide:1,
                    shuffle:"off",
                    autoHeight:"off",
                    disableProgressBar:"on",
                    hideThumbsOnMobile:"off",
                    hideSliderAtLimit:0,
                    hideCaptionAtLimit:0,
                    hideAllCaptionAtLilmit:0,
                    debugMode:false,
                    fallbacks: {
                        simplifyAll:"off",
                        nextSlideOnWindowFocus:"off",
                        disableFocusListener:false,
                    }
                });
            }
        });	/*ready*/
    </script>
@stop

@section('page.body')


    <div id="contentContainer" class="carousel-fluid">
        <div id="content" class="container">
            <div class="row">
                <div class="description col-md-9 col-lg-9">
                    @if($gallery->Items)
                        <div id="rev_slider_6_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="test" data-source="gallery" style="margin:0px auto;background-color:transparent;padding:0px;margin-top:0px;margin-bottom:0px;">
                            <div id="rev_slider_6_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.3.0.2.1">
                                <ul>	
                                    @foreach($gallery->Items as $index => $slide)
                                    <li data-index="rs-{{$index}}" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="300"  
                                        data-thumb="{{ Helpers::getImage($slide->Image,50,50) }}"  data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="2000" data-fsslotamount="7" data-saveperformance="off"  data-title="Slide">
                                        <img src="assets/dummy.png"  alt=""  data-lazyload="{{ Helpers::getImage($slide->Image) }}" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="off" class="rev-slidebg" data-no-retina>
                                    </li>
                                    @endforeach
                                </ul>
                                <div class="tp-bannertimer tp-bottom" style="visibility: hidden !important;"></div>	
                            </div>
                        </div>
                    @endif
                    <br>
                    <h2>{{$post->title}}</h2>
                    <div class="spotText">
                        {!! $post->content !!}
                    </div>
                    
                </div>

                <div class="right-col visible-md visible-lg col-md-3 col-lg-3">
                    
                </div>
            </div>
        </div>
    </div>

@stop


