<?php $theme=Helpers::getSettings('theme'); ?>
@extends("$theme::layout.master")

@section('body.class') home @stop

@section('page.style')
@stop

@section('page.script')

@stop

@section('page.body')

@if($page->getSlider)
<section class="slider">
    <div class="container">
        <div class="rev_slider_wrapper">
            <div id="topslider" class="rev_slider" style="display:none">
                <ul>
                    @foreach($page->getSlider->Items as $index => $slide)
                        <li data-transition="fade">
                            <img src="/themes/{{$theme}}/assets/img/dummy.png"   data-lazyload="{{ Helpers::getImage($slide->Image) }}" class="rev-slidebg" data-no-retina>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </div>
</section>
@endif


<section class="bg-1 categories">
    <div class="container">
        <h2>Product Categories</h2>
        @if($categories)
            <div class="row">
                            {{-- {{ dd($categories) }} --}}
                <div class="col-lg-offset-1 invisible-lg col-lg-1">&nbsp;</div>
                @foreach($categories as $category)
                    <div class="col-lg-2 col-md-3 col-sm-4 col-xs-6 item">
                    
                        @if(is_null($category->children->first()))
                        <a href="/products/category/{{ $category->slug }}">
                            <img src="{{Helpers::getImage($category->Cover,250,250)}}" alt="img" class="img-circle img-responsive">
                        </a>
                        @else
                        <a href="/products/category/{{ $category->children->first()->slug }}">
                            <img src="{{Helpers::getImage($category->Cover,250,250)}}" alt="img" class="img-circle img-responsive">
                        </a>
                        @endif
                        <span class="title">{{ $category->title }}</span>
                    </div>
                @endforeach
            </div>
        @endif
        <div class="clearfix"></div>
    </div>
</section>

@if($sweetmoments)
<div class="section">
    <div class="container">
        <h2 class="text-center">Sweet Moments</h2>
        @foreach($sweetmoments->Items as $slide)
            <div class="col-lg-2 mb10">
                <a href="{{Helpers::getImage($slide->Image)}}" data-fancybox="group" data-caption="{{ $slide->Image->title }}">
                    <img src="{{Helpers::getImage($slide->Image,260,200)}}" alt="{{ $slide->Image->title }}" class="img-responsive full-img img-thumbnail" />
                </a>
            </div>
        @endforeach
        <div class="clearfix"></div>
    </div>
</div>
<br>
@endif

@stop

