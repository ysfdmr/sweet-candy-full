<?php $theme=Helpers::getSettings('theme'); ?>
@extends("$theme::layout.master")

@section('body.class')
 contact-page
@stop
@section('page.style')
    <link rel="stylesheet" href="/assets/global/plugins/validate/validetta.min.css">
    <style>
    input[type='text'],textarea {
        background: #fbfaf8 !important;
        border: 1px solid #ceb895 !important;
        color: #512a2b !important;
        border-radius: 30px !important;
        padding: 14px 8px !important;
    }
    input[type='text']{
        height:50px;
    }

    .btn-primary{
        color: #ffffff;
        font-size: 12px;
        font-weight: 500;
        font-family: Brandon, Arial, Helvetica, sans-serif;
        text-transform: uppercase;
        filter: none;
        border: none;
        box-shadow: none;
        border-radius: 20px;
        text-shadow: none;
        background: #d3ad73;
        -webkit-transition: .2s linear;
        -moz-transition: .2s linear;
        -o-transition: .2s linear;
        transition: .2s linear;
    }
    </style>
@endsection
@section('page.script')
    <script src="/assets/global/plugins/validate/validetta.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBGWMg9-5Vss0jfxdSrEWEvVBGSu6Ax2nE"></script>
    <script src="/assets/global/plugins/gmap/gmaps.js"></script>
    <script>
        $(document).ready(function () {
            var ContactMap={
                long    : {{ Helpers::getSettings('company_long') }},
                lat     : {{ Helpers::getSettings('company_lat') }},
                name    : '{{ Helpers::getSettings('company_name','Please Set Campany Name') }}',
                name2   : '{{ Helpers::getSettings('company_name','Please Set Campany Name') }}',
            };

            map = new GMaps({
                div: '#google-map',
                lat: ContactMap.lat,
                lng: ContactMap.long,
                mapTypeControl: true,
                mapTypeControlOptions: {
                    style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
                    position: google.maps.ControlPosition.TOP_CENTER
                },
                zoomControl: true,
                zoomControlOptions: {
                    position: google.maps.ControlPosition.LEFT_CENTER
                },
                scaleControl: true,
                streetViewControl: false,
                streetViewControlOptions: {
                    position: google.maps.ControlPosition.LEFT_TOP
                },
                fullscreenControl: true,
                 scrollwheel:false,
                zoom:16,
            });
            map.addMarker({
                lat: ContactMap.lat,
                lng: ContactMap.long,
                title: ContactMap.name,
                infoWindow: {
                    content: ContactMap.name+'<br>'+ContactMap.info
                }
            });
        });

    </script>
    <script>

    var body=$('body');
        var token='&_token='+$("meta[name=\"csrf-token\"]").attr('content');

        /***
            * Validate Restaurant Form
            */
            $('.forms').validetta({
                realTime : true,
                display : 'bubble', // bubble or inline
                //errorTemplateClass : 'validetta-inline',
                bubblePosition: 'bottom', // Bubble position // right / bottom
                bubbleGapLeft: 15, // Right gap of bubble (px unit)
                bubbleGapTop: 0,
                onValid : function( event ) {
                    event.preventDefault();
                    $.ajax({
                        url : '/email/send',
                        data : $(this.form).serialize()+token,
                        type:'post',
                        beforeSend : function() {
                            $.helpers.disableSubmit(event.target,true);
                        }
                    }).done( function( data ){
                        console.log(data.message,'Success');
                        $.helpers.resetForm(event.target);
                        $.helpers.disableSubmit(event.target,false);
                        $.helpers.successForm(event.target,'.success-send',true);
                    }).fail( function( jqXHR, textStatus){
                        if(jqXHR.status==422){
                            console.log(jqXHR.responseText,'Validation Error');
                        }else{
                            console.log(textStatus+':'+jqXHR.status+' : '+jqXHR.statusText);
                        }
                    }).always( function( result ){
                        $.helpers.disableSubmit(event.target,false);
                    });
                }
            });


        /**
        * HELPERS
        * */

        /*Controls*/
        (function( $, window, undefined ) {
            $.helpers = $.extend( {}, {

                successForm:function(formID,successDiv,action){//hide form show success div
                    if(action){
                        $(formID).hide();
                        $(successDiv).show();
                    }else{
                        $(formID).show();
                        $(successDiv).hide();
                    }
                },

                resetForm:function(formID){
                    $(':input',formID)
                        .not(':button, :submit, :reset, :hidden')
                        .val('')
                        .removeAttr('checked')
                        .removeAttr('selected');
                },

                disableSubmit:function(form,action){
                    $(form).find(':submit').prop('disabled', action);
                }

            }, $.helpers);
        })(jQuery);
    </script>
@stop

@section('page.body')

    <div id="contentContainer" class="carousel-fluid">
        <div id="content" class="container">
            <div class="row">
                <div class="description col-md-12 col-lg-12">
                    <div class="spotText">
                        <div id="google-map" style="max-height: 600px;height:500px;"></div>
                         <br>
                    </div>
                    <br>
                </div>

                <div class="col-lg-5">
                    <form id="frm-contact" class="forms">
                        <input name="email_header" type="hidden" value="Contact Form"/>
                        <div class="">
                            <div class="form-group">
                                <div class="row">
                                    <div class="form-group col-xs-12">
                                        <label for="name">Name Surname</label>
                                        <input class="form-control" type="text" name="name" data-validetta="required,minLength[3]"/>
                                    </div>
                                    {{-- <div class="form-group col-xs-6">
                                        <label for="surname">Surname</label>
                                        <input class="form-control" type="text" name="name" data-validetta="required,minLength[3]"/>
                                    </div> --}}
                                    <div class="form-group col-xs-12">
                                        <label for="check">E-mail</label>
                                        <input class="form-control" type="text" name="email" data-validetta="required,email"/>
                                    </div>
                                    {{-- <div class="form-group col-xs-6">
                                        <label for="check">Phone</label>
                                        <input class="form-control" type="text" name="phone" data-validetta="number"/>
                                    </div> --}}
                                    <div class="form-group col-xs-12">
                                        <textarea class="form-control" name="message" id="" cols="20" rows="5"></textarea>
                                    </div>
                                    <div class="form-group col-lg-3 col-xs-6">
                                        <button type="submit" class="btn btn-primary form-control">Send</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-lg-1">&nbsp;</div>
                <div class="col-lg-6">
                    <h3 class="text-uppercase">{!! $page->title !!}</h3>
                        {!! $page->content !!}
                </div>

            </div>
        </div>
    </div>

    
@stop


