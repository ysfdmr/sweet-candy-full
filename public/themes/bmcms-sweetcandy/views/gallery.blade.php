<?php $theme=Helpers::getSettings('theme'); ?>
@extends("$theme::layout.master")
@section('body.class') page-template @stop

@section('page.style')
@stop
@section('page.script')
    <script>
        jQuery(document).ready(function ($) {
             $('.owl-carousel').owlCarousel({
                loop:true,
                margin:10,
                responsive:{
                    0:{
                        items:1
                    },
                    600:{
                        items:3
                    },
                    1000:{
                        items:5
                    }
                }                    
            });
            
            $(".modals").attr("rel", "gallery").fancybox({
                afterLoad: function () {
                    this.title = this.title ?
                    '<a class="download" download="' + this.href +
                    '" href="' + this.href +
                    '">Download</a> ' + this.title :
                    '<a class="download" download="' + this.href +
                    '" href="' + this.href +
                    '">Download</a>';
                },
                helpers: {
                    title: {
                        type: 'inside'
                    }
                }
            });
        });
    </script>
@stop

@section('page.body')
    <div id="hero-header" style="background-image: url({{ Helpers::getImage($page->Cover) }});">
        <div class="caption">
            <div class="title">{{ $page->title }}</div>
        </div>
    </div>

    <div id="contentContainer" class="carousel-fluid">
        <div id="content" class="container">
            <div class="row arrowtest">
                <div class="description col-md-12 col-lg-12 xscroll">

                        @if(!empty($gallery))
                            @foreach($gallery->Items as $slide)
                                <div class="col-lg-4 col-md-4 col-sm-4 col-xs-6 " style="margin-bottom:20px;">
                                    <a href="{{ Helpers::getImage($slide->Image) }}" data-fancybox="group" data-caption="Caption #1">
                                        <img src="{{ Helpers::getImage($slide->Image,370,275) }}" alt="{{$slide->title}}" class="img-full" />
                                    </a>
                                </div>
                            @endforeach
                        @endif



                </div>
            </div>
        </div>
    </div>

@stop


