<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{!! csrf_token() !!}" />
    <meta name="description" content="@if(!empty($page->meta_description)) {{ $page->meta_description }} @else {{ Helpers::getSettingTrans('site_description') }} @endif"/>
    <link rel="canonical" href="{{ "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]" }}"/>
    <title>@if($page) {{ $page->title }} | {{ Helpers::getSettingTrans('site_title') }} @else {{ Helpers::getSettingTrans('site_title') }} @endif </title>
    
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link href="{{ Theme::asset('build/css/vendor.css') }}" rel="stylesheet">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
    @yield('page.style')
    {!! Helpers::getSettings('custom_css') !!}
</head>
<body class="@yield('body.class')">
