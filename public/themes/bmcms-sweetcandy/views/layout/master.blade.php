@include("$theme::layout.head")
@include("$theme::layout.header")
    @yield('page.body')
@include("$theme::layout.footer")