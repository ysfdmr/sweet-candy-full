<nav id="navbar-top" class="navbar navbar-default navbar-static-top">
    <div class="container">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="/">
        <img src="{{ Helpers::getSettings('media_logo') }}" alt="logo">
        </a>
    </div>
    <div id="navbar" class="navbar-collapse collapse">
        <ul class="nav navbar-nav navbar-right">
        @if($menu=Helpers::getMenuBySettingsKey($theme."_menu")->Items)
            {!! Helpers::recursive_menu($menu,[
            'sub-menu-list'=>'',
            'sub-menu-id'=>'',
            'sub-menu-class'=>'sub-menu',
            'link-class'=>'',
            'li-class'=>'navitem menu-item',
            'image'=>false,
            ]) !!}
        @else
            <li>Please Select Menu</li>
        @endif
        @foreach($APP['LOCALE'] as $key => $locale)
            @if(App::getLocale() != $key)
                <li>
                    <a class="lsw text-uppercase" href="/switch-language/{{$key}}">
                        {{$key}} 
                    </a>
                </li>
            @endif
        @endforeach
        </ul>
    </div><!--/.nav-collapse -->
    </div>
</nav>                            
                        
                    


