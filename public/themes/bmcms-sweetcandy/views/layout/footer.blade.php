    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-6 col-lg-4 footerleft ">
                    <div class="logofooter"> 
                        <img src="{{ Helpers::getSettings('media_logo') }}" class="img-responsive" alt="">
                    </div>
                    <p>{!! Helpers::getSettings('company_info') !!}</p>
                    <p><i class="fa fa-map-pin"></i> {!! strip_tags(Helpers::getSettings('company_address')) !!}</p>
                    <p><i class="fa fa-phone"></i> Phone : {{ Helpers::getSettings('company_phone') }}</p>
                    <p><i class="fa fa-envelope"></i> E-mail : {{ Helpers::getSettings('company_email') }}</p>
                    
                </div>
                <div class="col-md-3 col-sm-6 col-lg-4 paddingtop-bottom text-center">
                    <h6 class="logofooter">{{ Helpers::getSettings($theme.'_footer2') }}</h6>
                    <div class="post">
                        {!! Helpers::getSettings($theme.'_footer2_content') !!}
                    </div>
                </div>
                <div class="col-md-2 col-sm-6 col-lg-4 paddingtop-bottom text-center">
                    <h6 class="logofooter">Menu</h6>
                    <ul class="footer-ul">
                    @if($menu=Helpers::getMenuBySettingsKey($theme."_menu")->Items)
                        {!! Helpers::recursive_menu($menu,[
                        'sub-menu-list'=>'',
                        'sub-menu-id'=>'',
                        'sub-menu-class'=>'sub-menu',
                        'link-class'=>'',
                        'li-class'=>'navitem menu-item',
                        'image'=>false,
                        ]) !!}
                    @else
                        <li>Please Select Menu</li>
                    @endif
                    </ul>
                </div>
                
                
            </div>
        </div>
    </footer>

    
    <script>            
        var ContactMap={
            long    : {{ Helpers::getSettings('company_long') }},
            lat     : {{ Helpers::getSettings('company_lat') }},
            name    : '{{ Helpers::getSettings('company_name','Please Set Campany Name') }}',
            name2   : '{{ Helpers::getSettings('company_name','Please Set Campany Name') }}',
        };
        var theme='{{$theme}}';
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBGWMg9-5Vss0jfxdSrEWEvVBGSu6Ax2nE"></script>
    <script src="/assets/global/plugins/gmap/gmaps.js"></script>
    <script src="{{ Theme::asset('build/js/vendors.js') }}"></script>

    @yield('page.script')
    {!! Helpers::getSettings('custom_js') !!}
    
    </body>

</html>
