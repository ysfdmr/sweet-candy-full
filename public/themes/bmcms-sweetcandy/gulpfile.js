var elixir = require('laravel-elixir');
// require('laravel-elixir-vueify');

elixir.config.assetsPath = "./assets/";
elixir.config.publicPath = "./assets/";
elixir.config.viewPath = "./views";
elixir(function(mix) {
    mix.sass('app.scss', './assets/tmp/css/customize.css');
    mix.styles([
        './bower_components/bootstrap/dist/css/bootstrap.min.css',
        // './bower_components/bootstrap-select/dist/css/bootstrap-select.min.css',
        './bower_components/fancybox/dist/jquery.fancybox.min.css',
        './bower_components/rev-slider/css/settings.css',
        './bower_components/rev-slider/css/layers.css',
        './bower_components/rev-slider/css/navigation.css',
        './bower_components/font-awesome/css/font-awesome.css',
        './bower_components/ihover/dist/styles/main.css',
        // './bower_components/ionicons/css/ionicons.min.css',
        // './bower_components/owl.carousel/dist/assets/owl.carousel.min.css',
        // './bower_components/jquery-date-range-picker/dist/daterangepicker.min.css',

        // './assets/css/**/*.css',
        './assets/tmp/css/*.css',
    ], './assets/build/css/vendor.css');

    mix.scripts([
        './bower_components/jquery/dist/jquery.min.js',
        // './bower_components/moment/min/moment.min.js',
        // './bower_components/device.js/device.js',
        './bower_components/bootstrap/dist/js/bootstrap.min.js',
        // './bower_components/bootstrap-select/dist/js/bootstrap-select.min.js',
        './bower_components/fancybox/dist/jquery.fancybox.min.js',
        './bower_components/rev-slider/js/jquery.themepunch.tools.min.js',
        './bower_components/rev-slider/js/jquery.themepunch.revolution.min.js',
        // './bower_components/owl.carousel/dist/owl.carousel.min.js',
        // './bower_components/jquery-slimscroll/jquery.slimscroll.js',
        // './bower_components/jquery-date-range-picker/dist/jquery.daterangepicker.min.js',

        './assets/js/main.js',
    ], './assets/build/js/vendors.js');

    // mix.scripts([
    //     './assets/js/vue.main.js'
    // ], './assets/build/js/app.js');

    mix.copy('./bower_components/font-awesome/fonts/', './assets/build/fonts/');
    mix.copy('./bower_components/ionicons/fonts/', './assets/build/fonts/');
    mix.copy('./bower_components/rev-slider/fonts/*', './assets/build/fonts/');
    mix.copy('./bower_components/rev-slider/js/extensions/*.js', './assets/build/extensions/');
    // mix.browserify('./assets/js/vue.main.js', './assets/build/js/app.js');
    mix.browserSync({
        proxy: 'localhost',
        notify: false,
        open: false
    });
});