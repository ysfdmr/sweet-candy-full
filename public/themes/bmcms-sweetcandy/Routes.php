<?php
/*INDEX PAGE*/
Route::get('/', 'ThemeController@getIndex');
/*Gallery PAGE*/
Route::get('/gallery', 'ThemeController@getGallery');
/*CONTACT PAGE*/
Route::get('/contact', 'ThemeController@getContact');

/*products PAGE*/
Route::get('/products', 'ThemeController@getProducts');
/*products PAGE*/
Route::get('/products/{slug}', 'ThemeController@getProductDetails');
/*products category PAGE*/
Route::get('/products/category/{slug}', 'ThemeController@getCategory');


Route::get('/page/{slug}', 'ThemeController@getPage');

Route::post('/email/send', 'ThemeController@postEmailSend');


