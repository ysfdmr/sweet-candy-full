console.log("BMCMS");

var tpj = jQuery;
var revapi6;
tpj(document).ready(function() {
    if (tpj("#topslider").revolution == undefined) {
        revslider_showDoubleJqueryError("#topslider");
    } else {
        revapi6 = tpj("#topslider").show().revolution({
            // sliderType: "standard",
            jsFileLocation: "/themes/" + theme + "/assets/build/",
            // sliderLayout: "auto",
            dottedOverlay: "none",
            delay: 5000,
            navigation: {
                keyboardNavigation: "off",
                keyboard_direction: "horizontal",
                mouseScrollNavigation: "off",
                mouseScrollReverse: "default",
                onHoverStop: "on",
                arrows: {
                    style: "uranus",
                    enable: true,
                    hide_onmobile: false,
                    hide_onleave: false,
                    tmp: '',
                    left: {
                        h_align: "left",
                        v_align: "center",
                        h_offset: 30,
                        v_offset: 0
                    },
                    right: {
                        h_align: "right",
                        v_align: "center",
                        h_offset: 30,
                        v_offset: 0
                    }
                }
            }
        });
    }
}); /*ready*/