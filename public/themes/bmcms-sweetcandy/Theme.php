<?php
namespace Theme\Methods;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use \Illuminate\Support\Facades\Validator;
use Modules\Blog\Entities\Category;
use Modules\Blog\Entities\Post;
use Modules\Location\Entities\Location;
use Modules\Module\Entities\CustomModule;
use Modules\Page\Entities\Page;
use Modules\Mediazer\Entities\Gallery;
use Modules\Product\Entities\Product;
use Modules\Product\Entities\ProductBrand;
use Modules\Product\Entities\ProductCategory;

trait Actions
{
    /**
     * Home Page
     */
    protected $cacheTime = 60;

    public function getTheme()
    {
        return \Helpers::getSettings('theme');
    }

    public function getIndex()
    {
        //return  Cache::remember('home.'.App::getLocale(), $this->cacheTime, function() {
        $page = \Helpers::getHomePage();
        $products=Product::filter()->get();
        $categories=ProductCategory::all()->toHierarchy();
        $sweetmoments=Gallery::with('Items.Image')->where('id',\Helpers::getSettings($this->getTheme().'_sweet_moments'))->first();
        return view($this->getTheme() . '::index', compact('page', 'products','categories','sweetmoments'))->render();
        //});
    }



    public function getProducts()
    {
        $page = \Helpers::getHomePage();
        $products=Product::with('Cover','gallery')->get();
        $categories=ProductCategory::all()->toHierarchy();
        // dd($categories);
        $brands=ProductBrand::all();
        return view($this->getTheme()."::products.all", compact('page','products','categories','brands'))->render();
    }

    public function getProductDetails($slug)
    {
        $product=Product::findBySlugOrId($slug);
        return view($this->getTheme()."::products.list", compact('product'))->render();
    }




    /**
     * Category Functions
     */
    public $categories=[];
    public function getSubCategories($node,$parent=true,$parent_id=null,$self_id=null) {
        if($parent==true){
          $this->categories;
        }
        //echo $node->id;
        array_push($this->categories,$node->id);
        if ( $node->children()->count() > 0 ) {
            foreach($node->children as $child){
                $this->getSubCategories($child,false,$node->id,$self_id);
            }
        }
        return $this->categories;
    }


    /**
     * Get Category Items
     */
    public function getCategory($slug)
    {
        $page = \Helpers::getHomePage();
        $categories=ProductCategory::all()->toHierarchy();
        $brands=ProductBrand::all();
        $category=ProductCategory::findBySlugOrIdOrFail($slug);//get Root Category
        // $products=Product::with('Cover')->get();
        // if($category){
        //     $subcategories=$this->getSubCategories($category);
        // }
        $product=Product::with('Cover','Gallery')->where('category_id',$category->id)->first();
        $gallery=$product->Gallery;
        // dd($gallery);
        return view($this->getTheme().'::products.list',compact('category','categories','brands','page','subcategories','gallery'))->render();
    }




    /**
     * Pages
     */
    public function getPage($slug)
    {
        $post = Page::findBySlugOrId($slug);
        $gallery=Gallery::with('Items.Image')->where('id',$post->slider)->first();
        return view($this->getTheme() . '::page', compact('post','gallery'))->render();
    }

    /**
     * Gallery Page
     */
    public function getGallery()
    {
        //return  Cache::remember('gallery.'.App::getLocale(), $this->cacheTime, function() {
        $page = \Helpers::getPageByID(\Helpers::getSettingTrans($this->getTheme() . '_gallery_page'));
        $id=\Helpers::getSettings($this->getTheme().'_gallery');
        $gallery=Gallery::with('Items.Image')->where('id',$id)->first();
        // dd($gallery);
        return view($this->getTheme() . '::gallery', compact('page','gallery'))->render();
        //});
    }

    /**
     * Contact Page
     */
    public function getContact()
    {
        //return  Cache::remember('contact.'.App::getLocale(), $this->cacheTime, function() {
        $page = \Helpers::getPageByID(\Helpers::getSettingTrans($this->getTheme() . '_contact_page'));
        //$contactLocales=CustomModule::with('Gallery.Image')->where('module','contactLocales')->get();
        return view($this->getTheme() . '::contact', compact('page', 'contactLocales'))->render();
        //});
    }
    

    /**
     * Locations Page
     */
    public function getLocations()
    {
        $page = \Helpers::getPageByID(\Helpers::getSettingTrans($this->getTheme() . '_locations_page'));
        $locations = Location::where('status', true)->get();
        return view($this->getTheme() . '::location', compact('locations', 'page'))->render();
    }

   


    public function postEmailSend()
    {
        $inputs = Input::all();
        $send = Mail::send(array('html' => 'email.template-1'), array('inputs' => $inputs), function ($message) use ($inputs) {
            $message->from(\Helpers::getSettings('company_email'), 'Restaurant Reservation');
            $message->to(\Helpers::getSettings('company_email'), \Helpers::getSettings('company_name'))->subject('Enquiry details');
        });
        if ($send) {
            return response()->Json([
                'message' => 'Success sending. Thank You!'
            ], 200);
        } else {
            return response()->Json([
                'message' => 'Error: Please Refresh Page & Try Again!'
            ], 422);
        }
    }

}
