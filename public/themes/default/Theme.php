<?php
namespace Theme\Methods;

use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use \Illuminate\Support\Facades\Validator;
use Modules\Blog\Entities\Category;
use Modules\Blog\Entities\Post;
use Modules\Location\Entities\Location;
use Modules\Module\Entities\CustomModule;
use Modules\Page\Entities\Page;
use Modules\Mediazer\Entities\Gallery;


trait Actions
{
    /**
     * Home Page
     */
    protected $cacheTime = 60;

    public function getTheme()
    {
        return \Helpers::getSettings('theme');
    }

    public function getIndex()
    {
        //return  Cache::remember('home.'.App::getLocale(), $this->cacheTime, function() {
        $page = \Helpers::getHomePage();
        return view($this->getTheme() . '::index', compact('page'))->render();
        //});
    }

    


    public function postEmailSend()
    {
        $inputs = Input::all();
        $send = Mail::send(array('html' => 'email.template-1'), array('inputs' => $inputs), function ($message) use ($inputs) {
            $message->from(\Helpers::getSettings('company_email'), 'Restaurant Reservation');
            $message->to(\Helpers::getSettings('company_email'), \Helpers::getSettings('company_name'))->subject('Enquiry details');
        });
        if ($send) {
            return response()->Json([
                'message' => 'Success sending. Thank You!'
            ], 200);
        } else {
            return response()->Json([
                'message' => 'Error: Please Refresh Page & Try Again!'
            ], 422);
        }
    }

}
