"use strict";
var token='&_token='+$("meta[name=\"csrf-token\"]").attr('content');
var urlPrefix="/admin/";
var body=$('body');

/*Nestable*/
$('#nestableMenu').nestable();
/*
body.on('click', '#save-menu', function () {
    var serialized=$('#nestableMenu').nestable('serialize')
    //console.log(serialized);
    //console.log(window.JSON.stringify(serialized));
});
*/

/*Events*/

body.on('click', '#delete-menu', function () {
    var question = confirm("Are you sure you want to delete this and the sub menus?");
    if (question == true) {
        var itemID = $(this).data('id');
        var url = urlPrefix+ 'menu/delete-menu';
        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'JSON',
            data: '&item=' +itemID+token
        }).done( function( data ){
            toastr.success(data.message, "Menu Notifications");
            location.reload();
        }).fail( function( jqXHR, textStatus){
            if(jqXHR.status==422){
                toastr.error(jqXHR.responseText,'Validation Error');
            }else{
                toastr.error(textStatus+':'+jqXHR.status+' : '+jqXHR.statusText,'Error');
                console.log(textStatus+':'+jqXHR.status+' : '+jqXHR.statusText);
            }
        });
    }
});

body.on('click', '.dd-action.action-delete', function () {
    var question = confirm("Are you sure you want to delete this menu link ?");
    if (question == true) {
        var itemID = $(this).data('id');
        var url = urlPrefix+ 'menu/delete-item';
        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'JSON',
            data: '&item=' +itemID+token
        }).done( function( data ){
            $('li[data-id="'+itemID+'"]').remove();
            toastr.success(data.message, "Menu Notifications");
        }).fail( function( jqXHR, textStatus){
            if(jqXHR.status==422){
                toastr.error(jqXHR.responseText,'Validation Error');
            }else{
                toastr.error(textStatus+':'+jqXHR.status+' : '+jqXHR.statusText,'Error');
                console.log(textStatus+':'+jqXHR.status+' : '+jqXHR.statusText);
            }
        });
    }
});


body.on('change', 'select[name="menu_list"]', function () {
    //window.location.href = urlPrefix+"menu/edit/"+$(this).val();
    var menu_id=$(this).val()
    var url = window.location.origin+window.location.pathname;
    if (url.indexOf('?') > -1){
        url += '&edit='+menu_id;
    }else{
        url += '?edit='+menu_id
    }
    window.location.href = url;
});

/*EDIT MENU*/
$('#edit-menu').validetta({
    realTime : true,
    display : 'inline', // bubble or inline
    errorTemplateClass : 'validetta-inline',
    onValid : function( event ) {
        event.preventDefault();
        var input = $("<input>")
            .attr("type", "hidden")
            .attr("name", "serialize").val(JSON.stringify($('#nestableMenu').nestable('serialize')));
        $(this.form).append($(input));
        $.ajax({
            url : urlPrefix + 'menu/save-menu',
            data : $(this.form).serialize()+token,
            type:'post',
            beforeSend : function() {
                $.bmmenu.disableSubmit(event.target,true);
            }
        }).done( function( data ){
            toastr.success(data.message,'Success');
        }).fail( function( jqXHR, textStatus){
            if(jqXHR.status==422){
                toastr.error(jqXHR.responseText,'Validation Error');
            }else{
                console.log(textStatus+':'+jqXHR.status+' : '+jqXHR.statusText);
            }
        }).always( function( result ){
            $.bmmenu.disableSubmit(event.target,false);
        });
    }
});


/*CREATE NEW MENU*/
$('.new-link').validetta({
    realTime : true,
    display : 'inline', // bubble or inline
    errorTemplateClass : 'validetta-inline',
    validators: {
        regExp: {
            url : {
                pattern : /^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/,
                errorMessage : 'Please Check Url Format !'
            }
        }
    },
    onValid : function( event ) {
        event.preventDefault();
        $.ajax({
            url : urlPrefix + 'menu/create-link',
            data : $(this.form).serialize()+token,
            type:'post',beforeSend : function() {
                $.bmmenu.disableSubmit(event.target,true);
            }
        }).done( function( data ){
            toastr.success(data.message,'Success');
            var newItem =   '<li data-id="'+data.item_id+'" class="dd-item dd3-item">' +
                '<div class="dd-handle dd3-handle">Drag</div>'+
                '<a class="dd-action action-delete" data-id="'+data.item_id+'"> Delete </a>' +
                '<div class="dd3-content">'+data.item_name+'</div>' +
                '</li>';
            $("#nestableMenu > ol").append(newItem);
            $.bmmenu.resetForm(event.target);
        }).fail( function( jqXHR, textStatus){
            if(jqXHR.status==422){
                toastr.error(jqXHR.responseText,'Validation Error');
            }else{
                console.log(textStatus+':'+jqXHR.status+' : '+jqXHR.statusText);
            }
        }).always( function( result ){
            $.bmmenu.disableSubmit(event.target,false);
        });
    }
});

/*CREATE NEW MENU*/
$('#CreateMenuForm').validetta({
    realTime : true,
    display : 'inline', // bubble or inline
    errorTemplateClass : 'validetta-inline',
    onValid : function( event ) {
        event.preventDefault();
        $.ajax({
            url : urlPrefix + 'menu/create-menu',
            data : $(this.form).serialize()+token,
            type:'post',beforeSend : function() {
                $.bmmenu.disableSubmit(event.target,true);
            }
        }).done( function( data ){
                toastr.success(data.message,'Success');
                $.bmmenu.resetForm(event.target);
                location.reload();
            }).fail( function( jqXHR, textStatus){
                if(jqXHR.status==422){
                    toastr.error(jqXHR.responseText,'Validation Error');
                }else{
                    console.log(textStatus+':'+jqXHR.status+' : '+jqXHR.statusText);
                }
            }).always( function( result ){
                $.bmmenu.disableSubmit(event.target,false);
            });
    }
});


/*Controls*/
(function( $, window, undefined ) {
    $.bmmenu = $.extend( {}, {

        onChangeMenu:function(menu){
            console.log('Menu Changed');
            console.log(menu.val());

        },

        resetForm:function(formID){
            $(':input',formID)
                .not(':button, :submit, :reset, :hidden')
                .val('')
                .removeAttr('checked')
                .removeAttr('selected');
        },

        disableSubmit:function(form,action){
            $(form).find(':submit').prop('disabled', action);
        }

    }, $.bmmenu);
})(jQuery);