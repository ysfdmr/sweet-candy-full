"use strict";
var token='_token='+$("meta[name=\"csrf-token\"]").attr('content');
var urlPrefix="/admin/";
var body=$('body');
var itemList=$( "#MZ-gallery").find(".items" );
var moduleName=$('input[name="module"]').val();
var modulePostID=$('input[name="post"]').val();
itemList.sortable({
    placeholder: "ui-gallery-highlight",
    containment: "#MZ-gallery",
    stop: function( event, ui ) {
        var a= itemList.sortable( "serialize", { key: "sort" } );
        var sortedIDs = itemList.sortable( "toArray" );
        console.log(a);
        console.log(sortedIDs);
    }
});
$( "#sortable" ).disableSelection();

body.on('click', '#MZ-gallery i.fa.fa-edit', function () {
    console.log('clicked');
    $.gallery.toggleRightColum('show');
});

body.on('click', '#MZ-gallery .gallery-edit-close', function () {
    console.log('clicked');
    $.gallery.toggleRightColum('hide');
});

body.on('click', '#MZ-gallery i.fa.fa-edit', function () {
    var editBox=$('.gallery-edit-box');
    editBox.html(null);
    $.gallery.toggleRightColum('show');
    var formData="&mediaID="+$(this).data('id');
    $.ajax({
        url: urlPrefix+'mediazer/edit',
        type: 'get',
        data: token+formData,
        success: function(data) {
            editBox.html(data);
        },
        error:function(){
            toastr.error('Connection error !');
            editBox.html(null);
        }
    });

});

body.on('click', '#MZ-gallery #MZ-btn-update', function () {
    $.gallery.updateMedia($(this).data('id'));
    return false;
});

body.on('click', '.gallery-add-items', function () {
    $.gallery.addItems();
});

body.on('click', '#MZ-gallery i.fa.fa-close', function () {
    console.log('deleted');
    if(confirm('Delete This Media ?')){
        $.gallery.deleteMedia($(this).data('id'));
    }
});

$('.module-form').submit(function(){
    $.gallery.saveGallery($(this));
});

(function($) {
    $.gallery = $.extend( {}, {
        saveGallery:function(form){
            var mediaSerialized=itemList.sortable( "toArray" );
            $('<input>').attr({
                type: 'hidden',
                id: 'gallery',
                name: 'gallery',
                value:mediaSerialized
            }).appendTo(form);
        },

        deleteMedia:function(itemId){
            if($('.module-form').attr('id')=='module-form-create'){
                $( "#MZ-gallery").find('.item[id="'+itemId+'"]').remove();
            }else{
                $.ajax({
                    url: urlPrefix+'modules/'+moduleName+'/gallery-delete/'+modulePostID,
                    type: 'post',
                    data: token+'&mediaID='+itemId,
                    success: function(data) {
                        if(data.status=='success'){
                            toastr.success('Success Delete Media');
                            $( "#MZ-gallery").find('.item[id="'+itemId+'"]').remove();
                        }
                    },
                    error:function(){
                        toastr.error('Connection Error!');
                    }
                });
            }

        },
        addItems:function(){
            var getItems=$('input[name="gallery"]').val();
            if(getItems==""){
                toastr.info('Please use mediazer and select photos!');
            }else{
                var items=getItems.split(",");
                var currentItems=[];
                $( "#MZ-gallery .item" ).each(function() {
                    currentItems.push($(this).attr('id'));
                });
                $.each(items, function(index,item) {
                    if ($.inArray(item, currentItems) !== -1){
                        toastr.info('This photo has already!');
                        $('input[name="gallery"]').val(null);
                        $('.MZ-medias.medias-gallery').html('Please Select Medias');
                    }else{
                        $.get( urlPrefix+'mediazer/item', {
                            _token: $("meta[name=\"csrf-token\"]").attr('content'),
                            mediaID: item
                        }).done(function( data ) {
                            console.log(data);
                            itemList.append($.gallery.itemTemplate(data));
                            $('input[name="gallery"]').val(null);
                            $('.MZ-medias.medias-gallery').html('Please Select Medias');
                        });
                    }

                });
            }
        },

        itemTemplate:function(data){
            console.log(data);
            var imgSrc ='/uploads'+data.path + data.file_name;
            var imgLink ='/photo/250/250/'+imgSrc;
            var imgID   = data.id;
            return '<div class="item col-xs-4 col-sm-3 col-md-3 col-lg-2" id="'+imgID+'">' +
                      '<img class="img-rounded img-responsive img-bordered-sm" src="'+imgLink+'" />' +
                      '<i class="fa fa-close" data-id="'+imgID+'"></i>' +
                      '<i class="fa fa-edit" data-id="'+imgID+'"></i>' +
                  '</div>';
        },

        toggleRightColum:function(status){
            var body=$( "#MZ-gallery").find(".items" );
            var right=$( "#MZ-gallery").find(".item-edit" );
            console.log('trigged')
            if(status=="show"){
                right.removeClass('close').show(0);
                body.removeClass('col-md-12','col-lg-12').addClass('col-lg-9','col-sm-9');
            }else if(status=="hide"){
                right.addClass('close').hide(0);
                $('.MZ-edit-box').html(null);
                body.removeClass('col-lg-9','col-sm-9').addClass('col-lg-12','col-sm-12');
            }
        },

        updateMedia:function(id){
            var alt         =$('.gallery-edit-box input[name="alt"]').val();
            var title       =$('.gallery-edit-box input[name="title"]').val();
            var description =$('.gallery-edit-box textarea[name="description"]').val();
            var form_data="&id="+id+"&alt="+alt+"&title="+title+"&description="+description;
            $.ajax({
                url: urlPrefix+'mediazer/edit',
                type: 'post',
                data: token+form_data,
                success: function(data) {
                    toastr.success('Success Edit Media');
                },
                error:function(){
                    toastr.error('Connection Error!');
                }
            });
        }

    }, $.gallery);
})(jQuery, this);