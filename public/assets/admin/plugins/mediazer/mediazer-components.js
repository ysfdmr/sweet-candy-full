/*
 * jQueryFileTree Plugin
 *
 * @author - Cory S.N. LaViska - A Beautiful Site (http://abeautifulsite.net/) - 24 March 2008
 * @author - Dave Rogers - https://github.com/daverogers/jQueryFileTree
 *
 * Usage: $('.fileTreeDemo').fileTree({ options }, callback )
 *
 * TERMS OF USE
 *
 * This plugin is dual-licensed under the GNU General Public License and the MIT License and
 * is copyright 2008 A Beautiful Site, LLC.
 */
(function($, window) {
    var FileTree;
    FileTree = (function() {
        function FileTree(el, args, callback) {
            var $el, defaults;
            $el = $(el);
            defaults = {
                root: '/',
                script: '/files/filetree',
                folderEvent: 'click',
                expandSpeed: 500,
                collapseSpeed: 500,
                expandEasing: 'swing',
                collapseEasing: 'swing',
                multiFolder: true,
                loadMessage: 'Loading...',
                errorMessage: 'Unable to get file tree information',
                multiSelect: false,
                onlyFolders: false,
                onlyFiles: false
            };
            this.jqft = {
                container: $el
            };
            this.options = $.extend(defaults, args);
            this.callback = callback;
            $el.html('<ul class="jqueryFileTree start"><li class="wait">' + this.options.loadMessage + '<li></ul>');
            this.showTree($el, escape(this.options.root));
        }

        FileTree.prototype.showTree = function(el, dir) {
            var $el, _this, data, handleFail, handleResult, options, result;
            $el = $(el);
            options = this.options;
            _this = this;
            $el.addClass('wait');
            $(".jqueryFileTree.start").remove();
            data = {
                _token : $('meta[name="csrf-token"]').attr('content'),
                dir: dir,
                onlyFolders: options.onlyFolders,
                onlyFiles: options.onlyFiles,
                multiSelect: options.multiSelect
            };
            handleResult = function(result) {
                var li;
                $el.find('.start').html('');
                $el.removeClass('wait').append(result);
                if (options.root === dir) {
                    $el.find('UL:hidden').show(typeof callback !== "undefined" && callback !== null);
                } else {
                    if (jQuery.easing[options.expandEasing] === void 0) {
                        console.log('Easing library not loaded. Include jQueryUI or 3rd party lib.');
                        options.expandEasing = 'swing';
                    }
                    $el.find('UL:hidden').slideDown({
                        duration: options.expandSpeed,
                        easing: options.expandEasing
                    });
                }
                li = $('[rel="' + decodeURIComponent(dir) + '"]').parent();
                if (options.multiSelect && li.children('input').is(':checked')) {
                    li.find('ul li input').each(function() {
                        $(this).prop('checked', true);
                        return $(this).parent().addClass('selected');
                    });
                }
                return _this.bindTree($el);
            };
            handleFail = function() {
                $el.find('.start').html('');
                $el.removeClass('wait').append("<p>" + options.errorMessage + "</p>");
                return false;
            };
            if (typeof options.script === 'function') {
                result = options.script(data);
                if (typeof result === 'string' || result instanceof jQuery) {
                    return handleResult(result);
                } else {
                    return handleFail();
                }
            } else {
                return $.ajax({
                    url: options.script,
                    type: 'POST',
                    dataType: 'HTML',
                    data: data
                }).done(function(result) {
                    return handleResult(result);
                }).fail(function() {
                    return handleFail();
                });
            }
        };

        FileTree.prototype.bindTree = function(el) {
            var $el, _this, callback, jqft, options, relPattern;
            $el = $(el);
            options = this.options;
            jqft = this.jqft;
            _this = this;
            callback = this.callback;
            relPattern = /^\/.*\/$/i;
            $el.find('LI A').on(options.folderEvent, function() {
                var data, ref;
                data = {};
                data.li = $(this).closest('li');
                data.type = (ref = data.li.hasClass('directory')) != null ? ref : {
                    'directory': 'file'
                };
                data.value = $(this).text();
                data.rel = $(this).prop('rel');
                data.container = jqft.container;
                if ($(this).parent().hasClass('directory')) {
                    if ($(this).parent().hasClass('collapsed')) {
                        _this._trigger($(this), 'filetreeexpand', data);
                        if (!options.multiFolder) {
                            $(this).parent().parent().find('UL').slideUp({
                                duration: options.collapseSpeed,
                                easing: options.collapseEasing
                            });
                            $(this).parent().parent().find('LI.directory').removeClass('expanded').addClass('collapsed');
                        }
                        $(this).parent().removeClass('collapsed').addClass('expanded');
                        $(this).parent().find('UL').remove();
                        _this.showTree($(this).parent(), $(this).attr('rel').match(relPattern)[0]);
                        _this._trigger($(this), 'filetreeexpanded', data);
                    } else {
                        _this._trigger($(this), 'filetreecollapse', data);
                        $(this).parent().find('UL').slideUp({
                            duration: options.collapseSpeed,
                            easing: options.collapseEasing
                        });
                        $(this).parent().removeClass('expanded').addClass('collapsed');
                        _this._trigger($(this), 'filetreecollapsed', data);
                    }
                } else {
                    if (!options.multiSelect) {
                        jqft.container.find('li').removeClass('selected');
                        $(this).parent().addClass('selected');
                    } else {
                        if ($(this).parent().find('input').is(':checked')) {
                            $(this).parent().find('input').prop('checked', false);
                            $(this).parent().removeClass('selected');
                        } else {
                            $(this).parent().find('input').prop('checked', true);
                            $(this).parent().addClass('selected');
                        }
                    }
                    _this._trigger($(this), 'filetreeclicked', data);
                    if (typeof callback === "function") {
                        callback($(this).attr('rel'));
                    }
                }
                return false;
            });
            if (options.folderEvent.toLowerCase !== 'click') {
                return $el.find('LI A').on('click', function() {
                    return false;
                });
            }
        };

        FileTree.prototype._trigger = function(el, eventType, data) {
            var $el;
            $el = $(el);
            return $el.trigger(eventType, data);
        };

        return FileTree;

    })();
    return $.fn.extend({
        fileTree: function(args, callback) {
            return this.each(function() {
                var $this, data;
                $this = $(this);
                data = $this.data('fileTree');
                if (!data) {
                    $this.data('fileTree', (data = new FileTree(this, args, callback)));
                }
                if (typeof args === 'string') {
                    return data[option].apply(data);
                }
            });
        }
    });
})(window.jQuery, window);

/*
 * dmuploader.js - Jquery File Uploader - 0.1
 * http://www.daniel.com.uy/projects/jquery-file-uploader/
 *
 * Copyright (c) 2013 Daniel Morales
 * Dual licensed under the MIT and GPL licenses.
 * http://www.daniel.com.uy/doc/license/
 */

(function($) {
    var pluginName = 'dmUploader';

    // These are the plugin defaults values
    var defaults = {
        url: document.URL,
        method: 'POST',
        extraData: {},
        maxFileSize: 0,
        maxFiles: 0,
        allowedTypes: '*',
        extFilter: null,
        dataType: null,
        fileName: 'file',
        onInit: function(){},
        onFallbackMode: function() {message},
        onNewFile: function(id, file){},
        onBeforeUpload: function(id){},
        onComplete: function(){},
        onUploadProgress: function(id, percent){},
        onUploadSuccess: function(id, data){},
        onUploadError: function(id, message){},
        onFileTypeError: function(file){},
        onFileSizeError: function(file){},
        onFileExtError: function(file){},
        onFilesMaxError: function(file){}
    };

    var DmUploader = function(element, options)
    {
        this.element = $(element);

        this.settings = $.extend({}, defaults, options);

        if(!this.checkBrowser()){
            return false;
        }

        this.init();

        return true;
    };

    DmUploader.prototype.checkBrowser = function()
    {
        if(window.FormData === undefined){
            this.settings.onFallbackMode.call(this.element, 'Browser doesn\'t support Form API');

            return false;
        }

        if(this.element.find('input[type=file]').length > 0){
            return true;
        }

        if (!this.checkEvent('drop', this.element) || !this.checkEvent('dragstart', this.element)){
            this.settings.onFallbackMode.call(this.element, 'Browser doesn\'t support Ajax Drag and Drop');

            return false;
        }

        return true;
    };

    DmUploader.prototype.checkEvent = function(eventName, element)
    {
        var element = element || document.createElement('div');
        var eventName = 'on' + eventName;

        var isSupported = eventName in element;

        if(!isSupported){
            if(!element.setAttribute){
                element = document.createElement('div');
            }
            if(element.setAttribute && element.removeAttribute){
                element.setAttribute(eventName, '');
                isSupported = typeof element[eventName] == 'function';

                if(typeof element[eventName] != 'undefined'){
                    element[eventName] = undefined;
                }
                element.removeAttribute(eventName);
            }
        }

        element = null;
        return isSupported;
    };

    DmUploader.prototype.init = function()
    {
        var widget = this;

        widget.queue = new Array();
        widget.queuePos = -1;
        widget.queueRunning = false;

        // -- Drag and drop event
        widget.element.on('drop', function (evt){
            evt.preventDefault();
            var files = evt.originalEvent.dataTransfer.files;

            widget.queueFiles(files);
        });

        //-- Optional File input to make a clickable area
        widget.element.find('input[type=file]').on('change', function(evt){
            var files = evt.target.files;

            widget.queueFiles(files);

            $(this).val('');
        });

        this.settings.onInit.call(this.element);
    };

    DmUploader.prototype.queueFiles = function(files)
    {
        var j = this.queue.length;

        for (var i= 0; i < files.length; i++)
        {
            var file = files[i];

            // Check file size
            if((this.settings.maxFileSize > 0) &&
                (file.size > this.settings.maxFileSize)){

                this.settings.onFileSizeError.call(this.element, file);

                continue;
            }

            // Check file type
            if((this.settings.allowedTypes != '*') &&
                !file.type.match(this.settings.allowedTypes)){

                this.settings.onFileTypeError.call(this.element, file);

                continue;
            }

            // Check file extension
            if(this.settings.extFilter != null){
                var extList = this.settings.extFilter.toLowerCase().split(';');

                var ext = file.name.toLowerCase().split('.').pop();

                if($.inArray(ext, extList) < 0){
                    this.settings.onFileExtError.call(this.element, file);

                    continue;
                }
            }

            // Check max files
            if(this.settings.maxFiles > 0) {
                if(this.queue.length >= this.settings.maxFiles) {
                    this.settings.onFilesMaxError.call(this.element, file);

                    continue;
                }
            }

            this.queue.push(file);

            var index = this.queue.length - 1;

            this.settings.onNewFile.call(this.element, index, file);
        }

        // Only start Queue if we haven't!
        if(this.queueRunning){
            return false;
        }

        // and only if new Failes were successfully added
        if(this.queue.length == j){
            return false;
        }

        this.processQueue();

        return true;
    };

    DmUploader.prototype.processQueue = function()
    {
        var widget = this;

        widget.queuePos++;

        if(widget.queuePos >= widget.queue.length){
            // Cleanup

            widget.settings.onComplete.call(widget.element);

            // Wait until new files are droped
            widget.queuePos = (widget.queue.length - 1);

            widget.queueRunning = false;

            return;
        }

        var file = widget.queue[widget.queuePos];

        // Form Data
        var fd = new FormData();
        fd.append(widget.settings.fileName, file);

        // Return from client function (default === undefined)
        var can_continue = widget.settings.onBeforeUpload.call(widget.element, widget.queuePos);

        // If the client function doesn't return FALSE then continue
        if( false === can_continue ) {
            return;
        }

        // Append extra Form Data
        $.each(widget.settings.extraData, function(exKey, exVal){
            fd.append(exKey, exVal);
        });

        widget.queueRunning = true;

        // Ajax Submit
        $.ajax({
            url: widget.settings.url,
            type: widget.settings.method,
            dataType: widget.settings.dataType,
            data: fd,
            cache: false,
            contentType: false,
            processData: false,
            forceSync: false,
            xhr: function(){
                var xhrobj = $.ajaxSettings.xhr();
                if(xhrobj.upload){
                    xhrobj.upload.addEventListener('progress', function(event) {
                        var percent = 0;
                        var position = event.loaded || event.position;
                        var total = event.total || e.totalSize;
                        if(event.lengthComputable){
                            percent = Math.ceil(position / total * 100);
                        }

                        widget.settings.onUploadProgress.call(widget.element, widget.queuePos, percent);
                    }, false);
                }

                return xhrobj;
            },
            success: function (data, message, xhr){
                widget.settings.onUploadSuccess.call(widget.element, widget.queuePos, data);
            },
            error: function (xhr, status, errMsg){
                widget.settings.onUploadError.call(widget.element, widget.queuePos, errMsg);
            },
            complete: function(xhr, textStatus){
                widget.processQueue();
            }
        });
    }

    $.fn.dmUploader = function(options){
        return this.each(function(){
            if(!$.data(this, pluginName)){
                $.data(this, pluginName, new DmUploader(this, options));
            }
        });
    };

    // -- Disable Document D&D events to prevent opening the file on browser when we drop them
    $(document).on('dragenter', function (e) { e.stopPropagation(); e.preventDefault(); });
    $(document).on('dragover', function (e) { e.stopPropagation(); e.preventDefault(); });
    $(document).on('drop', function (e) { e.stopPropagation(); e.preventDefault(); });
})(jQuery);
