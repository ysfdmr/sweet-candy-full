"use strict";
var token='_token='+$("meta[name=\"csrf-token\"]").attr('content');
var urlPrefix="/admin/";
var body=$('body');
var itemList=$( "#MZ-gallery").find(".items" );
itemList.sortable({
    placeholder: "ui-gallery-highlight",
    containment: "#MZ-gallery",
    stop: function( event, ui ) {
        var a= itemList.sortable( "serialize", { key: "sort" } );
        var sortedIDs = itemList.sortable( "toArray" );
    }
});
$( "#sortable" ).disableSelection();

body.on('click', '#MZ-gallery i.fa.fa-edit', function () {
    $.gallery.toggleRightColum('show');
});

body.on('click', '#MZ-gallery .gallery-edit-close', function () {
    $.gallery.toggleRightColum('hide');
});

body.on('click', '#MZ-gallery i.fa.fa-edit', function () {
    var editBox=$('.gallery-edit-box');
    editBox.html(null);
    $.gallery.toggleRightColum('show');
    var formData="&mediaID="+$(this).data('id');
    $.ajax({
        url: urlPrefix+'mediazer/edit',
        type: 'get',
        data: token+formData,
        success: function(data) {
            editBox.html(data);
        },
        error:function(){
            toastr.error('Connection error !');
            editBox.html(null);
        }
    });

});

body.on('click', '#MZ-gallery #MZ-btn-update', function () {
    $.gallery.updateMedia($(this).data('id'));
    return false;
});

body.on('click', '.gallery-add-items', function () {
    $.gallery.addItems();
});


$('#gallery-form').submit(function(){
    $.gallery.saveGallery();
    return false;
});
$('#gallery-form-update').submit(function(){
    $.gallery.updateGallery($(this).data('id'));
    return false;
});

body.on('click', '#MZ-gallery i.fa.fa-close', function () {
    if(confirm('Delete This Media ?')){
        $.gallery.deleteMedia($(this).data('id'),$('input[name="galleryID"]').val());
    }
});
(function($) {
    $.gallery = $.extend( {}, {
        saveGallery:function(){
            var formSerialized=$('#gallery-form').serialize();
            var mediaSerialized=itemList.sortable( "toArray" );
            var formData=formSerialized+'&medias='+mediaSerialized;
            $.ajax({
                url: urlPrefix+'gallery',
                type: 'post',
                data: formData,
                success: function(data) {
                    if(data.status=='success'){
                        toastr.success('Success Create Media');
                        window.location.href = "/admin/gallery/edit/"+data.return;
                    }else if(data.status=='error'){
                        $.each(data.errors, function(index,item) {
                            toastr.warning(item);
                        });
                        //toastr.error('Please Check Gallery form!');
                    }
                },
                error:function(){
                    toastr.error('Connection Error!');
                }
            });
        },
        updateGallery:function(id){
            var formSerialized=$('#gallery-form-update').serialize();
            var mediaSerialized=itemList.sortable( "toArray" );
            var formData=formSerialized+'&medias='+mediaSerialized;
            $.ajax({
                url: urlPrefix+'gallery/edit/'+id,
                type: 'post',
                data: formData,
                success: function(data) {
                    if(data.status=='success'){
                        toastr.success('Success Create Media');
                        //window.location.href = "/admin/gallery/edit/"+data.return;
                    }else if(data.status=='error'){
                        $.each(data.errors, function(index,item) {
                            toastr.warning(item);
                        });
                    }
                },
                error:function(){
                    toastr.error('Connection Error!');
                }
            });
        },
        deleteMedia:function(mediaID,galleryID){
            if(galleryID==0){
                $( "#MZ-gallery").find('.item[id="'+mediaID+'"]').remove();
            }else{
                $.ajax({
                    url: urlPrefix+'gallery/delete-media/'+mediaID+'/'+galleryID,
                    type: 'post',
                    data: token,
                    success: function(data) {
                        if(data.status=='success'){
                            toastr.success('Success Delete Media');
                            $( "#MZ-gallery").find('.item[id="'+mediaID+'"]').remove();
                        }
                    },
                    error:function(){
                        toastr.error('Connection Error!');
                    }
                });
            }
        },
        addItems:function(){
            var getItems=$('input[name="gallery"]').val();
            if(getItems==""){
                toastr.info('Please use mediazer and select photos!');
            }else{
                var items=getItems.split(",");
                var currentItems=[];
                $( "#MZ-gallery .item" ).each(function() {
                    currentItems.push($(this).attr('id'));
                });
                $.each(items, function(index,item) {
                    if ($.inArray(item, currentItems) !== -1){
                        toastr.info('This photo has already!');
                        $('input[name="gallery"]').val(null);
                        $('.MZ-medias.medias-gallery').html('Please Select Medias');
                    }else{
                        $.get( urlPrefix+'mediazer/item', {
                            _token: $("meta[name=\"csrf-token\"]").attr('content'),
                            mediaID: item
                        }).done(function( data ) {
                            itemList.append($.gallery.itemTemplate(data));
                            $('input[name="gallery"]').val(null);
                            $('.MZ-medias.medias-gallery').html('Please Select Medias');
                        });
                    }

                });
            }
        },

        itemTemplate:function(data){
            var imgSrc ='/uploads'+data.path + data.file_name;
            var imgLink ='/photo/250/250/'+imgSrc;
            var imgID   = data.id;
            return '<div class="item col-xs-4 col-sm-3 col-md-3 col-lg-2" id="'+imgID+'">' +
                      '<img class="img-rounded img-responsive img-bordered-sm" src="'+imgLink+'" />' +
                      '<i class="fa fa-close" data-id="'+imgID+'"></i>' +
                      '<i class="fa fa-edit" data-id="'+imgID+'"></i>' +
                  '</div>';
        },

        toggleRightColum:function(status){
            var body=$( "#MZ-gallery").find(".items" );
            var right=$( "#MZ-gallery").find(".item-edit" );
            if(status=="show"){
                right.removeClass('close').show(0);
                body.removeClass('col-md-12','col-lg-12').addClass('col-lg-9','col-sm-9');
            }else if(status=="hide"){
                right.addClass('close').hide(0);
                $('.MZ-edit-box').html(null);
                body.removeClass('col-lg-9','col-sm-9').addClass('col-lg-12','col-sm-12');
            }
        },

        updateMedia:function(id){
            var alt         =$('.gallery-edit-box input[name="alt"]').val();
            var title       =$('.gallery-edit-box input[name="title"]').val();
            var description =$('.gallery-edit-box textarea[name="description"]').val();
            var form_data="&id="+id+"&alt="+alt+"&title="+title+"&description="+description;
            $.ajax({
                url: urlPrefix+'mediazer/edit',
                type: 'post',
                data: token+form_data,
                success: function(data) {
                    toastr.success('Success Edit Media');
                },
                error:function(){
                    toastr.error('Connection Error!');
                }
            });
        }

    }, $.gallery);
})(jQuery, this);