var token='_token='+$("meta[name=\"csrf-token\"]").attr('content');
var urlPrefix="/admin/";
(function($) {
    "use strict";
    $.fn.Mediazer = function(param) {

        var options = $.extend({
            inputName:'image',
            select:'single',
            source:'id'
        }, param);

        var inputName=$(this).data('input')!=undefined?$(this).data('input'):options.inputName;
        var selectType=$(this).data('select')!=undefined?$(this).data('select'):options.select;
        var dataSource=$(this).data('source')!=undefined?$(this).data('source'):options.source;


        this.bind("click.Mediazer", function () {
            console.log(inputName + selectType + dataSource);
            getFiles($('.MZ-selected-folder').html());
            $('#mediazer').modal('show');
            $('.MZ-filetree').fileTree({
                script: urlPrefix+'mediazer/connector',
                multiFolder: false,
                onlyFolders: true,
                expandSpeed: 250,
                collapseSpeed: 250
            }).on('filetreeexpanded filetreecollapsed', function(e, data) {
                getFiles(data.rel);
                $('.MZ-info .MZ-selected-folder').html(data.rel);
            });
            setUploader('#drag-and-drop-zone');
        });

        // Click Root Folder
        $('.MZ-root').click(function () {
            $('.MZ-actions .MZ-selected-folder').html('/');
            getFiles('/');
        });

        $('.MZ-upload-switcher').click(function () {
            var uploadBox=$(".MZ-uploader");
            if(uploadBox.hasClass('close')){
                uploadBox.removeClass('close').show();
            }else{
                uploadBox.addClass('close').hide();
            }
        });

        /*
        * Folder CRUD
        * */
        $('.MZ-add-folder').click(function () {
            $('#mediazer-form').modal({
                show:true,
                remote: urlPrefix+'mediazer/forms/create-folder'
            });
        });

        $('.MZ-delete-folder').click(function () {
            if(confirm($('.MZ-selected-folder').html()+' Delete This Folder ?')){
                $.mediazerExtend.deleteFolder();
            }
        });

        $(document).delegate("#create-folder","submit",function(){
            $.mediazerExtend.createFolder();
            return false;
        });

        /*FUNCTIONS*/
        function getFiles(dir){
            var fileBox=$('.MZ-file-box');
            fileBox.html(null);
            var formData="&dir="+dir;
            $.ajax({
                url: urlPrefix+'mediazer/list',
                type: 'post',
                data: token+formData,
                success: function(data) {
                    fileBox.html(data);
                },
                error:function(){
                    toastr.error('Dir list connection error !');
                    $('.MZ-file-box').html(null);
                }
            });
        }

        /**
         *  File Select
         * */
        $('body').on('change', 'input.MZ-selector', function () {
            $.mediazerExtend.checkedMedia(this,selectType);
        });

        /**
         * Edit Media
         * */
        $('body').on('click', '.MZ-edit-btn', function () {
            var editBox=$('.MZ-edit-box');
            editBox.html(null);
            $.mediazerExtend.toggleRightColum('show');
            var formData="&mediaID="+$(this).data('id');
            $.ajax({
                url: urlPrefix+'mediazer/edit',
                type: 'get',
                data: token+formData,
                success: function(data) {
                    editBox.html(data);
                },
                error:function(){
                    toastr.error('Connection error !');
                    editBox.html(null);
                }
            });

        });

        $('body').on('click', '.MZ-right-close', function () {
            $.mediazerExtend.toggleRightColum('hide');
        });

        $('body').on('click', '#MZ-btn-update', function () {
            $.mediazerExtend.updateMedia($(this).data('id'));
        });


        $('body').on('click', '#MZ-delete-media', function () {
            if(confirm('Delete This Media ?')){
                $.mediazerExtend.deleteMedia($(this).data('id'));
            }
        });

        $('body').on('click', '#refresh-medias', function () {
            getFiles($('.MZ-selected-folder').html());
        });

        /**
         * GET SELECTED MEDIAS
         * */

        $('body').on('click', '#getMedias', function () {
            var mediaData=null;
            if(selectType=="single"){
                if(dataSource=="link"){
                    var mediaID= $('input.MZ-selector:checked').data('id');
                    mediaData= $('img[data-id="'+mediaID+'"]').attr('src');
                }else{
                    mediaData= $('input.MZ-selector:checked').data('id');
                }
            }else{
                var selected=[];
                $('input.MZ-selector:checked').each(function(){
                    selected.push($(this).data('id'));
                });
                mediaData= selected.toString();
            }
            $.mediazerExtend.getMediaCount('info');
            $("input[name='"+inputName+"']").val(mediaData);
            $('#mediazer').modal('hide');
        });

        function setUploader(element){
            $(element).dmUploader({
                url: urlPrefix+'mediazer/upload',
                dataType: 'json',
                allowedTypes: 'image/*',
                datatype: 'html',
                contentType: "text/html;charset=windows-1252",
                onBeforeUpload: function(id){
                    $(element).data('dmUploader').settings.extraData = {
                        _token: $("meta[name=\"csrf-token\"]").attr('content'),
                        path: $('.MZ-selected-folder').html()
                    };
                    $.mediazerUploader.updateFileStatus(id, 'default', 'Uploading...');
                },
                onNewFile: function(id, file){
                    $.mediazerUploader.addFile('#demo-files', id, file);
                    if (typeof FileReader !== "undefined"){
                        var reader = new FileReader();
                        var img = $('#demo-files').find('.demo-image-preview').eq(0);
                        reader.onload = function (e) {
                            img.attr('src', e.target.result);
                        };
                        reader.readAsDataURL(file);
                    } else {
                        $('#demo-files').find('.demo-image-preview').remove();
                    }
                },
                onComplete: function(){
                    getFiles($('.MZ-selected-folder').html());
                    toastr.success('Upload Success');
                },
                onUploadProgress: function(id, percent){
                    var percentStr = percent + '%';
                    $.mediazerUploader.updateFileProgress(id, percentStr);
                },
                onUploadSuccess: function(id, data){
                    $.mediazerUploader.updateFileStatus(id, 'success', 'Upload Complete');
                    $.mediazerUploader.updateFileProgress(id, '100%');
                },
                onUploadError: function(id, message){
                    $.mediazerUploader.updateFileStatus(id, 'error', message);
                    toastr.error('Upload Error');
                },
                onFileTypeError: function(file){
                    toastr.error('File Type Error!');
                }
            });
        };

        return this.each(function(){
            var appendHtml=
                '<button class="btn btn-primary">Mediazer</button>' +
                '<span class="medias">Please Select Media</span>' +
                '<input name="' + inputName + '" type="hidden" value=" "/>';
            $(this).append(appendHtml).addClass('mediazer-wrapper');
        });

    }
})(jQuery);

/**
 * File Checker *
 * */
(function( $, window, undefined ) {
    $.mediazerExtend = $.extend( {}, {
        checkedMedia: function(selector,select){
            if(select=="single"){
                $('input.MZ-selector').each(function(){
                    $(this).prop('checked', false);
                    $(this).next('img.MZ-thumb').removeClass('checked');
                });
                $(selector).prop('checked', true);
                $.mediazerExtend.checkedAction(selector);
            }else{
                $.mediazerExtend.checkedAction(selector);
            }
        },
        checkedAction: function(selector){
            if($(selector).is(":checked")){
                $(selector).next('img.MZ-thumb').addClass('checked');
            }else if($(selector).is(":not(:checked)")){
                $(selector).next('img.MZ-thumb').removeClass('checked');
            }
            $.mediazerExtend.getMediaCount('label');
        },
        getMediaCount:function(element){
            var count=0;
            $('input.MZ-selector:checked').each(function(){
                count++;
            });
            if(element=='label'){
                $('.MZ-count-label').html(count);
            }else if(element=='info'){
                $('.mediazer-wrapper .medias').html(count+' file selected.');
            }
            //$('#getMedias').
        },
        toggleRightColum:function(status){
            var right=$(".MZ-right");
            var body=$(".MZ-body");
            if(status=="show"){
                right.removeClass('close').show();
                body.removeClass('col-md-9','col-sm-9').addClass('col-md-6','col-sm-6');
            }else if(status=="hide"){
                right.addClass('close').hide();
                $('.MZ-edit-box').html(null);
                body.removeClass('col-md-6','col-sm-6').addClass('col-md-9','col-sm-9');
            }
        },
        updateMedia:function(id){
            var alt         =$('.MZ-edit-box input[name="alt"]').val();
            var title       =$('.MZ-edit-box input[name="title"]').val();
            var description =$('.MZ-edit-box textarea[name="description"]').val();
            var form_data="&id="+id+"&alt="+alt+"&title="+title+"&description="+description;
            $.ajax({
                url: urlPrefix+'mediazer/edit',
                type: 'post',
                data: token+form_data,
                success: function(data) {
                    toastr.success('Success Edit Media');
                },
                error:function(){
                    toastr.error('Connection Error!');
                }
            });
        },
        createFolder:function(){
            var selectedFolder=$('.MZ-selected-folder').html();
            var formData="&dir="+ selectedFolder +"&folder_name="+$('input[name="folder_name"]').val();
            $.ajax({
                url: urlPrefix+'mediazer/folder/create-folder',
                type: 'post',
                dataType: 'json',
                data: token+formData,
                success: function(data) {
                    if(data.status=='success'){
                        toastr.success(data.message);
                        $('#mediazer-form').modal('hide');
                    }else{
                        toastr.error(data.message);
                    }
                },
                error:function(){toastr.error('Connection error !');}
            });
        },
        deleteFolder:function(){
            var selectedFolder=$('.MZ-selected-folder').html();
            var form_data="&folder="+selectedFolder;
            $.ajax({
                url: urlPrefix+'mediazer/folder/delete',
                type: 'post',
                data: token+form_data,
                success: function(data) {
                    if(data.status=='success'){
                        toastr.success(data.message);
                    }else{
                        toastr.error(data.message);
                    }
                },
                error:function(){
                    toastr.error('Connection Error!');
                }
            });
        },
        deleteMedia:function(id){
            var form_data="&id="+id;
            $.ajax({
                url: urlPrefix+'mediazer/delete',
                type: 'post',
                data: token+form_data,
                success: function(data) {
                    toastr.success('Success Delete Media');
                    $.mediazerExtend.toggleRightColum('hide');
                    $('#refresh-medias').trigger( "click" );
                },
                error:function(){
                    toastr.error('Connection Error!');
                }
            });
        }


    }, $.mediazerExtend);
})(jQuery);

/**
 * UPLOADER EXTENDER
 * */
(function( $, window, undefined ) {
    $.mediazerUploader= $.extend( {}, {
        addFile: function(id, i, file){
            var template = '<div id="demo-file' + i + '">' +
                '<img src="http://placehold.it/48.png" class="demo-image-preview" style="width: 24px; height: 24px;" />' +
                '<span class="demo-file-id">#' + i + '</span> - ' + file.name + ' <span class="demo-file-size">(' + $.mediazerUploader.humanizeSize(file.size) + ')</span><br />Status: <span class="demo-file-status">Waiting to upload</span>'+
                '<div class="progress progress-striped active">'+
                '<div class="progress-bar" role="progressbar" style="width: 0%;">'+
                '<span class="sr-only">0% Complete</span>'+
                '</div>'+
                '</div>'+
                '</div>';
            var i = $(id).attr('file-counter');
            if (!i){
                $(id).empty();
                i = 0;
            }
            i++;
            $(id).attr('file-counter', i);
            $(id).prepend(template);
        },
        updateFileStatus: function(i, status, message){
            $('#demo-file' + i).find('span.demo-file-status').html(message).addClass('demo-file-status-' + status);
        },
        updateFileProgress: function(i, percent){
            $('#demo-file' + i).find('div.progress-bar').width(percent);
            $('#demo-file' + i).find('span.sr-only').html(percent + ' Complete');
        },
        humanizeSize: function(size) {
            var i = Math.floor( Math.log(size) / Math.log(1024) );
            return ( size / Math.pow(1024, i) ).toFixed(2) * 1 + ' ' + ['B', 'kB', 'MB', 'GB', 'TB'][i];
        }
    }, $.mediazerUploader);
})(jQuery, this);

$('.mediazer').Mediazer();
$('.mediazers').Mediazer();
/**
 * Mediazer
 * ++TODO: Ajax Load Mediazer
 * ++TODO: Uploads Media
 * TODO: Click & edit Media İnfo
 * TODO: Delete Media
 * TODO: Select & return media
 * TODO: Create Gallery
 * TODO: Next Version **********
 * ++TODO: Three view folder
 * +TODO: Folder CRUD*
 * **/


